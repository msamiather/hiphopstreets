//
//  Artist.h
//  HipHopStreets
//
//  Created by IOS on 13/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property (nonatomic ,strong) NSString *userId;
@property (nonatomic ,strong) NSString *name;
@property (nonatomic ,strong) NSString *lName;
@property (nonatomic ,strong) NSString *emailId;
@property (nonatomic ,strong) NSString *mobileNum;
@property (nonatomic ,strong) NSString *gender;
@property (nonatomic ,strong) NSString *imgUrl;
@property (nonatomic ,strong) NSString *regDate;




@end
