//
//  Song.h
//  HipHopStreets
//
//  Created by IOS on 13/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface Song : NSObject
@property (nonatomic ,strong) NSString *songId;
@property (nonatomic ,strong) NSString *songName;
@property (nonatomic ,strong) NSString *songCategory;
@property (nonatomic ,strong) NSString *songImgUrl;
@property (nonatomic ,strong) NSString *songFile;
@property (nonatomic ,strong) NSString *likeCount;
@property (nonatomic ,strong) NSString *category;

@property (nonatomic ,strong) User *user;


-(NSDictionary*)crateDictionary;
-(NSArray*)createArryWithFromModel:(NSArray*)songs;






@end
