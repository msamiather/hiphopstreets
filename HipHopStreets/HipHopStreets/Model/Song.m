//
//  Song.m
//  HipHopStreets
//
//  Created by IOS on 13/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "Song.h"

@implementation Song

-(NSDictionary*)crateDictionary{

    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:self.songName forKey:@"song_name"];
    [dict setObject:self.songId forKey:@"songid"];
    [dict setObject:self.songImgUrl forKey:@"song_image"];
    [dict setObject:self.songFile forKey:@"song_file"];
    [dict setObject:self.user.userId forKey:@"id"];
    [dict setObject:self.user.name forKey:@"name"];
    dict[@"likecount"]=self.likeCount;
    return dict;
}

-(NSArray*)createArryWithFromModel:(NSArray*)songs{
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    for (Song *sn in songs) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:sn.songName forKey:@"song_name"];
        [dict setObject:sn.songId forKey:@"songid"];
        [dict setObject:sn.songImgUrl forKey:@"song_image"];
        [dict setObject:sn.songFile forKey:@"song_file"];
        [dict setObject:sn.user.userId forKey:@"id"];
        [dict setObject:sn.user.name forKey:@"name"];
        dict[@"likecount"]=sn.likeCount;
        [arr addObject:dict];
    }
    
    return arr;
}

@end
