//
//  SBPickerView.h
//  Fleamonkey
//
//  Created by IOS on 03/02/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DATE_PICKER_HEIGHT 260.0f

@class SBDatePickerView;

@protocol SBDatePickerViewDelegate <NSObject>
@optional
- (void)datePickerView:(SBDatePickerView *)datePicker didSelectDate:(NSDate *)item dateString:(NSString *)dateString;
- (void)datePickerViewDidCancel:(SBDatePickerView *)datePicker;

@end

@interface SBDatePickerView : UIView

@property (strong, nonatomic) IBOutlet UIView *pickerContainerView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnCancel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnDone;
@property (strong, nonatomic) IBOutlet UIDatePicker *pickerView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *containerBottomConstraint;
@property (weak, nonatomic) id<UITextInput> inputField;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (weak,   nonatomic) id<SBDatePickerViewDelegate> delegate;

+ (instancetype)newDatePicker;

- (void)showActivityIndicator:(BOOL)show;
- (void)showInView:(UIView *)view;
- (void)hide;
- (void)reloadData;
- (BOOL)isVisible;

@end
