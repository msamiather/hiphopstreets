//
//  TableHeaderFooterView.h
//  MyMusic
//
//  Created by IOS on 09/09/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableHeaderFooterView : UITableViewHeaderFooterView
@property (nonatomic ,strong) IBOutlet UILabel *titleLbl;
//@property (nonatomic ,strong) IBOutlet UIButton *seeAllbtn;

@end
