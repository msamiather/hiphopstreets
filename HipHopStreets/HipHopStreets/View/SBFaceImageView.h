//
//  SBFaceImageView.h
//  EEChat
//
//  Created by Soumen Bhuin on 5/6/16.
//  Copyright © 2016 IGI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBFaceImageView : UIImageView

- (void)setGestureTarget:(nullable id)target action:(nullable SEL)action;

@end
