//
//  SBDatePickerView.m
//  Fleamonkey
//
//  Created by IOS on 03/02/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "SBDatePickerView.h"

@interface SBDatePickerView ()

@end

@implementation SBDatePickerView

+ (instancetype)newDatePicker {
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.dateFormat = @"MM-dd-yy";
}

- (void)showActivityIndicator:(BOOL)show {
    if (show) {
        [self.activityIndicatorView startAnimating];
        [self.pickerView setHidden:YES];
        [self.btnDone setEnabled:NO];
    }
    else {
        [self.activityIndicatorView stopAnimating];
        [self.pickerView setHidden:NO];
        [self.btnDone setEnabled:YES];
    }
}

- (void)reloadData {
    
}

#pragma mark - Presentation

- (void)showInView:(UIView *)view {
    
    self.frame = view.window.bounds;
    self.backgroundColor = [UIColor clearColor];
    [view.window addSubview:self];
    
    self.containerBottomConstraint.constant = -DATE_PICKER_HEIGHT;
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
    
    self.containerBottomConstraint.constant = 0;
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.1f];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide {
    self.containerBottomConstraint.constant = -DATE_PICKER_HEIGHT;
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (BOOL)isVisible {
    return self.superview!=nil;
}

#pragma mark - Actions

- (IBAction)btnCancelAction:(UIBarButtonItem *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(datePickerViewDidCancel:)]) {
        [self.delegate datePickerViewDidCancel:self];
    }
    [self hide];
}

- (IBAction)btnDoneAction:(UIBarButtonItem *)sender {
    NSDate *date = [self.pickerView date];
    if (self.delegate && [self.delegate respondsToSelector:@selector(datePickerView:didSelectDate:dateString:)]) {
        [self.delegate datePickerView:self didSelectDate:date dateString:[self.dateFormatter stringFromDate:date]];
    }
    [self hide];
}

#pragma mark - 

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self hide];
}

@end
