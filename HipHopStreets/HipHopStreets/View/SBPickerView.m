//
//  SBPickerView.m
//  Fleamonkey
//
//  Created by IOS on 03/02/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "SBPickerView.h"

@interface SBPickerView () <UIPickerViewDataSource>

@end

@implementation SBPickerView

+ (instancetype)newPicker {
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)showActivityIndicator:(BOOL)show {
    if (show) {
        [self.activityIndicatorView startAnimating];
        [self.pickerView setHidden:YES];
        [self.btnDone setEnabled:NO];
    }
    else {
        [self.activityIndicatorView stopAnimating];
        [self.pickerView setHidden:NO];
        [self.btnDone setEnabled:YES];
    }
}

- (void)reloadData {
    [self.pickerView reloadAllComponents];
}

#pragma mark - Presentation

- (void)showInView:(UIView *)view {
    
    self.frame = view.window.bounds;
    self.backgroundColor = [UIColor clearColor];
    [view.window addSubview:self];
    
    self.containerBottomConstraint.constant = -PICKER_HEIGHT;
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
    
    self.containerBottomConstraint.constant = 0;
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.1f];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide {
    self.containerBottomConstraint.constant = -PICKER_HEIGHT;
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (BOOL)isVisible {
    return self.superview!=nil;
}

#pragma mark - UIPickerViewDelegate

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.items.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (_itemTitleAccessor) {
        return [self.items[row] valueForKey:_itemTitleAccessor];
    }
    else {
        return self.items[row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
}

#pragma mark - Actions

- (IBAction)btnCancelAction:(UIBarButtonItem *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(pickerViewDidCancel:)]) {
        [self.delegate pickerViewDidCancel:self];
    }
    [self hide];
}

- (IBAction)btnDoneAction:(UIBarButtonItem *)sender {
    NSInteger row = [self.pickerView selectedRowInComponent:0];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pickerView:didSelectItem:atIndex:)]) {
        [self.delegate pickerView:self didSelectItem:self.items[row] atIndex:row];
    }
    [self hide];
}

#pragma mark - 

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self hide];
}

@end
