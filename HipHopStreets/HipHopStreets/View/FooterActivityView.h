//
//  FooterActivityView.h
//  HipHopStreets
//
//  Created by IOS on 28/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterActivityView : UICollectionReusableView
@property (nonatomic ,strong) IBOutlet UIActivityIndicatorView *activity;
@end
