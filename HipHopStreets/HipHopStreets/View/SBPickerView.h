//
//  SBPickerView.h
//  Fleamonkey
//
//  Created by IOS on 03/02/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PICKER_HEIGHT 264.0f

@class SBPickerView;

@protocol SBPickerViewDelegate <NSObject>
@optional
- (void)pickerView:(SBPickerView *)picker didSelectItem:(id)item atIndex:(NSInteger)index;
- (void)pickerViewDidCancel:(SBPickerView *)picker;

@end

@interface SBPickerView : UIView

@property (strong, nonatomic) IBOutlet UIView *pickerContainerView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnCancel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnDone;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *containerBottomConstraint;

@property (weak, nonatomic) id<UITextInput> inputField;

@property (weak,   nonatomic) id<SBPickerViewDelegate> delegate;
@property (strong, nonatomic) NSArray *items;
@property (copy,   nonatomic) NSString *itemTitleAccessor; // If items contains object then title of each iten will be picked by this property of the object

+ (instancetype)newPicker;

- (void)showActivityIndicator:(BOOL)show;
- (void)showInView:(UIView *)view;
- (void)hide;
- (void)reloadData;
- (BOOL)isVisible;

@end
