//
//  SBTextField.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 08/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBTextField.h"

@implementation SBTextField

- (void)setRightImage:(UIImage *)rightImage {
    
    _rightImage = rightImage;
    
    if (rightImage==nil) {
        self.rightView = nil;
    }
    else {
        if (self.rightView!=nil) {
            if ([self.rightView isKindOfClass:[UIImageView class]]) {
                [(UIImageView *)self.rightView setImage:rightImage];
            }
        }
        else {
            UIImageView *imgView = [[UIImageView alloc] initWithImage:rightImage];
            self.rightView = imgView;
            self.rightViewMode = UITextFieldViewModeAlways;
        }
    }
}

- (void)setLeftImage:(UIImage *)leftImage {
    
    _leftImage = leftImage;
    
    if (leftImage==nil) {
        self.leftView = nil;
    }
    else {
        if (self.leftView!=nil) {
            if ([self.leftView isKindOfClass:[UIImageView class]]) {
                [(UIImageView *)self.leftView setImage:leftImage];
            }
        }
        else {
            UIImageView *imgView = [[UIImageView alloc] initWithImage:leftImage];
            self.leftView = imgView;
            self.leftViewMode = UITextFieldViewModeAlways;
        }
    }
}

@end
