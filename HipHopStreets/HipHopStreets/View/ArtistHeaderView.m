//
//  ArtistHeaderView.m
//  HipHopStreets
//
//  Created by IOS on 16/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "ArtistHeaderView.h"

@implementation ArtistHeaderView


-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.btnFollow.layer.cornerRadius = self.btnFollow.bounds.size.height/2;
    self.btnFollow.layer.borderWidth =1;
    self.btnFollow.layer.borderColor =[[UIColor whiteColor] CGColor];
    self.btnSongCount.layer.cornerRadius =self.btnSongCount.bounds.size.height/2 ;
    self.btnOptions.layer.cornerRadius = self.btnOptions.bounds.size.height/2;
    self.btnOptions.layer.borderWidth =1;
    self.btnOptions.layer.borderColor =[[UIColor whiteColor] CGColor];
}
@end
