//
//  SBFaceImageView.m
//  EEChat
//
//  Created by Soumen Bhuin on 5/6/16.
//  Copyright © 2016 IGI. All rights reserved.
//

#import "SBFaceImageView.h"

@interface SBFaceImageView ()
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@end

@implementation SBFaceImageView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.layer.borderWidth = 0.5f;
//    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.clipsToBounds = YES;
}

- (void)setGestureTarget:(nullable id)target action:(nullable SEL)action {
    if (target) {
        if (self.tapGesture==nil) {
            self.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
            [self addGestureRecognizer:tap];
            self.tapGesture = tap;
        }
    }
    else {
        if (self.tapGesture!=nil) {
            [self removeGestureRecognizer:self.tapGesture];
            self.tapGesture = nil;
            self.userInteractionEnabled = NO;
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.bounds.size.width/2;
}
@end
