//
//  HHCollectionHeaderView.h
//  HipHopStreets
//
//  Created by IOS on 27/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHCollectionHeaderView : UICollectionReusableView
@property (nonatomic ,strong) IBOutlet UILabel *headerTitle;
@property (nonatomic ,strong) IBOutlet UIButton *viewAllSongsBtn;


@end
