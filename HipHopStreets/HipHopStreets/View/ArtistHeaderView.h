//
//  ArtistHeaderView.h
//  HipHopStreets
//
//  Created by IOS on 16/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistHeaderView : UICollectionReusableView
@property(nonatomic,strong)IBOutlet UIButton *btnFollow;
@property(nonatomic,strong)IBOutlet UIButton *btnSongCount;
@property(nonatomic,strong)IBOutlet UIButton *btnOptions;

@property(nonatomic,strong)IBOutlet UIImageView *artistImgView;
@property(nonatomic,strong)IBOutlet UILabel *artistName;
@property(nonatomic,strong)IBOutlet UILabel *followers;

@end
