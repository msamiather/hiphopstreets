//
//  CirclerView.h
//  MyMusic
//
//  Created by IOS on 01/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "MBCircularProgressBarView.h"

@interface CirclerView :MBCircularProgressBarView

+(instancetype)newCirclerView;

@end
