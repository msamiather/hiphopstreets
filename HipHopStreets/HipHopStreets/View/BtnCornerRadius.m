//
//  BtnCornerRadius.m
//  HipHopStreets
//
//  Created by IOS on 25/03/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "BtnCornerRadius.h"

@implementation BtnCornerRadius

-(void)awakeFromNib{
    [super awakeFromNib];
    self.layer.cornerRadius=2.0f;
    self.layer.borderWidth=1.0f;
    self.layer.borderColor=[[UIColor whiteColor]CGColor];
    self.clipsToBounds=YES;

}

@end
