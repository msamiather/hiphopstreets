//
//  SBTextField.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 08/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface SBTextField : UITextField
@property (nonatomic) IBInspectable UIImage *rightImage;
@property (nonatomic) IBInspectable UIImage *leftImage;
@end
