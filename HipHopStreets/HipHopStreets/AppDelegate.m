//
//  AppDelegate.m
//  HipHopStreets
//
//  Created by IOS on 12/17/16.
//  Copyright © 2016 Goigi. All rights reserved.
//

#import "AppDelegate.h"
#import "SBPreference.h"
#import "TabBarVC.h"
#import "SongLibrary.h"
#import "NavVC.h"
#import <AVFoundation/AVFoundation.h>
#import "SBExtensions.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[SBPreference sharedPreference] setupDefaults];

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    if ([[SBPreference sharedPreference]isUserLoggedIn]){
        NSString *userType=[SBPreference sharedPreference].loggedInUserInfo[USERTYPE];
        if ([userType isEqualToString:@"user"]) {
            TabBarVC *tabVC=(TabBarVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"userTabBar"];
            [[SongLibrary sharedLibrary] setTabBarController:tabVC];
            self.window.rootViewController = tabVC;
        }
        else{
            TabBarVC *tabVC=(TabBarVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"artistTabBar"];
            [[SongLibrary sharedLibrary] setTabBarController:tabVC];
            self.window.rootViewController = tabVC;
        }
    }
    else
    {
        UIViewController *vc=[mainStoryboard instantiateViewControllerWithIdentifier:@"SplashVC"];
        self.window.rootViewController=vc;
    }
    
    AVAudioSession *backgroundMusic = [AVAudioSession sharedInstance];
    NSError *setCategoryErr = nil;
    NSError *activationErr  = nil;
    
    [backgroundMusic setCategory:AVAudioSessionCategoryPlayback error:&setCategoryErr];
    [backgroundMusic setActive:YES error:&activationErr];
    
   // NSLog(@"DOC PATH: %@",[[NSFileManager defaultManager] documentDirectoryPath]);
    
    return YES;
}

-(void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
