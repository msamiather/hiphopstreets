//
//  DeviceSongsVC.m
//  HipHopStreets
//
//  Created by IOS on 13/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "DeviceSongsVC.h"

@interface DeviceSongsVC ()
@property (nonatomic ,strong) NSMutableArray *deviceSongs;

@end

@implementation DeviceSongsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.allowsMultipleSelection = NO;
    self.deviceSongs=[[NSMutableArray alloc]init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];
    for (NSString *item in directoryContent){
        
        // m4a and mp3
        if ([[item pathExtension] isEqualToString:@"m4a"] || [[item pathExtension] isEqualToString:@"mp3"]) {
            
            [self.deviceSongs addObject:item];
        }
    }

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.deviceSongs.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text=self.deviceSongs[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
   
    cell.accessoryType=UITableViewCellAccessoryCheckmark;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    
    cell.accessoryType=UITableViewCellAccessoryNone;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{

    if ([cell isSelected]) {
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
}

-(IBAction)doneBtnAction:(UIBarButtonItem*)sender{
    
    NSIndexPath *indexPath=[self.tableView indexPathForSelectedRow];
    NSString *songFile=self.deviceSongs[indexPath.row];
    if ([self.delegate respondsToSelector:@selector(finishedPickUpSongfile:controller:)]) {
        [self.delegate finishedPickUpSongfile:songFile controller:self];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)cancelBtn:(UIBarButtonItem*)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
