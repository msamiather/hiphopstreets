//
//  DeviceSongsVC.h
//  HipHopStreets
//
//  Created by IOS on 13/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DeviceSongsVC;
@protocol DeviceSongsVCDelegate <NSObject>
-(void)finishedPickUpSongfile:(NSString*)fileName controller:(UIViewController*)controller;

@end

@interface DeviceSongsVC : UITableViewController
@property(nonatomic ,assign) id<DeviceSongsVCDelegate> delegate;
@end
