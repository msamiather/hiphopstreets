//
//  DeviceSongsCell.h
//  HipHopStreets
//
//  Created by IOS on 13/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceSongsCell : UITableViewCell
@property (nonatomic ,strong) IBOutlet UILabel *songNameLbl;
@property (nonatomic ,strong) IBOutlet UILabel *songFileLbl;




@end
