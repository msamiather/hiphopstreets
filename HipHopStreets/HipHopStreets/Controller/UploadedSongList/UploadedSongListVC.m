//
//  UploadedSongListVC.m
//  HipHopStreets
//
//  Created by IOS on 13/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "UploadedSongListVC.h"
#import "UploadedSongListCell.h"

#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"
#import "UIKit+AFNetworking.h"
#import "NavVC.h"
#import "IAPStore.h"
#import "HomeVC.h"

#define FEATURE_PRODUCT_ID @"com.hhs.hiphopstreets.mkfeature"

@interface UploadedSongListVC ()<IAPStoreDelegate>
@property (nonatomic, strong)NSMutableArray *songList;
@property (nonatomic, strong) IAPStore *store;
@property (nonatomic, strong) NSNumberFormatter *currencyFormatter;
@property (nonatomic, strong) NSDictionary *serverProducts;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@end

@implementation UploadedSongListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fetchUploadedsongsList];
    // self.tableView.estimatedRowHeight = 60;
    
    _currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    self.store = [[IAPStore alloc] init];
    _serverProducts = @{FEATURE_PRODUCT_ID:@{@"trackcount":@"1",@"amount":@"44.99"}};
    [_store setProductIdentifiers:@[FEATURE_PRODUCT_ID]];
    [_store setDelegate:self];
    [_store startObservingTransaction];
}

#pragma - mark

-(void)fetchUploadedsongsList{
    
    NSString *userID = [[SBPreference sharedPreference] loggedInUserInfo][USER_ID];
    
    NSString *songListUrl =[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_UPLOAD_SONG,userID];//userID
    
    [self showActivity:nil];
    [[SBHTTPClient sharedClient] GET:songListUrl parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        
        NSDictionary *resp=responseObject;
                
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"]containsString:@"success"]) {
            
            self.songList = [[[resp objectForKey:@"message"] arrayFromCorruptedDictionary] mutableCopy];
            
            [self.tableView reloadData];
        }
        else{
            [self showAlert:@"No events found"];
            [self.tableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        NSLog(@"RESP ERROR:%@",error);
        NSDictionary *resp = [[SBHTTPClient sharedClient] responseObjectForError:error];
        if ([resp isKindOfClass:[NSDictionary class]]) {
            NSString *msg = resp[@"status"];
            if (msg) {
                [self showMessage:@"No songs found"];
            }
            else {
                [self showAlert:error.localizedDescription];
            }
        }
        else {
            [self showAlert:error.localizedDescription];
        }
    }];
}

#pragma inappPurchases

- (void)didFinishPurchasingProductID:(NSString*)pid error:(NSError*)error {
    
    if (error==nil) {
        //[self showAlert:@"You are successfully subscribed."];
        
        NSString *token = [_store base64EncodedTransactionReceipt];
        if (token==nil) {
            token = @"devtoken-unknown";
        }
        
        if ([pid isEqualToString:FEATURE_PRODUCT_ID]) {
            
            NSDictionary *dict=self.songList[self.selectedIndexPath.row];
            NSString *url=[NSString stringWithFormat:@"%@%@",BASE_URL,SERVICE_PATH_MAKE_FEATURE_SONG];
            NSString *userid=[SBPreference sharedPreference].loggedInUserInfo[USER_ID];
            
            [self showActivity:nil];
            
            [[SBHTTPClient sharedClient] GET:url parameters:@{@"songid":dict[@"songid"],@"uid":userid,@"ref_id":token} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [self hideActivity];
                
                NSDictionary *resp=responseObject;
                
                if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"]containsString:@"success"]) {
                    
                    [self showAlert:@"Song successfully made featured."];
                    UINavigationController *nav=[[self.tabBarController viewControllers]objectAtIndex:0];
                    HomeVC *vc=(HomeVC*)[[nav viewControllers]firstObject];
                    [vc fetchFeaturesSongs];
                }
                else{
                    [self showAlert:@"Unable to make featured, try after some times."];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self hideActivity];
                NSLog(@"RESP ERROR:%@",error);
                NSDictionary *resp = [[SBHTTPClient sharedClient] responseObjectForError:error];
                if ([resp isKindOfClass:[NSDictionary class]]) {
                    if ([resp[@"status"]containsString:@"success"]) {
                        [self showAlert:@"Song successfully made featured."];
                    }
                    else {
                        [self showAlert:@"Unable to update, try after some times."];
                    }
                }
                else {
                    [self showAlert:error.localizedDescription];
                }
            }];
        }
    }
}

- (void)didFinishRestoringProductsWithError:(NSError*)error {
    [self hideActivity];
    if (error!=nil) {
        [self showAlert:@"Unable to purchase, Try later."];
    }
}

-(void)didFinishDownloadingProductID:(NSString*)pid error:(NSError*)error {
    
    if (![pid isEqualToString:FEATURE_PRODUCT_ID]) {
        return;
    }
    
    [self hideActivity];
    
    if (error==nil) {
        SKProduct *p = [_store productWithID:FEATURE_PRODUCT_ID];
        [_currencyFormatter setLocale:p.priceLocale];
        [self.store buyProduct:p];
    }
}


- (void)didFinishFetchingProductsWithError:(NSError *)erroe {
    [self hideActivity];
    [self showAlert:erroe.localizedDescription];
    
    NSLog(@"error: %@",erroe);
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.songList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UploadedSongListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary *dic = self.songList[indexPath.row];
    
    cell.lblSongName.text = dic[@"song_name"];
    cell.lblCategoryName.text = dic[@"song_category"];
    NSString *imrUrl = [NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,dic[@"song_image"]];
    [cell.songImage setImageWithURL:[NSURL URLWithString:imrUrl] placeholderImage:[UIImage imageNamed:@"songPlacehodler"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIAlertController *alrt=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alrt addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self deleteSong:indexPath];
    }]];
    [alrt addAction:[UIAlertAction actionWithTitle:@"Make Feature" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self makeFeatureSong:indexPath];
    }]];
    [alrt addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alrt animated:YES completion:nil];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

#pragma - mark buttion Action

-(IBAction)backButtonAction:(UIBarButtonItem*)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(IBAction)bntAddSongAction:(UIBarButtonItem*)sender{
    
    //[self makeFeatureSong:nil];
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self deleteSong:indexPath];
    }];
    editAction.backgroundColor = [UIColor redColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Make Feature"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self makeFeatureSong:indexPath];
    }];
    
    deleteAction.backgroundColor = [UIColor blackColor];
    return @[deleteAction,editAction];
}



#pragma ButtonActions

-(void)deleteSong:(NSIndexPath*)indexPath{

    NSDictionary *dict=self.songList[indexPath.row];
    NSString *url=[NSString stringWithFormat:@"%@%@%@",BASE_URL,SERVICE_PATH_DELETE_SONG,dict[@"songid"]];
    
    //http://xpandingus.us/hiphopstreets/api/songfr/5
    
    [self showActivity:nil];
    [[SBHTTPClient sharedClient] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        
        NSDictionary *resp=responseObject;
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"]containsString:@"deleted"]) {
            [self.songList removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self showToast:@"Song successfully deleted."];
        }
        else
        {
            [self showAlert:@"Unable to delete song, try after some times."];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        NSLog(@"RESP ERROR:%@",error);
        NSDictionary *resp = [[SBHTTPClient sharedClient] responseObjectForError:error];
        if ([resp isKindOfClass:[NSDictionary class]]) {
            if ([resp[@"status"]containsString:@"success"]) {
                [self.songList removeObjectAtIndex:indexPath.row];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [self showAlert:@"Song successfully deleted"];
            }
            else {
                [self showAlert:@"Unable to delete song, try after some times."];
            }
        }
        else {
            [self showAlert:error.localizedDescription];
        }
    }];
}

-(void)makeFeatureSong:(NSIndexPath*)indexPath{
    self.selectedIndexPath = indexPath;
    
    if (_store.products.count==0) {
        [self showActivity:nil];
        [_store fetchAndValidateProducts];
    }
    else {

        SKProduct *p = [_store productWithID:FEATURE_PRODUCT_ID];
        [_currencyFormatter setLocale:p.priceLocale];
        [self.store buyProduct:p];
        
    }
}


- (void)dealloc {
    [_store stopObservingTransaction];
    [_store setDelegate:nil];
}

@end
