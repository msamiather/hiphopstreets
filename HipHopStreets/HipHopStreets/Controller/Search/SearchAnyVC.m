//
//  SearchAnyVC.m
//  HipHopStreets
//
//  Created by IOS on 26/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "SearchAnyVC.h"
#import "SBPreference.h"
#import "SBExtensions.h"
#import "SearchAnyCell.h"

#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"

#import "Song.h"
#import "SongLibrary.h"

@interface SearchAnyVC ()
@property (strong, nonatomic) UITableViewController *searchResultsTableViewController;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong ,nonatomic) NSMutableArray *songsList;

@property (nonatomic ,assign) NSInteger startPageCount;
@property (nonatomic ,assign) BOOL ispageEnded;

@end

@implementation SearchAnyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.delegate=self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchBar.tintColor=[UIColor lightGrayColor];
    self.searchController.searchBar.barTintColor=[UIColor colorWithRed:156.0f/255.0f green:0.0f blue:0.0f alpha:1.0f];

    self.searchController.searchBar.backgroundColor=[UIColor clearColor];
    self.searchController.searchBar.placeholder=@"Search song here";
    [self.tableView smb_showMessage:@"Enter text to search song"];
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    [self.searchController.searchBar sizeToFit];
    
    self.ispageEnded=NO;
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithSize:CGSizeMake(10, 64) color:[UIColor colorWithRed:156.0f/255.0f green:0.0f blue:0.0f alpha:1.0f]]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage imageWithSize:CGSizeMake(10, 1) color:[UIColor colorWithRed:156.0f/255.0f green:0.0f blue:0.0f alpha:1.0f]];
}


-(void)fetchCategorySongsWithStartWith:(NSInteger)startCount{
    
    if (self.startPageCount==0) {
        [self showActivity:nil];
    }
    
    NSString *searchString=self.searchController.searchBar.text;
    
    NSString *url=[NSString stringWithFormat:@"%@/%@/%lu/%@",SERVICE_SONG_SEARCH,searchString,startCount,@"10"];
    
    //dev.goigi.biz/hiphop/api/songsearch/s/1/3

    
    [[SBHTTPClient sharedClient] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        [self hideMessage];
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        if ([resp[@"songanduserlist"] isKindOfClass:[NSArray class]] && [resp[@"songanduserlist"] count]!=0) {
            if (self->_startPageCount==0) {
                self.songsList=[[NSMutableArray alloc]init];
                NSArray *arr=resp[@"songanduserlist"];
                if (arr.count!=0) {
                    for (NSDictionary *sDict in arr) {
                        Song *s=[[Song alloc]init];
                        s.songId=sDict[@"songid"];
                        s.songName=sDict[@"song_name"];
                        s.songImgUrl=sDict[@"song_image"];
                        s.songCategory=sDict[@"song_category"];
                        s.songFile=sDict[@"song_file"];
                        s.likeCount=sDict[@"likecount"];
                        NSDictionary *uDict=sDict[@"user"];
                        s.user=[[User alloc]init];
                        s.user.userId=uDict[@"id"];
                        s.user.name=uDict[@"name"];
                        s.user.emailId=uDict[@"email_id"];
                        s.user.mobileNum=uDict[@"mobile"];
                        s.user.imgUrl=uDict[@"pic"];
                        s.user.gender=uDict[@"sex"];
                        s.user.regDate=uDict[@"regddate"];
                        [self.songsList addObject:s];
                    }
                    
                }
                [self.tableView reloadData];
            }
            else{
                NSInteger i=self.songsList.count;
                NSArray *arr=resp[@"songanduserlist"];
                NSMutableArray *indexPaths=[[NSMutableArray alloc]init];
                if (arr.count!=0) {
                    for (NSDictionary *sDict in arr) {
                        Song *s=[[Song alloc]init];
                        s.songId=sDict[@"songid"];
                        s.songName=sDict[@"song_name"];
                        s.songImgUrl=sDict[@"song_image"];
                        s.songCategory=sDict[@"song_category"];
                        s.songFile=sDict[@"song_file"];
                        s.likeCount=sDict[@"likecount"];
                        NSDictionary *uDict=sDict[@"user"];
                        s.user=[[User alloc]init];
                        s.user.userId=uDict[@"id"];
                        s.user.name=uDict[@"name"];
                        s.user.emailId=uDict[@"email_id"];
                        s.user.mobileNum=uDict[@"mobile"];
                        s.user.imgUrl=uDict[@"pic"];
                        s.user.gender=uDict[@"sex"];
                        s.user.regDate=uDict[@"regddate"];
                        [self.songsList addObject:s];
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        i++;
                    }
                }
                [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
            }
        }
        else{
            if (self.startPageCount==0) {
                self.songsList=nil;
                [self showMessage:@"No songs found"];
                [self.tableView reloadData];
            }
            else{
        
                self.ispageEnded=YES;
                [self.activity stopAnimating];
                
            }
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        [self hideMessage];
        [self showToast:error.localizedDescription];
    }];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.songsList.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Song *sn=self.songsList[indexPath.row];
    SearchAnyCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchAnyCell class])];
    [cell.songImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,sn.songImgUrl]] placeholderImage:[UIImage imageNamed:@"songPlacehodler"]];
    cell.songNameLbl.text=sn.songName;
    cell.artistNameLbl.text=sn.songCategory;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.ispageEnded) {
        [self.activity stopAnimating];
    }
    else{
        
        if (indexPath.row==self.songsList.count-1) {
            [self.activity startAnimating];
            self.startPageCount=_startPageCount+10;
            [self fetchCategorySongsWithStartWith:_startPageCount];
        }
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Song *sn=self.songsList[indexPath.row];
        
    [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    [[SongLibrary sharedLibrary] showPlayerAndPlaySong:[sn crateDictionary]];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSLog(@"%@",searchController.searchBar.text);
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    self.startPageCount=0;
    self.ispageEnded=NO;
    [self fetchCategorySongsWithStartWith:_startPageCount];
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma buttonActions



@end
