//
//  SearchAnyCell.h
//  HipHopStreets
//
//  Created by IOS on 26/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchAnyCell : UITableViewCell
@property (nonatomic ,strong) IBOutlet UIImageView *songImg;
@property (nonatomic ,strong) IBOutlet UILabel *songNameLbl;
@property (nonatomic ,strong) IBOutlet UILabel *artistNameLbl;


@end
