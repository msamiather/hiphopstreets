//
//  SearchAnyVC.h
//  HipHopStreets
//
//  Created by IOS on 26/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchAnyVC : UITableViewController<UISearchResultsUpdating,UISearchControllerDelegate,UISearchBarDelegate>
@property (nonatomic ,strong) IBOutlet  UIActivityIndicatorView *activity;

@end
