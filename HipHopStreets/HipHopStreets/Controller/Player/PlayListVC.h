//
//  PlayListVC.h
//  MyMusic
//
//  Created by IOS on 02/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PlayListVC;
@protocol PlayListVCDelegate <NSObject>
-(void)didSelectSongFomPlayList:(PlayListVC*)controller songIndex:(NSUInteger)index;

@end

@interface PlayListVC : UITableViewController
@property(nonatomic ) NSUInteger currentIndex;
@property (nonatomic,assign) id <PlayListVCDelegate>Delegate;

@end
