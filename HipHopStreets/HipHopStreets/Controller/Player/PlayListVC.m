//
//  PlayListVC.m
//  MyMusic
//
//  Created by IOS on 02/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "PlayListVC.h"
#import "PlayListCell.h"
#import "SongLibrary.h"
#import "SBExtensions.h"

#import "SBPreference.h"
#import "SBHTTPClient.h"

@interface PlayListVC ()

@end

@implementation PlayListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[[SongLibrary sharedLibrary]allSongs]count]) {
        [self hideMessage];
    }
    else{
        [self showMessage:@"You have no playlist songs"];
    
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([[[SongLibrary sharedLibrary]allSongs]count]) {
        return 1;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[SongLibrary sharedLibrary]allSongs]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict=[[[SongLibrary sharedLibrary]allSongs] objectAtIndex:indexPath.row];
    PlayListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayListCell" forIndexPath:indexPath];
    cell.songName.text=dict[@"song_name"];
    cell.songDescription.text=dict[@"name"];
    [cell.songImgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,dict[@"song_image"]]] placeholderImage:[UIImage imageNamed:@"songPlacehodler"]];
    if (indexPath.row==_currentIndex) {
        cell.audioImg.image=[UIImage imageNamed:@"playingSong"];
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
    
    [cell.moreOptionsBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.moreOptionsBtn.tag=indexPath.row;
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{

    PlayListCell *customcell = (PlayListCell*)cell;

    if ([customcell isSelected]) {
        customcell.audioImg.image=[UIImage imageNamed:@"playingSong"];
    }
    else{
        customcell.audioImg.image=nil;

    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PlayListCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    cell.audioImg.image=[UIImage imageNamed:@"playingSong"];
    
    if ([self.Delegate respondsToSelector:@selector(didSelectSongFomPlayList:songIndex:)]) {
        [self.Delegate didSelectSongFomPlayList:self songIndex:indexPath.row];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{

    PlayListCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    cell.audioImg.image=nil;
    
}

#pragma UibuttonActions

-(void)moreBtnAction:(UIButton*)sender{
    
    UIAlertController *controller=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *fav=[UIAlertAction actionWithTitle:@"Add to favourite" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self makeFavourite:sender.tag];
    }];
    UIAlertAction *delete=[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        [self deleteSongAt:sender.tag];
    }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [controller dismissViewControllerAnimated:YES completion:nil];
        
    }];
    [controller addAction:fav];
    [controller addAction:cancel];

    
    if (sender.tag!=_currentIndex) {
        [controller addAction:delete];
    }
    
    [self presentViewController:controller animated:YES completion:nil];


}

-(void)makeFavourite:(NSInteger)cIndex{
    
    NSString *userid=[NSString stringWithFormat:@"%@",[SBPreference sharedPreference].loggedInUserInfo[USER_ID]];
    NSUInteger currentIndex = [[SongLibrary sharedLibrary] currentIndex];
    
    NSDictionary *songDict=[[SongLibrary sharedLibrary]songAtIndex:currentIndex];
    
    self.view.userInteractionEnabled=NO;
    [[SBHTTPClient sharedClient] POST:SERVICE_PATH_LIKE_SONG parameters:@{@"userid":userid,@"songid":songDict[@"songid"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.view.userInteractionEnabled=YES;
        NSDictionary *resp = responseObject;
        if ([resp isKindOfClass:[NSDictionary class]] && ![resp[@"error"]boolValue]) {
            [self showToast:resp[@"message"]];
        }
        else{
            [self showToast:resp[@"message"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        self.view.userInteractionEnabled=YES;
        
        [self showAlert:error.localizedDescription];
    }];
    
}

-(void)deleteSongAt:(NSInteger)cIndex{
    
    NSDictionary *songDict=[[SongLibrary sharedLibrary]songAtIndex:cIndex];
    [[SongLibrary sharedLibrary]removeSong:songDict];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"AlbumSongs"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSError *error = nil;
        path = [path stringByAppendingPathComponent:songDict[@"song_file"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:path])	//Does file already exist?
        {
            [[NSFileManager defaultManager]removeItemAtPath:path error:&error];
        }
    }
    
    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:cIndex inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    if ([[[SongLibrary sharedLibrary]allSongs]count]==0) {
        [self showMessage:@"No Playlist songs found!"];
    }
}

-(IBAction)cancelBtnAction:(UIBarButtonItem*)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
