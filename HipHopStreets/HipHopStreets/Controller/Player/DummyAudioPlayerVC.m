//
//  DummyAudioPlayerVC.m
//  HipHopStreets
//
//  Created by IOS on 22/03/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "DummyAudioPlayerVC.h"
#import <AVFoundation/AVFoundation.h>
#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"
#import "UIKit+AFNetworking.h"



@interface DummyAudioPlayerVC ()
@property (nonatomic ,strong) AVPlayer *avplayer;
@property (nonatomic ,strong) AVPlayerItem *playerItem;
@end

@implementation DummyAudioPlayerVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
