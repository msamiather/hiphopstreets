//
//  AudioPlayerVC.h
//  MyMusic
//
//  Created by IOS on 12/09/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVAsset.h>
#import "Song.h"

typedef NS_ENUM(NSUInteger, PlayingType) {
    PlayingTypeStaticItems,
    PlayingTypeSync,
    PlayingTypeAsync,
};

@interface AudioPlayerVC : UIViewController<AVAudioSessionDelegate>

@property (nonatomic ,strong) IBOutlet UIImageView *coverImg;
@property (nonatomic ,strong) IBOutlet UISlider *audioProgress;
@property (nonatomic ,strong) IBOutlet UILabel *startTime;
@property (nonatomic ,strong) IBOutlet UILabel *progressTime;
@property (nonatomic ,strong) IBOutlet UISlider *volumeSlider;
@property (nonatomic ,strong) IBOutlet UIButton *forwardBtn;
@property (nonatomic ,strong) IBOutlet UIButton *backWardBtn;
@property (nonatomic ,strong) IBOutlet UIButton *playAudioBtn;
@property (nonatomic ,strong) IBOutlet UILabel *songName;
@property (nonatomic ,strong) IBOutlet UIButton *artistNameBtn;

@property (nonatomic ,strong) IBOutlet UIButton *likeSongBtn;
@property (nonatomic ,strong) IBOutlet UIButton *addtoPlayerBtn;
@property (strong, nonatomic) IBOutlet UIButton *btnRepeat;
@property (strong, nonatomic) IBOutlet UILabel *likesCountLbl;


@property (nonatomic ,strong) AVPlayer *avplayer;
@property (nonatomic ,strong) AVPlayerItem *playerItem;

@property (nonatomic ,strong) id audioSong;

- (void)playSong:(id)song currentIdex:(NSInteger)index type:(BOOL)isTopRated;

@end
