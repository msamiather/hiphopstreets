//
//  AudioPlayerVC.m
//  MyMusic
//
//  Created by IOS on 12/09/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "AudioPlayerVC.h"
#import "SBExtensions.h"
#import "SongLibrary.h"
#import "NavVC.h"
#import "PlayListVC.h"

#import "SBPreference.h"
#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "Song.h"

#import "ArtistProfileVC.h"
#import "PlayListVC.h"
#import "HPPsongsList.h"
#import <CoreTelephony/CTCall.h>

@interface AudioPlayerVC ()<AVAudioPlayerDelegate,PlayListVCDelegate>
@property (nonatomic, assign) BOOL playing;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) NSUInteger backWardIndex;

@property (nonatomic ,strong) NSTimer *songTimer;
@property (nonatomic ,strong) NSMutableArray *songsList;
@property (nonatomic ,strong) NSArray *storedSongs;

@property (nonatomic ,assign) NSInteger currentDuration;
@property (nonatomic ,assign) NSInteger likesCount;
@property (nonatomic ,assign) BOOL isPalyerReachedTheEnd;

@end

@implementation AudioPlayerVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [self becomeFirstResponder];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playBackAfterCallEnd:) name:AVAudioSessionInterruptionNotification object:nil];
    
}

-(void)startSongTimer{
    _songTimer =[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateSongInfo:) userInfo:nil repeats:YES];
}

-(void)stopSongTimer{
    [self.songTimer invalidate];
    self.songTimer=nil;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)playSong:(id)song currentIdex:(NSInteger)index type:(BOOL)isTopRated{
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];

    } @catch (NSException *exception) {
    
    }
    self.songsList=[[NSMutableArray alloc]init];
    
    if ([self isViewLoaded]) {
        [self setupRepeatStatus];
        
        if ([song isKindOfClass:[NSDictionary class]]) {
            [self.songsList addObject:song];
        }
        else{
            [[SongLibrary sharedLibrary] setCurrentIndex:index];
            self.songsList=[song mutableCopy];
        }
        
        [self playParticularSong:[self.songsList objectAtIndex:index]];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        _isPalyerReachedTheEnd=NO;
    }
    
    else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self setupRepeatStatus];
            if ([song isKindOfClass:[NSDictionary class]]) {
                [self.songsList addObject:song];
            }
            else{
                [[SongLibrary sharedLibrary] setCurrentIndex:index];
                self.songsList=[song mutableCopy];
            }
            [self playParticularSong:[self.songsList objectAtIndex:index]];
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
            _isPalyerReachedTheEnd=NO;

        });
    }
}

- (void)setupRepeatStatus {
    // Update repeating state
    switch ([SongLibrary sharedLibrary].repeatType){
        case SongRepeatNone: {
            
            [self.btnRepeat setImage:[UIImage imageNamed:@"repeatoff"] forState:UIControlStateNormal];
        }
            break;
        case SongRepeatAll: {
            [self.btnRepeat setImage:[UIImage imageNamed:@"repeateall"] forState:UIControlStateNormal];
        }
            break;
        case SongRepeatOne:{
            
            [self.btnRepeat setImage:[UIImage imageNamed:@"repeate_signleSong"] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

-(void)playParticularSong:(NSDictionary*)dict{
if (self.songsList.count==0) {
        [self showAlert:@"You have no song in the library."];
        return;
}
    NSInteger currectIndex=[[SongLibrary sharedLibrary]getcurrentIndexFromArry:self.songsList fromDict:dict];

    [[SongLibrary sharedLibrary] setCurrentIndex:currectIndex];
    
    [self.playAudioBtn setSelected:YES];
    
    if (currectIndex==self.songsList.count-1) {
        [self.forwardBtn setEnabled:NO];
    }
    else{
        [self.forwardBtn setEnabled:YES];
    }

    if (currectIndex==0) {
        [self.backWardBtn setEnabled:NO];
    }
    else{
        [self.backWardBtn setEnabled:YES];
    }
    
    NSString *urstring=[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONGFILE,dict[@"song_file"]];
    
    NSURL *url = [NSURL URLWithString:[urstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    @try{
        [self.playerItem removeObserver:self forKeyPath:@"status"];

        
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        UIImage *artworkImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,dict[@"song_image"]]]]];
        MPMediaItemArtwork *albumArt;
        if(artworkImage)
        {
            albumArt = [[MPMediaItemArtwork alloc] initWithImage: artworkImage];
            
        }
        else{
        albumArt = [[MPMediaItemArtwork alloc] initWithImage: [UIImage imageNamed:@"song_placeholder"]];
        }
        
        MPNowPlayingInfoCenter *mpc=[MPNowPlayingInfoCenter defaultCenter];
        mpc.nowPlayingInfo=@{MPMediaItemPropertyArtist:dict[@"name"],
                             MPMediaItemPropertyTitle:dict[@"song_name"],
                             MPMediaItemPropertyArtwork:albumArt
                    };
        });
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    self.playerItem=[AVPlayerItem playerItemWithURL:url];
    [self.playerItem addObserver:self forKeyPath:@"status" options:0 context:(__bridge void * _Nullable)(dict)];
    self.avplayer=[AVPlayer playerWithPlayerItem:_playerItem];
    _avplayer.automaticallyWaitsToMinimizeStalling =NO;
    _isPalyerReachedTheEnd=NO;


    dispatch_async(dispatch_get_main_queue(), ^{
        [self showActivity:nil];
        self.songName.text=dict[@"song_name"];
        [self.coverImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,dict[@"song_image"]]] placeholderImage:[UIImage imageNamed:@"song_placeholder"]];
        [self.artistNameBtn setTitle:dict[@"name"] forState:UIControlStateNormal];
        [self.artistNameBtn setTag:currectIndex];
        [self.artistNameBtn addTarget:self action:@selector(moveToArtist:) forControlEvents:UIControlEventTouchUpInside];
        [self.likeSongBtn addTarget:self action:@selector(likeSongAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.likeSongBtn setSelected:NO];
        _likesCount=[dict[@"likecount"] integerValue];
        self.likesCountLbl.text=[NSString stringWithFormat:@"%@ Likes",dict[@"likecount"]];
        [self songLikeStatus:dict[@"songid"]];
    });
}

#pragma remotController

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    UIEventSubtype rc = event.subtype;
    NSLog(@"got a remote event! %ld", (long)rc);
    
    if (rc == UIEventSubtypeRemoteControlPlay) {
        [self.playAudioBtn setSelected:YES];
        int32_t timeScale = self.avplayer.currentItem.asset.duration.timescale;
        CMTime time = CMTimeMakeWithSeconds(_currentDuration, timeScale);
        [self.avplayer seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        [self.avplayer play];
        
        
    } else if (rc == UIEventSubtypeRemoteControlPause) {
        [self.playAudioBtn setSelected:NO];
        _currentDuration = (int)CMTimeGetSeconds (self.avplayer.currentItem.currentTime);
        [self.avplayer pause];
    }
    else if (event.subtype == UIEventSubtypeRemoteControlNextTrack){
        
        NSUInteger currentIndex = [[SongLibrary sharedLibrary] currentIndex];
        
        if (currentIndex<self.songsList.count-1) {
            [self.backWardBtn setEnabled:YES];
            currentIndex=currentIndex+1;
            [[SongLibrary sharedLibrary] setCurrentIndex:currentIndex];
            [self playParticularSong:[self.songsList objectAtIndex:currentIndex]];
            
            if (currentIndex==self.songsList.count-1) {
                [self.forwardBtn setEnabled:NO];
                
            }
        }
    }
    else if (event.subtype == UIEventSubtypeRemoteControlPreviousTrack){
        
        NSUInteger currentIndex = [[SongLibrary sharedLibrary] currentIndex];
        
        if (currentIndex>0) {
            [self.forwardBtn setEnabled:YES];
            currentIndex=currentIndex-1;
            [[SongLibrary sharedLibrary] setCurrentIndex:currentIndex];
            [self playParticularSong:[self.songsList objectAtIndex:currentIndex]];
            
            if (currentIndex==0) {
                [self.backWardBtn setEnabled:NO];
            }
            
        }
    }
    else if (event.subtype == UIEventSubtypeRemoteControlStop){
        [self.avplayer seekToTime:CMTimeMake(0, 1)];
        [self.avplayer pause];
    }
}


-(void)updateSongInfo:(NSTimer*)timer{
    
    int totalTime = CMTimeGetSeconds([self.avplayer.currentItem duration]);

    int currentTime = CMTimeGetSeconds([self.avplayer.currentItem currentTime]);
    
    
    int minutes = (int)currentTime/60;
    int seconds = (int)currentTime% 60;
    
    int remingTime=totalTime-currentTime;
    int remaingMin=(int)remingTime/60.0f;
    int remaingSec=(int)remingTime%60;
    
    if (currentTime!=0) {
        [self hideActivity];
    }
    
    self.audioProgress.value=(float)currentTime;
    
    self.startTime.text=[NSString stringWithFormat:@"%d:%d",minutes,seconds];
    self.progressTime.text=[NSString stringWithFormat:@"%d:%d",remaingMin,remaingSec];

}

-(float)totalSongTime{
    float xb = CMTimeGetSeconds([self.avplayer.currentItem duration]);
    return xb;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    if ([object isKindOfClass:[AVPlayerItem class]] && [keyPath isEqualToString:@"status"] ) {
        if ([_playerItem status]==AVPlayerStatusReadyToPlay) {
            [self.avplayer play];
            
            [self showActivity:nil];
            [self startSongTimer];
            self.audioProgress.maximumValue=[self totalSongTime];
            self.audioProgress.minimumValue=0;
            
        }
        else if ([_playerItem status]==AVPlayerStatusFailed){
            [self showAlert:@"this file is not in currect format"];
            [self hideActivity];
        }
    }
}

-(void)playerItemDidReachEnd:(NSNotification*)notification{
    
    if (_isPalyerReachedTheEnd){
        return;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];

    _isPalyerReachedTheEnd=YES;
    SongLibrary *lib = [SongLibrary sharedLibrary];
    
    NSUInteger currentIndex = [lib currentIndex];
    
    if (currentIndex < self.songsList.count-1) {
        if (lib.repeatType==SongRepeatAll) {
            [self stopSongTimer];
            
            currentIndex++;
            [lib setCurrentIndex:currentIndex];
            [self playParticularSong:[self.songsList objectAtIndex:currentIndex]];
        }
        else if (lib.repeatType==SongRepeatOne) {
            [self stopSongTimer];
            
            [self playParticularSong:[self.songsList objectAtIndex:currentIndex]];
        }
        else {
            [self.timer invalidate];
            self.timer = nil;
            self.volumeSlider.value = 0;
            [self.playAudioBtn setSelected:NO];
        }
    }
    else {
        if (lib.repeatType==SongRepeatAll) {
            [self.timer invalidate];
            self.timer = nil;
            currentIndex = 0;
            [lib setCurrentIndex:currentIndex];
            [self playParticularSong:[self.songsList objectAtIndex:currentIndex]];
        }
        else if (lib.repeatType==SongRepeatOne){
            [self.timer invalidate];
            self.timer = nil;
            [self playParticularSong:[self.songsList objectAtIndex:currentIndex]];
        }
        else {
            [self.playAudioBtn setSelected:NO];
            [self.timer invalidate];
            self.timer=nil;
            self.volumeSlider.value = 0;
        }
        
    }
}

#pragma buttonAction

-(IBAction)playBtnAction:(UIButton*)sender{
    
    if (self.songsList.count==0) {
        [self showAlert:@"You have no song in the library."];
        return;
    }
    
    if (self.avplayer!=nil && self.avplayer.rate!=0) {
        _currentDuration = (int)CMTimeGetSeconds (self.avplayer.currentItem.currentTime);

        [sender setSelected:NO];
        [self.avplayer pause];
    }
    else{
        [sender setSelected:YES];
        int32_t timeScale = self.avplayer.currentItem.asset.duration.timescale;
        CMTime time = CMTimeMakeWithSeconds(_currentDuration, timeScale);
        [self.avplayer seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        [self.avplayer play];
    }
    
}
-(IBAction)sliderAction:(UISlider*)sender {
    if (self.avplayer!=nil && self.avplayer.rate!=0 && [self.avplayer.currentItem isPlaybackLikelyToKeepUp]) {
        int32_t timeScale = self.avplayer.currentItem.asset.duration.timescale;
        CMTime time = CMTimeMakeWithSeconds(sender.value, timeScale);
    
        NSLog(@"%f",sender.value);
        [self.avplayer seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    }
}

-(IBAction)forwardBtnAction:(UIButton*)sender{
    
    if (self.songsList.count==0) {
        [self showAlert:@"You have no song in the library."];
        return;
    }
    NSUInteger currentIndex = [[SongLibrary sharedLibrary] currentIndex];
    
    if (currentIndex<self.songsList.count-1) {
        [self.backWardBtn setEnabled:YES];
        currentIndex=currentIndex+1;
        [[SongLibrary sharedLibrary] setCurrentIndex:currentIndex];
        [self playParticularSong:[self.songsList objectAtIndex:currentIndex]];
        
        if (currentIndex==self.songsList.count-1) {
            [self.forwardBtn setEnabled:NO];
        }
    }
}

-(IBAction)backwardBtnAction:(UIButton*)sender{
    if (self.songsList.count==0) {
        [self showAlert:@"You have no song in the library."];
        return;
    }
    
    NSUInteger currentIndex = [[SongLibrary sharedLibrary] currentIndex];
    
    if (currentIndex>0) {
        [self.forwardBtn setEnabled:YES];
        currentIndex=currentIndex-1;
        [[SongLibrary sharedLibrary] setCurrentIndex:currentIndex];
        [self playParticularSong:[self.songsList objectAtIndex:currentIndex]];
        
        if (currentIndex==0) {
            [self.backWardBtn setEnabled:NO];
        }
        
    }
}

-(IBAction)addToplayList:(UIButton*)sender{
    
    if (self.songsList.count==0) {
        [self showAlert:@"You have no song in the library."];
        return;
    }
    
  BOOL isExist=[[SongLibrary sharedLibrary]addSong:[self.songsList objectAtIndex:[[SongLibrary sharedLibrary]currentIndex]]];
    if (isExist) {
        [self showToast:@"Song already added to playlist"];
    }
    else{
        [self showToast:@"Song added to playlist"];
    }
}

-(IBAction)suffelsongsAction:(UIButton*)sender {
    if ([sender isSelected]) {
        [sender setSelected:NO];
        [self shuffle:NO ];
    }
    else {
        [sender setSelected:YES];
        [self shuffle:YES];
    }
}

-(IBAction)btnRepeatAction:(UIButton*)sender {
    
    if (self.songsList.count==0) {
        [self showAlert:@"You have no song in the library."];
        return;
    }
    SongLibrary *lib = [SongLibrary sharedLibrary];
    
    switch (lib.repeatType) {
        case SongRepeatNone: {
            lib.repeatType = SongRepeatAll;
            [sender setImage:[UIImage imageNamed:@"repeateall"] forState:UIControlStateNormal];
        }
            break;
        case SongRepeatAll: {
            lib.repeatType = SongRepeatOne;
            [sender setImage:[UIImage imageNamed:@"repeate_signleSong"] forState:UIControlStateNormal];
        }
            break;
        case SongRepeatOne: {
            lib.repeatType = SongRepeatNone;
            [sender setImage:[UIImage imageNamed:@"repeatoff"] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

- (void)dealloc {
    
    @try{
        [self.playerItem removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
        
    }
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    
    self.avplayer= nil;
}

#pragma UIButton Actions/

-(IBAction)moveToPlayListBtnAction:(UIButton*)sender{
    
    UINavigationController *nav=[self.storyboard instantiateViewControllerWithIdentifier:@"playlistNav"];
    PlayListVC *vc=(PlayListVC*)[nav topViewController];
    vc.Delegate=self;
    vc.currentIndex=[[SongLibrary sharedLibrary]currentIndex];
    [self presentViewController:nav animated:YES completion:nil];

}

-(void)didSelectSongFomPlayList:(PlayListVC *)controller songIndex:(NSUInteger)index{
    
    [[SongLibrary sharedLibrary] setCurrentIndex:index];
    self.songsList=[[NSMutableArray alloc]init];
    self.songsList=[[[SongLibrary sharedLibrary] allSongs] mutableCopy];
    
    
    if (index==self.songsList.count-1) {
        [self.forwardBtn setEnabled:NO];
    }
    else{
        [self.forwardBtn setEnabled:YES];

    }
    if (index==0) {
        [self.backWardBtn setEnabled:NO];
    }
    else{
        [self.backWardBtn setEnabled:YES];
        
    }
    [self playParticularSong:[self.songsList objectAtIndex:index]];

}

-(void)songLikeStatus:(NSString*)songId{
    
    NSString *userid=[NSString stringWithFormat:@"%@",[SBPreference sharedPreference].loggedInUserInfo[USER_ID]];


    NSString *url=[NSString stringWithFormat:@"%@/%@/%@",SERVICE_PATH_LIKE_SONG,songId,userid];
    
    [[SBHTTPClient sharedClient] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"found"]) {
            [self.likeSongBtn setSelected:YES];
            
        }
       
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       // [self showToast:error.localizedDescription];
    }];
}

#pragma buttonActions
-(void)likeSongAction:(UIButton*)sender{
    
    if ([sender isSelected]) {
        return;
    }
    if ([_songsList count]) {
        NSString *userid=[NSString stringWithFormat:@"%@",[SBPreference sharedPreference].loggedInUserInfo[USER_ID]];
        NSUInteger currentIndex = [[SongLibrary sharedLibrary] currentIndex];
        
        NSDictionary *songDict=[self.songsList objectAtIndex:currentIndex];
        
        self.view.userInteractionEnabled=NO;
        [[SBHTTPClient sharedClient] POST:SERVICE_PATH_LIKE_SONG parameters:@{@"userid":userid,@"songid":songDict[@"songid"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.view.userInteractionEnabled=YES;
            NSDictionary *resp = responseObject;
            if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"success"]) {
               // [self showToast:@"Like song completed"];
                [self.likeSongBtn setSelected:YES];
                self.likesCount+=1;
                self.likesCountLbl.text=[NSString stringWithFormat:@"%ld Likes",self.likesCount];
                
            }
            else{
                
                [self showToast:resp[@"message"]];
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self hideActivity];
            self.view.userInteractionEnabled=YES;
            
            [self showAlert:error.localizedDescription];
        }];
    }
}

-(void)moveToArtist:(UIButton*)sender{
    NSDictionary *dict = [self.songsList objectAtIndex:sender.tag];
    ArtistProfileVC *vc=(ArtistProfileVC*)[self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ArtistProfileVC class])];
    vc.artistId=dict[@"id"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)shuffle:(BOOL)shuffle {
    if (shuffle) {
        self.storedSongs=[self.songsList copy];
        NSUInteger count = [self.storedSongs count];
        for (NSUInteger i = 0; i < count; ++i) {
            NSInteger remainingCount = count - i;
            NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
            [self.songsList exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
        }
    }
    else {
        self.songsList = [self.storedSongs mutableCopy];
    }
}

#pragma mark call status

- (void)playBackAfterCallEnd:(NSNotification*)notification{
    
    //Check the type of notification, especially if you are sending multiple AVAudioSession events here
    if ([notification.name isEqualToString:AVAudioSessionInterruptionNotification]) {
        NSLog(@"Interruption notification received!");
        
        //Check to see if it was a Begin interruption
        if (self.songsList.count==0) {
            return;
        }
        if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:[NSNumber numberWithInt:AVAudioSessionInterruptionTypeBegan]]) {
            NSLog(@"Interruption began!");
            if (self.avplayer!=nil && self.avplayer.rate!=0) {
                _currentDuration = (int)CMTimeGetSeconds (self.avplayer.currentItem.currentTime);
                [_playAudioBtn setSelected:NO];
                [self.avplayer pause];
            }
        } else {
            NSLog(@"Interruption ended!");
            //Resume your audio
            [_playAudioBtn setSelected:YES];
            int32_t timeScale = self.avplayer.currentItem.asset.duration.timescale;
            CMTime time = CMTimeMakeWithSeconds(_currentDuration, timeScale);
            [self.avplayer seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
            [self.avplayer play];
        }
    }
}
@end
