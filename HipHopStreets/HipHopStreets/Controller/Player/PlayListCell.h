//
//  PlayListCell.h
//  MyMusic
//
//  Created by IOS on 02/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayListCell : UITableViewCell
@property (nonatomic ,strong) IBOutlet UIImageView *songImgView;
@property (nonatomic ,strong) IBOutlet UILabel *songName;
@property (nonatomic ,strong) IBOutlet UILabel *songDescription;

@property (nonatomic ,strong) IBOutlet UIImageView *audioImg;

@property (nonatomic ,strong) IBOutlet UIButton *moreOptionsBtn;


@end
