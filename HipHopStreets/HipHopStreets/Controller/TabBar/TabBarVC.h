//
//  TabBarVC.h
//  TabbarPrototype
//
//  Created by Soumen Bhuin on 12/17/16.
//  Copyright © 2016 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarVC : UITabBarController<UITabBarControllerDelegate>

@end
