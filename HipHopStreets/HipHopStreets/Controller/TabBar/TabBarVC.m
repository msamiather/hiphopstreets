//
//  TabBarVC.m
//  TabbarPrototype
//
//  Created by Soumen Bhuin on 12/17/16.
//  Copyright © 2016 Goigi. All rights reserved.
//

#import "TabBarVC.h"
#import "SBPreference.h"

@interface TabBarVC ()

@end

@implementation TabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                            fontWithName:@"Century Gothic" size:14], NSFontAttributeName,
                                 [UIColor lightGrayColor], NSForegroundColorAttributeName, nil];
    NSDictionary *attributes2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                            fontWithName:@"Century Gothic" size:14], NSFontAttributeName,
                                 [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    

    for (UITabBarItem *item in self.tabBar.items) {
        [item setTitleTextAttributes:attributes1 forState:(UIControlStateNormal)];
        [item setTitleTextAttributes:attributes2 forState:(UIControlStateSelected)];
    }
    
}


@end
