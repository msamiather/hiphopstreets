//
//  SplashVC.m
//  BarberiaMobile
//
//  Created by IOS on 05/10/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SplashVC.h"
#import "SBLoginVC.h"
#import "SBPreference.h"

@interface SplashVC ()

@property (nonatomic,strong) IBOutlet UIButton *btnGetStart;
@property (nonatomic,strong) NSTimer *timer;


@end

@implementation SplashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
   self.timer=[NSTimer scheduledTimerWithTimeInterval:23 target:self selector:@selector(moveToHome:) userInfo:nil repeats:NO];
    [self.view setMultipleTouchEnabled:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)moveToHome:(id)sender{//homenav
    
    SBLoginVC *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    
}

/*#pragma GestureRecognizer

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
        [self.timer invalidate];
        self.timer=nil;
    
        if ([[SBPreference sharedPreference] isUserLoggedIn]) {
            
            UIViewController *lvc = self.launchvc;
            UINavigationController *nav= [self.storyboard instantiateViewControllerWithIdentifier:@"homenav"];
            [[UIApplication sharedApplication].keyWindow setRootViewController:nav];
            
            if (lvc) {
                [nav presentViewController:self.launchvc animated:NO completion:nil];
            }
        }
        else{
            SBLoginVC *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
        }
}*/



@end
