//
//  SplashVC.h
//  BarberiaMobile
//
//  Created by IOS on 05/10/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSVideoViewController.h"

@interface SplashVC : JSVideoViewController

@property (nonatomic, strong) UIViewController *launchvc;

@end
