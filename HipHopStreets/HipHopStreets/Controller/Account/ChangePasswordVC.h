//
//  ChangePasswordVC.h
//  HipHopStreets
//
//  Created by IOS on 06/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChangePasswordVC;
@protocol ChangePasswordVCDelegate <NSObject>
-(void)didFinishChangePassword:(ChangePasswordVC*)controller;

@end

@interface ChangePasswordVC : UITableViewController
@property (strong, nonatomic) IBOutlet UITextField *oldPasswordTxt;
@property (strong, nonatomic) IBOutlet UITextField *PasswordNewTxt;
@property (strong, nonatomic) IBOutlet UITextField *PasswordNewConfirmTxt;
@property (strong, nonatomic) IBOutlet UIButton *btnChangePassword;

@property (nonatomic ,assign) id<ChangePasswordVCDelegate> delegate;

@end
