//
//  SBLoginVC.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/11/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBLoginVC : UITableViewController

@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet UIButton *btnRemember;
@property (strong, nonatomic) IBOutlet UIButton *btnForgotPassword;

@end
