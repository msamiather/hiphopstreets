//
//  ContactUsVC.m
//  Soshandy
//
//  Created by IOS on 15/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "ContactUsVC.h"
#import <MessageUI/MessageUI.h>
#import "SBExtensions.h"
#import "SBHTTPClient.h"
#import "SBPreference.h"

@interface ContactUsVC ()<MFMailComposeViewControllerDelegate,UITextFieldDelegate,UIWebViewDelegate>

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.submitBtn.layer.cornerRadius = self.submitBtn.layer.bounds.size.height/2;
    
    self.tableView.estimatedRowHeight = 300;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    // TextView Toolbar
    UIToolbar *toolBar=[[UIToolbar alloc]init];
    UIBarButtonItem *barbtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(texviewdonebtn:)];
    barbtn.tintColor=[UIColor blackColor];
    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolBar.items=[NSArray arrayWithObjects:negativeSeparator,barbtn, nil];
    [toolBar sizeToFit];
    [self.textViewMessage setInputAccessoryView:toolBar];
    
    
}


- (void)texviewdonebtn:(UIBarButtonItem*)sender {
    [self.view endEditing:YES];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}


#pragma mark - Button Actions


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


- (void)textFieldDidBeginEditing:(SBTextField *)textField {
    [textField setRightImage:nil];
}
//- (void)textFieldDidEndEditing:(UITextField *)textField{
//
//    if (textField==self.txtUserName) {
//        NSString *name = self.txtUserName.trimmedText;
//        
//        if (name.length>0) {
//            [self.txtUserName setRightImage:[UIImage imageNamed:@"Checkmark"]];
//        }
//        else {
//            [self.txtUserName setRightImage:[UIImage imageNamed:@"Wrongmark"]];
//        }
//        
//        [self.phoneTxt becomeFirstResponder];
//        
//    }
//    else if (textField==self.phoneTxt) {
//        NSString *phone = self.phoneTxt.trimmedText;
//        
//        if (phone.length>4 && phone.length<16 && [phone isContainsOnlyDigit]) {
//            [self.phoneTxt setRightImage:[UIImage imageNamed:@"Checkmark"]];
//        }
//        else {
//            [self.phoneTxt setRightImage:[UIImage imageNamed:@"Wrongmark"]];
//        }
//        
//        [self.emailTxt becomeFirstResponder];
//    }
//    else if (textField==self.emailTxt) {
//        NSString *mailid = self.emailTxt.trimmedText;
//        if (mailid && [mailid isValidEmail]) {
//            [self.emailTxt setRightImage:[UIImage imageNamed:@"Checkmark"]];
//        }
//        else {
//            [self.emailTxt setRightImage:[UIImage imageNamed:@"Wrongmark"]];
//        }
//        
//        [self.textViewMessage becomeFirstResponder];
//    }
//    
//
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==self.txtUserName) {
    
        [self.phoneTxt becomeFirstResponder];
        
    }
    else if (textField==self.phoneTxt) {
        
        [self.emailTxt becomeFirstResponder];
    }
    else if (textField==self.emailTxt) {
        [self.textViewMessage becomeFirstResponder];
    }
    
    return YES;
}

-(IBAction)btnSubmitAction:(UIBarButtonItem*)sender {

    [self.view endEditing:YES];
    
    NSString *name = self.txtUserName.trimmedText;
    NSString *phone = self.phoneTxt.trimmedText;
    NSString *mailid = self.emailTxt.trimmedText;
    NSString *bodyMessage = self.textViewMessage.trimmedText;
    
    //    Validate form
    NSString *errorString = nil;
    if (name.length==0) {
        errorString = @"Please enter your Name.";
    }
    else if (phone.length==0 || ![phone isValidPhoneNumber]) {
        errorString = @"Please enter valid phone number.";
    }
    else if (mailid.length==0 || ![mailid isValidEmail]) {
        errorString = @"Please enter valid email.";
    }
    else if (bodyMessage.length==0) {
        errorString = @"Please enter Message.";
    }
    if (errorString) {
        [self showToast:errorString];
        return;
    }

    NSMutableString *str = [[NSMutableString alloc] init];
    [str appendFormat:@"Name: %@\n",name];
    [str appendFormat:@"Phone Number: %@\n",phone];
    [str appendFormat:@"Email: %@\n",mailid];
    [str appendFormat:@"Message: %@\n",bodyMessage];
    
    NSString *emailTitle = @"Help Support";
    NSArray *toRecipents = [NSArray arrayWithObject:@"live@hiphopstreets.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:str isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    [self presentViewController:mc animated:YES completion:NULL];
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)backBtnAction:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
