//
//  ChangePasswordVC.m
//  HipHopStreets
//
//  Created by IOS on 06/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "SBExtensions.h"
#import "SBHTTPClient.h"
#import "SBPreference.h"
#import "UITheme.h"
#import "UIView+Toast.h"


@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.btnChangePassword setBackgroundImage:[[UIImage imageNamed:@"red3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
   
    
    UIImageView *imgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"register_bg"]];
    imgv.contentMode = UIViewContentModeScaleAspectFill;
    //imgv.alpha = 0.3f;
    self.tableView.backgroundView = imgv;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.oldPasswordTxt==textField) {
        [self.PasswordNewTxt becomeFirstResponder];
    }
   else if (self.PasswordNewTxt==textField) {
        [self.PasswordNewConfirmTxt becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return  YES;
}

#pragma mark - Actions

- (IBAction)btnChangePasswordBtn:(UIButton*)sender {
    [self.view endEditing:YES];
    
    NSString *oldPassword = self.oldPasswordTxt.trimmedText;
    NSString *newPassword = self.PasswordNewTxt.trimmedText;
    NSString *confriNewPassword = self.PasswordNewConfirmTxt.trimmedText;

    NSString *errorString = nil;
    if (oldPassword.length==0) {
        errorString = @"Password must be at least 8 characters in length.";
    }
    else if (newPassword.length==0) {
        errorString = @"New password must be at least 8 characters in length.";
    }
    else if (![newPassword isEqualToString:confriNewPassword]) {
        errorString = @"confirm new password should match with new password.";
    }
    
    if (errorString) {
        [self.btnChangePassword shakeOnCompletion:^{
            [self.view makeToast:errorString duration:1 position:CSToastPositionBottom style:nil];
        }];
        
        return;
    }
  
     //http://dev.goigi.biz/hiphop/api/userpass/
    
    NSString *userId=[SBPreference sharedPreference].loggedInUserInfo[USER_ID];
    
    self.view.userInteractionEnabled = NO;
    [self.btnChangePassword smb_showActivity:UIActivityIndicatorViewStyleWhite animated:YES offset:UIOffsetMake(-85, 0)];
    
    [[SBHTTPClient sharedClient] POST:SERVICE_PATH_CHANGE_PASSWORD parameters:@{@"uid":userId,@"old_pass":oldPassword,@"newpassword":newPassword} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.view.userInteractionEnabled = YES;
        [self.btnChangePassword smb_hideActivity];
        
        if ([responseObject isKindOfClass:[NSDictionary class]] && [(NSString *)responseObject[@"message"] containsString:@"Success"]) {
            
            [self showToast:@"successfully password updated."];
            
            if ([self.delegate respondsToSelector:@selector(didFinishChangePassword:)]) {
                [self.delegate didFinishChangePassword:self];
            }
            
            [self.navigationController popViewControllerAnimated:YES];

        }
        else {
            [self showAlert:@"Unble to update password try after some time."];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.view.userInteractionEnabled = YES;
        [self.btnChangePassword smb_hideActivity];
        [self showAlert:error.localizedDescription];
    }];
    
}


-(IBAction)backBtnAction:(UIBarButtonItem*)sender{

    [self.navigationController popViewControllerAnimated:YES];
}



@end
