//
//  SBLoginTextField.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/11/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBLoginTextField.h"
#import "UIColor+Additions.h"

@implementation SBLoginTextField

- (void)setRightImage:(UIImage *)rightImage {
    UIImage *image = [rightImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [super setRightImage:image];
    [(UIImageView *)self.rightView setContentMode:UIViewContentModeScaleAspectFit];
    [(UIImageView *)self.rightView setTintColor:[UIColor colorWithHex:0x9C0000]];
}

- (CGRect)borderRectForBounds:(CGRect)bounds {
    return bounds;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 0);
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 0);
}

- (CGRect)clearButtonRectForBounds:(CGRect)bounds {
    return [super clearButtonRectForBounds:bounds];
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    return [super leftViewRectForBounds:bounds];
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.size.width-15-10, (bounds.size.height-15)/2, 15, 15);
    return [super rightViewRectForBounds:bounds];
}

@end
