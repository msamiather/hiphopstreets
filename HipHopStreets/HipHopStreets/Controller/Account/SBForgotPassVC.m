//
//  ForgotPassVC.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 5/28/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBForgotPassVC.h"
#import "SBExtensions.h"
#import "SBHTTPClient.h"
#import "SBPreference.h"
#import "UITheme.h"
#import "UIView+Toast.h"

@interface SBForgotPassVC ()

@end


@implementation SBForgotPassVC

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect r = self.tableView.tableFooterView.frame;
    r.size.height = [UIScreen mainScreen].bounds.size.height - 622;
    self.tableView.tableFooterView.frame = r;
    
}

#pragma mark -

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return  YES;
}

#pragma mark - Actions

- (IBAction)btnContinesAction:(id)sender {
    [self.view endEditing:YES];
    
    NSString *eMail = self.txtEmail.trimmedText;

    // Validate form
    NSString *errorString = nil;
    
    if (eMail.length==0 || ![eMail isValidEmail]) {
        errorString = @"Please enter valid email.";
    }
    
    if (errorString) {
        [self.btnGetPassword shakeOnCompletion:^{
            [self.view makeToast:errorString duration:1 position:CSToastPositionBottom style:nil];
        }];
        
        return;
    }
    
    self.view.userInteractionEnabled = NO;
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:eMail forKey:@"email_id"];
    self.view.userInteractionEnabled = NO;
    [self.btnGetPassword smb_showActivity:UIActivityIndicatorViewStyleWhite animated:YES offset:UIOffsetMake(-70, 0)];
    
    [[SBHTTPClient sharedClient] POST:SERVICE_PATH_FORGOTPASSWORD parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.view.userInteractionEnabled = YES;
        [self.btnGetPassword smb_hideActivity];
        
        NSDictionary *resp = responseObject;
        if ([resp isKindOfClass:[NSDictionary class]] && [(NSString *)resp[@"status"] containsString:@"Send"]) {
            
            UIViewController *presentingVC = self.presentingViewController;
            [self dismissViewControllerAnimated:YES completion:nil];
             [presentingVC showAlert:resp[@"message"]];
        }
        
        else {
            [self showAlert:@"Incorrect email."];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.view.userInteractionEnabled = YES;
        [self.btnGetPassword smb_hideActivity];
        [self showAlert:error.localizedDescription];
    }];
}

- (IBAction)lblLoginHereAction:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnCloseAction:(UIButton *)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
