//
//  SBLoginVC.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/11/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBLoginVC.h"

#import "SBExtensions.h"
#import "SBHTTPClient.h"
#import "SBPreference.h"
#import "UITheme.h"
#import "UIView+Toast.h"
#import "TabBarVC.h"

#import "SongLibrary.h"

@interface SBLoginVC ()
@property (strong, nonatomic) UIView *focusedTextField;
@end

@implementation SBLoginVC

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setup Appearance
    _txtUsername.background = [[UIImage imageNamed:@"white3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
    _txtPassword.background = [[UIImage imageNamed:@"white3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
    [_btnLogin setBackgroundImage:[[UIImage imageNamed:@"red3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    [_btnRemember setImage:[[UIImage imageNamed:@"ic_check_box_outline_blank"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [_btnRemember setImage:[[UIImage imageNamed:@"ic_check_box"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    
    // Resize the table footer to align with screen bottom
    CGRect r = self.tableView.tableFooterView.frame;
    r.size.height = MAX([UIScreen mainScreen].bounds.size.height - 497, 50);
    self.tableView.tableFooterView.frame = r;
    
    // Background Image
    UIImageView *imgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"register_bg"]];
    imgv.contentMode = UIViewContentModeScaleAspectFill;
    self.tableView.backgroundView = imgv;
    
    // Setup Placeholder colors
    _txtUsername.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_txtUsername.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    _txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_txtPassword.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    // Enable tap to dismiss
    [self setTapToDismissKeyboardEnabled:YES];
    
    BOOL remember = [[NSUserDefaults standardUserDefaults] boolForKey:@"user_remember_account"];
    if (remember) {
        // Load saved values
        _txtUsername.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"user_mobile"];
        _txtPassword.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"user_password"];
        [_btnRemember setSelected:YES];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.focusedTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.focusedTextField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.txtUsername==textField) {
        [self.txtPassword becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return  YES;
}

#pragma mark - Actions


- (IBAction)btnLoginAction:(id)sender {
    [self.view endEditing:YES];
    
    NSString *username = self.txtUsername.trimmedText;
    NSString *password = self.txtPassword.trimmedText;
    
    // Validate form
    NSString *errorString = nil;
    if (username.length==0 || ![username isValidEmail]) {
        errorString = @"Please enter valid email.";
    }
    else if (password.length==0) {
        errorString = @"Please enter valid password.";
    }
    else if (password.length<8 ) {
        errorString = @"Password must be at least 8 characters in length.";
    }
    
    if (errorString) {
        [self.btnLogin shakeOnCompletion:^{
            [self.view makeToast:errorString duration:1 position:CSToastPositionBottom style:nil];
        }];
        
        return;
    }
    
    self.view.userInteractionEnabled = NO;
    [self.btnLogin smb_showActivity:UIActivityIndicatorViewStyleWhite animated:YES offset:UIOffsetMake(-50, 0)];
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    NSDictionary *params = @{@"email_id": username,
//                             @"password": password};
//    [manager POST:@"http://www.hiphopstreets.com/api/login" parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
    
    [[SBHTTPClient sharedClient] POST:SERVICE_PATH_LOGIN parameters:@{@"email_id":username,@"password":password} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        NSLog(@"%@", responseObject);
        
        self.view.userInteractionEnabled = YES;
        [self.btnLogin smb_hideActivity];
        
        NSDictionary *resp = responseObject;
        if ([resp isKindOfClass:[NSDictionary class]] && [(NSString *)resp[@"status"] containsString:@"success"]) {
            
            if ([self.btnRemember isSelected]) {
                [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"user_mobile"];
                [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"user_password"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"user_remember_account"];
            }
            
            SBPreference *pref = [SBPreference sharedPreference];
            [pref setLoggedInUserInfo:[resp[@"message"] dictionaryRemovingNullValues]];
            [pref setUserLoggedIn:YES];

            if ([pref.loggedInUserInfo[USERTYPE]isEqualToString:@"user"]) {
                TabBarVC *tabVC=(TabBarVC*)[self.storyboard instantiateViewControllerWithIdentifier:@"userTabBar"];
                [[SongLibrary sharedLibrary] setTabBarController:tabVC];
                [[[UIApplication sharedApplication] keyWindow]setRootViewController:tabVC];
            }
            else{
                TabBarVC *tabVC=(TabBarVC*)[self.storyboard instantiateViewControllerWithIdentifier:@"artistTabBar"];
                [[SongLibrary sharedLibrary] setTabBarController:tabVC];
                [[[UIApplication sharedApplication] keyWindow]setRootViewController:tabVC];

            }
        }
        else {
            [self showAlert:@"Incorrect email or password."];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        self.view.userInteractionEnabled = YES;
        [self.btnLogin smb_hideActivity];
        
        NSDictionary *resp = [[SBHTTPClient sharedClient] responseObjectForError:error];
        if ([resp isKindOfClass:[NSDictionary class]]) {
            NSString *msg = resp[@"status"];
            if (msg) {
                [self showAlert:@"Incorrect email or password."];
            }
            else {
                [self showAlert:error.localizedDescription];
            }
        }
        else {
           [self showAlert:error.localizedDescription];
        }
    }];
    
}

- (IBAction)btnRememberAction:(UIButton *)sender {
    [sender setSelected:!(sender.selected)];
}


- (IBAction)btnRegisterAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"registerNav"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)btnForgotPassAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"forgotpass"];
    [self presentViewController:vc animated:YES completion:nil];
}
 


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


