//
//  SBRegistrationPhotoButton.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/11/16.
//  Copyright © 2016 SMB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBRegistrationPhotoButton : UIButton
@property (nonatomic, strong) UIImage *selectedPhoto;

- (void)setPhoto:(UIImage *)image;

@end
