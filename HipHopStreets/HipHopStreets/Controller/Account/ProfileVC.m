//
//  ProfileVC.m
//  Leeseeme
//
//  Created by IOS on 28/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "ProfileVC.h"
#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"
#import "UIKit+AFNetworking.h"

#import "NavVC.h"
#import "SBLoginVC.h"
#import "UploadedSongListVC.h"
#import "ChangePasswordVC.h"
#import "EditProfileVC.h"
#import "SBWebViewController.h"
#import <AVFoundation/AVFoundation.h>

#import "AudioPlayerVC.h"


@interface ProfileVC ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate,EditProfileVCDelegate,ChangePasswordVCDelegate>

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _profileImgIcon.image = [_profileImgIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _profileImgIcon.tintColor=[UIColor whiteColor];
    
    self.btnEdit.layer.cornerRadius=3.0f;
    self.btnEdit.layer.borderWidth=1.0f;
    self.btnEdit.layer.borderColor=[[UIColor blackColor]CGColor];
    self.btnEdit.clipsToBounds=YES;
    
    NSDictionary *dic =[[SBPreference sharedPreference] loggedInUserInfo];
    
    
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_IMAGES,dic[PHOTO]];
    [self.profileImage setImageWithURL:[NSURL URLWithString:imageUrl]];
    self.lblName.text = dic[NAME];
    self.emaiIdLbl.text=dic[EMAIL];
    
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithSize:CGSizeMake(10, 64) color:[UIColor clearColor]]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage imageWithSize:CGSizeMake(10, 1) color:[UIColor clearColor]];
//}


#pragma mark - Table view data source

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section==1 && indexPath.row==0) {
        
        NavVC *nav=(NavVC*)[[self.tabBarController viewControllers] objectAtIndex:2];
        AudioPlayerVC *avc=(AudioPlayerVC*)[nav topViewController];
        if ([avc isKindOfClass:[AudioPlayerVC class]] && avc.avplayer!=nil && avc.avplayer.rate!=0) {
            [avc.playerItem removeObserver:avc forKeyPath:@"status"];
            avc.avplayer=nil;
        }
        [[SBPreference sharedPreference]setLoggedInUserInfo:nil];
        [[SBPreference sharedPreference]setUserLoggedIn:NO];
        SBLoginVC *vc =[self.storyboard instantiateViewControllerWithIdentifier:@"SBLoginVC"];
        [[[UIApplication sharedApplication]keyWindow ]setRootViewController:vc];
        
    }
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case 0:{
                EditProfileVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
                vc.delegate=self;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
                
            case 1:{
                ChangePasswordVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordVC"];
                vc.delegate=self;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
                
            case 3:{
                NSString *path = [[NSBundle mainBundle] pathForResource:@"AboutUs" ofType:@"pdf"];
                NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]];
                SBWebViewController *vc = [[SBWebViewController alloc] initWithRequest:req];
                vc.title = @"About Us";
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            default:
                break;
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    if (picker.view.tag==0) {
        self.profileImage.image=[image imageScaledToFitSize:CGSizeMake(260, 260)];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker1 {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)editProfileImg:(UIButton*)sender{
    
    if (self.profileImage.image==[UIImage imageNamed:@"usericon"]) {
        [self showToast:@"Plese choose profile image"];
        return;
    }
    NSString *userId =[[SBPreference sharedPreference]loggedInUserInfo][USER_ID];

    [self showActivity:nil];
    [[SBHTTPClient sharedClient] POST:SERVICE_UPDATE_PROFILE_IMAGE parameters:@{@"id":userId} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        if (self.profileImage.image) {
            NSData *documentImgdata = UIImageJPEGRepresentation(self.profileImage.image, 0.5);
            [formData appendPartWithFileData:documentImgdata name:@"pic" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
            
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        NSDictionary *resp = responseObject;
        if ([resp isKindOfClass:[NSDictionary class]] && [(NSString *)resp[@"status"]  isEqualToString:@"success"]) {
            [self showToast:@"Successfylly Registered"];
            
        }
        else {
            [self showAlert:@"Unable to register, Please check input fields."];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self hideActivity];
        [self showAlert:error.localizedDescription];
    }];

}

-(void)didFinishUpdatingProfile:(EditProfileVC *)controller{
    [self showToast:@"Successfully Profile Updated"];

    NSDictionary *dic =[[SBPreference sharedPreference] loggedInUserInfo];
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_IMAGES,dic[PHOTO]];
    [self.profileImage setImageWithURL:[NSURL URLWithString:imageUrl]];
    self.lblName.text = dic[NAME];
    self.emaiIdLbl.text=dic[EMAIL];
    
}

-(void)didFinishChangePassword:(ChangePasswordVC *)controller{
    [self showToast:@"Password Successfully changed"];

}
@end
