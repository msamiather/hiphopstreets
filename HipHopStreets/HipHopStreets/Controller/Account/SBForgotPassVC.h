//
//  ForgotPassVC.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 5/28/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBForgotPassVC : UITableViewController

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnGetPassword;

@end
