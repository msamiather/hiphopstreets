//
//  SBLoginTextField.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/11/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBRegistrationTextField.h"
#import "UIColor+Additions.h"

@implementation SBRegistrationTextField

- (void)setLeftImage:(UIImage *)leftImage {
    UIImage *image = [leftImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [super setLeftImage:image];
    [(UIImageView *)self.leftView setContentMode:UIViewContentModeScaleAspectFit];
    [(UIImageView *)self.leftView setTintColor:[UIColor colorWithHex:0x9C0000]];
}

- (CGRect)borderRectForBounds:(CGRect)bounds {
    return bounds;
}


- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect r = CGRectInset(bounds, 20, 0);
    r.origin.x +=20;
    return r;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    CGRect r = CGRectInset(bounds, 20, 0);
    r.origin.x +=20;
    return r;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect r = CGRectInset(bounds, 20, 0);
    r.origin.x +=20;
    return r;
}

- (CGRect)clearButtonRectForBounds:(CGRect)bounds {
    return [super clearButtonRectForBounds:bounds];
}

 
- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x+10, (bounds.size.height-15)/2, 15, 15);
    return [super leftViewRectForBounds:bounds];
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds {
    return [super rightViewRectForBounds:bounds];
}

@end
