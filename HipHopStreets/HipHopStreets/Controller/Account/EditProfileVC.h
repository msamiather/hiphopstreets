//
//  EditProfileVC.h
//  HipHopStreets
//
//  Created by IOS on 06/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBFaceImageView.h"
#import "RadioButton.h"
@class EditProfileVC;
@protocol EditProfileVCDelegate <NSObject>

-(void)didFinishUpdatingProfile:(EditProfileVC*)controller;

@end

@interface EditProfileVC : UITableViewController
@property (nonatomic,strong)IBOutlet SBFaceImageView *profileImage;
@property (strong, nonatomic) IBOutlet UITextField *nameTxt;
@property (strong, nonatomic) IBOutlet UILabel *emailLbl;

@property (strong, nonatomic) IBOutlet UIButton *btnChangeProfile;

@property (strong ,nonatomic) IBOutlet RadioButton *maleBtn;
@property (strong ,nonatomic) IBOutlet RadioButton *femaleBtn;
@property (strong ,nonatomic) IBOutlet UITextView *biographyTxtView;
@property (nonatomic ,assign) id<EditProfileVCDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblCharsCount;


@end
