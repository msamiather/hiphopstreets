//
//  SBRegistrationPhotoButton.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/11/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBRegistrationPhotoButton.h"
#import "UIColor+Additions.h"

@interface SBRegistrationPhotoButton ()
@end

@implementation SBRegistrationPhotoButton

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        UIImage *img = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self setImage:img forState:UIControlStateNormal];
        self.tintColor=[UIColor lightGrayColor];
        
    }
    return self;
}

- (CGRect)backgroundRectForBounds:(CGRect)bounds {
    return [super backgroundRectForBounds:bounds];
}

- (CGRect)contentRectForBounds:(CGRect)bounds {
    return [super contentRectForBounds:bounds];
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    return CGRectMake(contentRect.origin.x, contentRect.origin.y+contentRect.size.height-40, contentRect.size.width, 40);
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    if (self.selectedPhoto) {
        return CGRectInset(contentRect, 5, 2);
    }
    return CGRectMake(contentRect.origin.x, contentRect.origin.y, contentRect.size.width, contentRect.size.height-40);
}

- (void)setPhoto:(UIImage *)image {
    self.selectedPhoto = image;
    [self setImage:image forState:UIControlStateNormal];
    [self setTitle:nil forState:UIControlStateNormal];
    self.imageView.layer.cornerRadius = 5.0f;
    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
}

@end
