//
//  EditProfileVC.m
//  HipHopStreets
//
//  Created by IOS on 06/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "EditProfileVC.h"
#import "SBExtensions.h"
#import "SBHTTPClient.h"
#import "SBPreference.h"
#import "UITheme.h"
#import "UIView+Toast.h"

#import "RSKImageCropper.h"


#define MAX_WORD_COUNT 60

@interface EditProfileVC ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate,RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource>
@property (nonatomic ,strong) UIImage *songImg;
@property (nonatomic, assign) NSInteger wordCount;


@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _wordCount=0;
    
     [self.btnChangeProfile setBackgroundImage:[[UIImage imageNamed:@"red3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    
     [_profileImage setGestureTarget:self action:@selector(uploadImg)];
    
    NSDictionary *dic =[[SBPreference sharedPreference] loggedInUserInfo];
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_IMAGES,dic[PHOTO]];
    [self.profileImage setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"usericon"]];
    self.nameTxt.text=dic[NAME];
    self.emailLbl.text=dic[EMAIL];
    self.biographyTxtView.text=dic[BIOGRAPHY];
    
    if (_biographyTxtView.text.length!=0) {
        NSArray *words = [_biographyTxtView.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _wordCount = [words count];
        _lblCharsCount.text = [NSString stringWithFormat:@"%lu words left",(unsigned long)(MAX_WORD_COUNT-_wordCount)];
    }

    [self textfieldToolBar:self.biographyTxtView];
    
    if ([dic[GENDER] isEqualToString:@"Male"]) {
        [self.maleBtn setSelected:YES];
    }
    else {
        [self.femaleBtn setSelected:YES];
    }
}

-(void)textfieldToolBar:(UITextView*)txtfld{
    
    UIToolbar *toolBar=[[UIToolbar alloc]init];
    toolBar.translucent = NO;
    UIBarButtonItem *barbtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(texviewdonebtn:)];
    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolBar.items=@[negativeSeparator,barbtn];
    [toolBar sizeToFit];
    [txtfld setInputAccessoryView:toolBar];
}

- (void)texviewdonebtn:(UIBarButtonItem*)sender{
    [self.view endEditing:YES];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return  YES;
}

#pragma TextView Delegates
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if (text.length==0) {
        return YES;
    }
    
    if (_wordCount<MAX_WORD_COUNT) {
        return YES;
    }
    return NO;
}
- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length!=0) {
        _wordCount =[[[textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]componentsSeparatedByString:@" "] count];
        _lblCharsCount.text = [NSString stringWithFormat:@"%lu words left",(unsigned long)(MAX_WORD_COUNT-_wordCount)];
    }
    else{
        _lblCharsCount.text =@"60 words left";

    }
}

#pragma TableView Delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    SBPreference *pref = [SBPreference sharedPreference];
    if ([pref.loggedInUserInfo[USERTYPE]isEqualToString:@"user"]) {
        return 3;
    }
    return 4;
    
}

#pragma imagePickerController

- (void)uploadImg {
    UIAlertController *con = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [con addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [con addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
        }
    }]];
    [con addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    }]];
    
    [self presentViewController:con animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeCustom];
    imageCropVC.delegate = self;
    imageCropVC.dataSource = self;
    [picker pushViewController:imageCropVC animated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker1 {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -

// Crop image has been canceled.
- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller {
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
}

// The original image has been cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect {
    
    self.profileImage.image = croppedImage; //[croppedImage imageScaledToFitSize:CGSizeMake(320, 204)];
    self.songImg=croppedImage;
    
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    //Upload the image
}

// Returns a custom rect for the mask.
- (CGRect)imageCropViewControllerCustomMaskRect:(RSKImageCropViewController *)controller
{
    CGSize maskSize = CGSizeMake(360, 360);
    
    CGFloat viewWidth = CGRectGetWidth(controller.view.frame);
    CGFloat viewHeight = CGRectGetHeight(controller.view.frame);
    
    CGRect maskRect = CGRectMake((viewWidth - maskSize.width) * 0.5f,
                                 (viewHeight - maskSize.height) * 0.5f,
                                 maskSize.width,
                                 maskSize.height);
    
    return maskRect;
}

// Returns a custom path for the mask.
- (UIBezierPath *)imageCropViewControllerCustomMaskPath:(RSKImageCropViewController *)controller
{
    
    UIBezierPath *triangle = [UIBezierPath bezierPathWithRect:controller.maskRect];
    
    
    return triangle;
}

// Returns a custom rect in which the image can be moved.
- (CGRect)imageCropViewControllerCustomMovementRect:(RSKImageCropViewController *)controller
{
    // If the image is not rotated, then the movement rect coincides with the mask rect.
    return controller.maskRect;
}



#pragma mark - Actions

- (IBAction)btnChangePasswordBtn:(UIButton*)sender {
    [self.view endEditing:YES];
    
    NSString *name = self.nameTxt.trimmedText;
    NSString *bioGraphy=self.biographyTxtView.trimmedText;
    NSString *gender=@"";
    if ([self.maleBtn isSelected]) {
        gender=@"Male";
    }
    if ([self.femaleBtn isSelected]) {
        gender=@"Female";
    }
    
    NSString *errorString = nil;
    if (name.length==0) {
        errorString = @"Please enter first name.";
    }
    else if (gender.length==0) {
        errorString = @"Please select your gender.";
    }
    
    else if (self.profileImage.image==[UIImage imageNamed:@"usericon"])
    {
        errorString = @"Plese choose profile image";
    }
    
    
    if (errorString) {
        [self.btnChangeProfile shakeOnCompletion:^{
            [self.view makeToast:errorString duration:1 position:CSToastPositionBottom style:nil];
        }];
        return;
    }
    
//http://dev.goigi.biz/hiphop/api/userprofileupdate/
        
    NSString *userId=[SBPreference sharedPreference].loggedInUserInfo[USER_ID];
    
    [self showActivity:nil];
    
    [[SBHTTPClient sharedClient] POST:SERVICE_PATH_EDIT_PROFILE parameters:@{@"uid":userId,@"name":name,@"sex":gender,@"biography":bioGraphy} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (self.songImg) {
            NSData *documentImgdata = UIImageJPEGRepresentation(self.songImg, 0.5);
            [formData appendPartWithFileData:documentImgdata name:@"pic" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        }
        }
        progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        if ([responseObject isKindOfClass:[NSDictionary class]] && [(NSString *)responseObject[@"status"] containsString:@"successfully updated"]) {
           
            [[SBPreference sharedPreference] setLoggedInUserInfo:responseObject[@"message"]];
            [[SBPreference sharedPreference] setUserLoggedIn:YES];

    
            if ([self.delegate respondsToSelector:@selector(didFinishUpdatingProfile:)]) {
                [self.delegate didFinishUpdatingProfile:self];
            }

            [self.navigationController popViewControllerAnimated:YES];

        }
        else {
            [self showAlert:@"Unble to update profile, try after some time"];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        [self showAlert:error.localizedDescription];
    }];
    
}


-(IBAction)backBtnAction:(UIBarButtonItem*)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}



@end
