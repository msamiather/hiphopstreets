//
//  SBRegistrationVC.h
//  SBLibrary
//
//  Created by IOS on 11/11/16.
//  Copyright © 2016 SB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioButton.h"
@class SBRegistrationPhotoButton;

@interface SBRegistrationVC : UITableViewController

@property (strong, nonatomic) IBOutlet SBRegistrationPhotoButton *imgBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UIImageView *bgImgtxtfield;


@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtConfirmPassword;

@property (strong, nonatomic) IBOutlet UIButton *btnCreateAccount;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;


@property (strong ,nonatomic) IBOutlet RadioButton *maleBtn;
@property (strong ,nonatomic) IBOutlet RadioButton *femaleBtn;


@end
