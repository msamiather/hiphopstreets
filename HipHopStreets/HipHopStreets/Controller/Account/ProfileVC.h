//
//  ProfileVC.h
//  Leeseeme
//
//  Created by IOS on 28/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBFaceImageView.h"

@interface ProfileVC : UITableViewController

@property (nonatomic,strong)IBOutlet UIButton *btnEdit;
@property (nonatomic,strong)IBOutlet UIImageView *profileImgIcon;
@property (nonatomic,strong)IBOutlet UILabel *lblName;
@property (nonatomic,strong)IBOutlet SBFaceImageView *profileImage;

@property (nonatomic,strong)IBOutlet UILabel *emaiIdLbl;
@end
