//
//  ArtistRegistrationVC.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/11/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "ArtistRegistrationVC.h"
#import "SBLoginVC.h"

#import "SBExtensions.h"
#import "SBHTTPClient.h"
#import "SBPreference.h"
#import "UITheme.h"
#import "UIView+Toast.h"
#import "SBRegistrationPhotoButton.h"
#import "SBWebViewController.h"

@interface ArtistRegistrationVC () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (strong, nonatomic) UIView *focusedTextField;
@property (nonatomic ,strong) NSString *gengerString;

@end

@implementation ArtistRegistrationVC

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    //[_btnLogin setBackgroundImage:[[UIImage imageNamed:@"red3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];

    // Resize the table footer to align with screen bottom
    CGRect r = self.tableView.tableFooterView.frame;
    r.size.height = MAX([UIScreen mainScreen].bounds.size.height - 584, 50);
    self.tableView.tableFooterView.frame = r;
    
    //_imgUser.image = [[UIImage imageNamed:@"white3dbg"]resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20) resizingMode:UIImageResizingModeStretch];
    //_bgImgtxtfield.image = [[UIImage imageNamed:@"white3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20) resizingMode:UIImageResizingModeStretch];
    //_txtPhoneNumber.background =[[UIImage imageNamed:@"white3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];

    _txtName.background = [[UIImage imageNamed:@"white3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
    _txtEmail.background = [[UIImage imageNamed:@"white3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
    _txtPassword.background =[[UIImage imageNamed:@"white3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
    _txtConfirmPassword.background = [[UIImage imageNamed:@"white3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
    [_btnCreateAccount setBackgroundImage:[[UIImage imageNamed:@"red3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    
    
    _txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_txtName.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    //_txtLastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_txtLastName.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    //_txtPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_txtPhoneNumber.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];

    _txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_txtEmail.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    _txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_txtPassword.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    _txtConfirmPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_txtConfirmPassword.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    [self setTapToDismissKeyboardEnabled:YES];
    
    UIImageView *imgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"register_bg"]];
    imgv.contentMode = UIViewContentModeScaleAspectFill;
    //imgv.alpha = 0.3f;
    self.tableView.backgroundView = imgv;
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    [self.imgBtn setPhoto:image];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker1 {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.focusedTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.focusedTextField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.txtName==textField) {
        [self.txtEmail becomeFirstResponder];
    }
    else if (self.txtEmail==textField) {
        [self.txtPassword becomeFirstResponder];
    }
    else if (self.txtPassword==textField) {
        [self.txtConfirmPassword becomeFirstResponder];
    }
    else if (self.txtConfirmPassword==textField) {
        [self.txtConfirmPassword resignFirstResponder];
    }
   
    
    return  YES;
}

#pragma mark - Actions


- (IBAction)btnRegisterAction:(id)sender {
    
    [self.view endEditing:YES];

    NSString *name = self.txtName.trimmedText;
    NSString *eMail = self.txtEmail.trimmedText;
    NSString *password = self.txtPassword.trimmedText;
    NSString *conpassword = self.txtConfirmPassword.trimmedText;
    NSString *gender=@"";
    
    if ([self.maleBtn isSelected]) {
        gender=@"Male";
    }
    else if ([self.femaleBtn isSelected]) {
        gender=@"Female";
    }
    else {
        gender=@"Male";
    }

    // Validate form
    NSString *errorString = nil;
    if (name.length==0) {
        errorString = @"Please enter your full name.";
    }

    else if (eMail.length==0 || ![eMail isValidEmail]) {
        errorString = @"Please enter valid email.";
    }
    else if (password.length<8 ) {
        errorString = @"The password field must be at least 8 characters in length.";
    }
    else if(![password isEqualToString:conpassword]) {
        errorString = @"Confirm password not matching with password.";
    }
    
    else if (gender.length==0){
        errorString = @"Please select your gender.";
    
    }
    else if (![self.chekInBtn isSelected]){
        errorString = @"Please Agree the Terms and conditions.";

    }

    if (errorString) {
        [self.btnCreateAccount shakeOnCompletion:^{
            [self.view makeToast:errorString duration:1 position:CSToastPositionBottom style:nil];
        }];
        
        return;
    }
    
    
    self.view.userInteractionEnabled = NO;
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:name forKey:@"name"];
    [params setObject:eMail forKey:@"email_id"];
    [params setObject:password forKey:@"password"];
    [params setObject:gender forKey:@"sex"];
    [params setObject:@"artist" forKey:@"user_cat"];

    self.view.userInteractionEnabled = NO;
    [self.btnCreateAccount smb_showActivity:UIActivityIndicatorViewStyleWhite animated:YES offset:UIOffsetMake(-70, 0)];

    [[SBHTTPClient sharedClient] POST:SERVICE_PATH_REGISTRATION parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        if (self.imgBtn.selectedPhoto) {
//            NSData *documentImgdata = UIImageJPEGRepresentation(self.imgBtn.selectedPhoto, 0.5);
//            [formData appendPartWithFileData:documentImgdata name:@"pic" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
//            
//        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.view.userInteractionEnabled = YES;
        [self.btnCreateAccount smb_hideActivity];
        NSDictionary *resp = responseObject;
        if ([resp isKindOfClass:[NSDictionary class]] && [(NSString *)resp[@"status"]  isEqualToString:@"success"]) {
            
            UIViewController *vc = self.presentingViewController;
            
            while (vc.presentingViewController) {
                vc = vc.presentingViewController;
                [vc dismissViewControllerAnimated:YES completion:nil];
                
            }
            [vc showAlert:@"Successfully Registered"];
       

        }
        else {
            [self showAlert:@"Unable to register, Please check input fields."];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.view.userInteractionEnabled = YES;
        [self.btnCreateAccount smb_hideActivity];
        NSDictionary *resp = [[SBHTTPClient sharedClient] responseObjectForError:error];
        if ([resp isKindOfClass:[NSDictionary class]]){
            NSString *msg = resp[@"status"];
            if ([msg isEqualToString:@"failure"]) {
                [self showAlert:@"The email field must contain a unique value"];
            }
        }
        else {
            [self showAlert:error.localizedDescription];
        }
    }];
}


- (IBAction)btnUserPhotoAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    UIAlertController *con = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [con addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    UIAlertAction *act1 = [UIAlertAction actionWithTitle:@"Photo from camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.delegate = self;
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }];
    [con addAction:act1];
    
    UIAlertAction *act2 = [UIAlertAction actionWithTitle:@"Photo from album" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    [con addAction:act2];
    
    [self presentViewController:con animated:YES completion:nil];
    
    con.view.tintColor = [UIColor blackColor];
}

#pragma buttonActions

- (IBAction)btnCloseAction:(UIButton *)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnLoginAction:(UIButton *)sender {
    [self.view endEditing:YES];
    UIViewController *vc = self.presentingViewController;

    while (vc.presentingViewController) {
        vc = vc.presentingViewController;
        [vc dismissViewControllerAnimated:YES completion:nil];

    }
}

-(IBAction)cancelBtnAction:(UIBarButtonItem*)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)checkTermsAndCondtionsAction:(UIButton*)sender{
    
    if ([sender isSelected ]) {
        [sender setSelected:NO];
    }
    else{
        [sender setSelected:YES];

    }
}

-(IBAction)termsAndCondtionsBtnAction:(UIButton*)sender{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"TermsAndCondtions" ofType:@"pdf"];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]];
    SBWebViewController *vc = [[SBWebViewController alloc] initWithRequest:req];
    vc.title = @"Terms of Service";
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
    
}
@end
