//
//  SBForgotPassTextField.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 1/13/17.
//  Copyright © 2017 goigi. All rights reserved.
//

#import "SBForgotPassTextField.h"

@implementation SBForgotPassTextField

- (CGRect)textRectForBounds:(CGRect)bounds; {
    return CGRectInset(bounds, 10, 0);
}
- (CGRect)placeholderRectForBounds:(CGRect)bounds; {
    return CGRectInset(bounds, 10, 0);
}
- (CGRect)editingRectForBounds:(CGRect)bounds; {
    return CGRectInset(bounds, 10, 0);
}

@end
