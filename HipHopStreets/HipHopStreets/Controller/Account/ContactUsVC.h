//
//  ContactUsVC.h
//  Soshandy
//
//  Created by IOS on 15/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTextView.h"
#import "SBTextField.h"
@class SBTextField, SBTextView;

@interface ContactUsVC : UITableViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet SBTextView *textViewMessage;
@property (strong, nonatomic) IBOutlet SBTextField *txtUserName;
@property (strong, nonatomic) IBOutlet SBTextField *phoneTxt;
@property (strong, nonatomic) IBOutlet SBTextField *emailTxt;

@property (strong, nonatomic) IBOutlet UIButton *submitBtn;

@end
