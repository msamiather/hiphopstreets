//
//  ArtistProfileVC.m
//  HipHopStreets
//
//  Created by IOS on 29/12/16.
//  Copyright © 2016 Goigi. All rights reserved.
//

#import "ArtistProfileVC.h"
#import "ArtistProfileCell.h"

#import "TableHeaderFooterView.h"

#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"
#import "UIKit+AFNetworking.h"

#import "ArtistHeaderView.h"
#import "Song.h"
#import "Song.h"
#import "SongLibrary.h"
#import "User.h"

@interface ArtistProfileVC ()
@property (nonatomic ,strong) NSMutableArray *artistSongs;
@property (nonatomic ,strong) NSDictionary *artistProfileInfo;
@property (nonatomic, assign) CGSize screenSize;
@property (nonatomic, assign) NSInteger followCount;

@end

@implementation ArtistProfileVC
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _screenSize = [[UIScreen mainScreen] bounds].size;
    [self fetchArtistDetails];
}

-(void)fetchArtistDetails{

    [self showActivity:nil];
    
    NSString *url=[NSString stringWithFormat:@"%@/%@",SERVICE_PATH_ARTIST_PROFILE,self.artistId];
    
    [[SBHTTPClient sharedClient] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"success"]) {
            self.artistSongs=[[NSMutableArray alloc]init];
            self.artistProfileInfo=[resp[@"message"] dictionaryRemovingNullValues];
            
            id arr=[self.artistProfileInfo nonNullObjectForKey:@"song"];
            if ([arr isKindOfClass:[NSArray class]]) {
                User *art=[[User alloc]init];
                art.userId=self.artistProfileInfo[@"id"];
                art.name=self.artistProfileInfo[@"name"];
                art.emailId=self.artistProfileInfo[@"email_id"];
                art.imgUrl=self.artistProfileInfo[@"pic"];
                art.gender=self.artistProfileInfo[@"sex"];
                art.regDate=self.artistProfileInfo[@"regddate"];
                
                for (NSDictionary *d in arr) {
                    NSDictionary *dict=[d dictionaryRemovingNullValues];
                    Song *sn=[[Song alloc]init];
                    sn.songName=dict[@"song_name"];
                    sn.songId=dict[@"songid"];
                    sn.songImgUrl=dict[@"song_image"];
                    sn.songFile=dict[@"song_file"];
                    //sn.likeCount=dict[@"likecount"];
                    sn.category=dict[@"song_category"];
                    sn.likeCount=dict[@"likecount"];
                    sn.user=art;
                    [self.artistSongs addObject:sn];
                }
            }
        }
        else{
            self.artistSongs=nil;
        }
        
        [self.collectionView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        [self showToast:error.localizedDescription];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
   
    return self.artistSongs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Song *sn=self.artistSongs[indexPath.row];
    
    ArtistProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ArtistProfileCell class]) forIndexPath:indexPath];
    [cell.songImgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,sn.songImgUrl]] placeholderImage:[UIImage imageNamed:@"songPlacehodler"]];
    
    cell.songCategory.text=sn.category;
    cell.songName.text=sn.songName;
    return cell;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        ArtistHeaderView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ArtistHeaderView" forIndexPath:indexPath];
        view.artistName.text=self.artistProfileInfo[@"name"];
        
        [view.artistImgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_IMAGES,self.artistProfileInfo[@"pic"]]] placeholderImage:[UIImage imageNamed:@"usericon"]];
        [view.btnSongCount setTitle:[NSString stringWithFormat:@"%lu Songs",self.artistSongs.count] forState:UIControlStateNormal];
        
        self.followCount=[self.artistProfileInfo[@"followcount"] integerValue];
        
        if ([self.artistProfileInfo[@"followcount"] integerValue]==0) {
            view.followers.text=[NSString stringWithFormat:@"No Followers"];
        }
        else if ([self.artistProfileInfo[@"followcount"] integerValue]==1){
            view.followers.text=[NSString stringWithFormat:@"Artist:%@ Follower",self.artistProfileInfo[@"followcount"]];

        }
        else{
            view.followers.text=[NSString stringWithFormat:@"Artist:%@ Followers",self.artistProfileInfo[@"followcount"]];
        }
        
        [view.btnFollow addTarget:self action:@selector(followArtist) forControlEvents:UIControlEventTouchUpInside];
        [view.btnOptions addTarget:self action:@selector(artistBiography) forControlEvents:UIControlEventTouchUpInside];

        return view;
    }

    else{
        return nil;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (_screenSize.width-28)/2;
    return CGSizeMake(width, width+50);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10,10,10,10);//top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Song *sn=self.artistSongs[indexPath.row];
    
    [[SongLibrary sharedLibrary] addSong:[sn crateDictionary]];
    [self.navigationController popViewControllerAnimated:YES];
    //[[SongLibrary sharedLibrary] showPlayerAndPlaySong:[sn crateDictionary]];
    
    [[SongLibrary sharedLibrary] showPlayerAndPlaySong:[sn createArryWithFromModel:self.artistSongs] repeatType:SongRepeatAll songIdex:indexPath.item type:NO];
    
}


#pragma ButtonActions

-(IBAction)backBtnAction:(UIBarButtonItem*)sender{

    [self.navigationController popViewControllerAnimated:YES];

}

-(void)artistBiography{
    
    UIAlertController *alrt=[UIAlertController alertControllerWithTitle:@"Biography" message:self.artistProfileInfo[@"biography"] preferredStyle:UIAlertControllerStyleAlert];
    [alrt addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alrt dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alrt animated:YES completion:nil];
}

-(void)followArtist{
    
    ArtistHeaderView *view=(ArtistHeaderView*)[self.collectionView supplementaryViewForElementKind:UICollectionElementKindSectionHeader atIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    
    [self showActivity:nil];
    [[SBHTTPClient sharedClient] POST:SERVICE_PATH_FOLLOW_ARTIST parameters:@{@"user_id":[SBPreference sharedPreference].loggedInUserInfo[@"id"],@"follower_id":self.artistId} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"success"]) {
            [self showToast:[NSString stringWithFormat:@"Now you are following %@",self.artistProfileInfo[@"name"]]];
            self.followCount+=1;
            view.followers.text=[NSString stringWithFormat:@"Artist:%ld Followers",self.followCount];

        }
        else{
            [self showToast:[NSString stringWithFormat:@"Unable to follow %@, try after some time",self.artistProfileInfo[@"name"]]];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        NSDictionary *resp = [[SBHTTPClient sharedClient] responseObjectForError:error];
        if ([resp isKindOfClass:[NSDictionary class]]){
            NSString *msg = resp[@"status"];
            if ([msg isEqualToString:@"failure"]) {
                [self showToast:@"You are already following the artist"];
            }
        }
        else {
            [self showAlert:error.localizedDescription];
        }
        //[self showToast:error.localizedDescription];
    }];
}

@end
