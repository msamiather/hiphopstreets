//
//  ArtistProfileVC.h
//  HipHopStreets
//
//  Created by IOS on 29/12/16.
//  Copyright © 2016 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistProfileVC : UICollectionViewController

@property (nonatomic ,strong) NSString *artistId;

@end
