//
//  ArtistProfileCell.h
//  HipHopStreets
//
//  Created by IOS on 29/12/16.
//  Copyright © 2016 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistProfileCell : UICollectionViewCell
@property (nonatomic ,strong) IBOutlet UILabel *songName;
@property (nonatomic ,strong) IBOutlet UILabel *songCategory;
@property (nonatomic ,strong) IBOutlet UIImageView *songImgView;
@end
