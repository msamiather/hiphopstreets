//
//  CategoriesCell.m
//  HipHopStreets
//
//  Created by IOS on 27/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "CategoriesCell.h"

@implementation CategoriesCell

-(void)awakeFromNib{
    [super awakeFromNib];
//    self.layer.borderColor=[[UIColor lightGrayColor]CGColor];
//    self.layer.borderWidth=0.50f;
    
    
    CGSize _screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat width = ((_screenSize.width-28)/2);
    
    CALayer * layer = [self layer];
    [layer setShadowOffset:CGSizeMake(0, 1)];
    [layer setShadowRadius:1.0];
    [layer setShadowColor:[UIColor blackColor].CGColor] ;
    [layer setShadowOpacity:0.5];
    layer.cornerRadius = 2;
    layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, width,width) cornerRadius:2].CGPath;
    self.clipsToBounds=NO;
}

@end
