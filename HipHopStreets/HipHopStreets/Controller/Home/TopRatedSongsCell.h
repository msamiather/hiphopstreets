//
//  CategorySongsCell.h
//  HipHopStreets
//
//  Created by IOS on 26/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopRatedSongsCell : UICollectionViewCell
@property (nonatomic ,strong) IBOutlet UILabel *songName;
@property (nonatomic ,strong) IBOutlet UILabel *artistName;
@property (nonatomic ,strong) IBOutlet UIImageView *songImgView;
@property (nonatomic ,strong) IBOutlet UIButton *moreOptionBtn;

@end
