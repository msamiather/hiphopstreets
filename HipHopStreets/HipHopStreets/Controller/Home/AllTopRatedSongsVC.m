//
//  AllTopRatedSongsVC.m
//  HipHopStreets
//
//  Created by IOS on 28/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "AllTopRatedSongsVC.h"
#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"
#import "UIKit+AFNetworking.h"
#import "CategorySongsVC.h"
#import "TopRatedSongsCell.h"
#import "SongLibrary.h"
#import "FooterActivityView.h"
#import "DRPLoadingSpinner.h"
#import "Song.h"

@interface AllTopRatedSongsVC ()
@property (nonatomic, strong) NSMutableArray *topRatedSongs;
@property (nonatomic, assign) CGSize screenSize;
@property (nonatomic ,strong) DRPLoadingSpinner *drpLoadingSpinner;

@property (nonatomic ,assign) NSInteger startPageCount;
@property (nonatomic ,assign) NSInteger totalPageCount;
@property (nonatomic ,assign) NSInteger endPageCount;

@end

@implementation AllTopRatedSongsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _screenSize = [[UIScreen mainScreen] bounds].size;
    self.startPageCount=0;
    self.totalPageCount=100;
}

-(void)fetchTopratedStartingWith:(NSInteger)startIndex{
    
    //http://dev.goigi.biz/hiphop/api/songlist/1/10
    
    if (self.startPageCount==0) {
     [self showActivity:nil];
    }
    
    NSString *url=[NSString stringWithFormat:@"%@/%lu/%@",SERVICE_TOP_RATED_SONGS,startIndex,@"10"];

    [[SBHTTPClient sharedClient] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        [self hideMessage];
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"success"]) {
            
            self.totalPageCount=[resp[@"paginationtotalrows"] integerValue];
            
            
            NSArray *arr=resp[@"songanduserlist"];

            if ([arr isKindOfClass:[NSArray class]] && arr.count==0){
                FooterActivityView *footer=(FooterActivityView*)[[self.collectionView visibleSupplementaryViewsOfKind:UICollectionElementKindSectionFooter] lastObject];
                [footer.activity stopAnimating];
            }
            
            else if (self.startPageCount==0) {
                self.topRatedSongs=[[NSMutableArray alloc]init];
               NSArray *arr=resp[@"songanduserlist"];
                for (NSDictionary *dict in arr) {
                    NSMutableDictionary *xa=[[NSMutableDictionary alloc]init];
                    xa[@"song_name"]=dict[@"song_name"];
                    xa[@"songid"]=dict[@"songid"];
                    xa[@"song_image"]=dict[@"song_image"];
                    xa[@"song_file"]=dict[@"song_file"];
                    xa[@"likecount"]=dict[@"likecount"];
                    NSDictionary *uDict=dict[@"user"];
                    xa[@"id"]=uDict[@"id"];
                    xa[@"name"]=uDict[@"name"];
                    [_topRatedSongs addObject:xa];
                }
                self.startPageCount=self.startPageCount+10;
                [self.collectionView reloadData];
            }
            else{
                NSInteger i=[self.topRatedSongs count];
                NSArray *arr=resp[@"songanduserlist"];
                NSMutableArray *indexPaths=[[NSMutableArray alloc]init];
                for (NSDictionary *dict in arr) {
                    NSMutableDictionary *xa=[[NSMutableDictionary alloc]init];
                    xa[@"song_name"]=dict[@"song_name"];
                    xa[@"songid"]=dict[@"songid"];
                    xa[@"song_image"]=dict[@"song_image"];
                    xa[@"song_file"]=dict[@"song_file"];
                    xa[@"likecount"]=dict[@"likecount"];
                    NSDictionary *uDict=dict[@"user"];
                    xa[@"id"]=uDict[@"id"];
                    xa[@"name"]=uDict[@"name"];
                    [self.topRatedSongs addObject:xa];
                    [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
                    i++;
                }
                self.startPageCount=self.startPageCount+10;
                [self.collectionView insertItemsAtIndexPaths:indexPaths];
            }

        }
        else{
            self.topRatedSongs=nil;
            [self showMessage:@"No Songs Found"];
            [self.collectionView reloadData];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        [self hideMessage];
        [self showToast:error.localizedDescription];
    }];
    
    
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.topRatedSongs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *sn=self.topRatedSongs[indexPath.row];
    TopRatedSongsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TopRatedSongsCell class]) forIndexPath:indexPath];
    [cell.songImgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,sn[@"song_image"]]] placeholderImage:[UIImage imageNamed:@"songPlacehodler"]];
    cell.songName.text=sn[@"song_name"];
    cell.artistName.text=sn[@"name"];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (_screenSize.width-28)/2;
    return CGSizeMake(width, width+50);
    
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10,10, 4,10);//top, left, bottom, right
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
        
    [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    //[[SongLibrary sharedLibrary] showPlayerAndPlaySong:self.topRatedSongs repeatType:SongRepeatAll];
    [[SongLibrary sharedLibrary] showPlayerAndPlaySong:self.topRatedSongs repeatType:SongRepeatAll songIdex:indexPath.item type:YES];
    
}

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        FooterActivityView *footerView=(FooterActivityView*)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterActivityView" forIndexPath:indexPath];
        return footerView;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath{
    if ([elementKind isEqualToString:UICollectionElementKindSectionFooter]) {

        FooterActivityView *footer=(FooterActivityView*)view;
        if (self.startPageCount >=self.totalPageCount) {
            [footer.activity stopAnimating];
        }
       
        else{
            [footer.activity startAnimating];
            [self fetchTopratedStartingWith:_startPageCount];
        }
    };
}

#pragma ButtonActions

- (IBAction)backBtnAction:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
