//
//  CategorySongsCell.h
//  HipHopStreets
//
//  Created by IOS on 14/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategorySongsCell : UITableViewCell
@property (nonatomic ,strong) IBOutlet UIView *bgView;
@property (nonatomic ,strong) IBOutlet UIImageView *songImgView;
@property (nonatomic ,strong) IBOutlet UILabel *songNameLbl;
@property (nonatomic ,strong) IBOutlet UILabel *artistNameLbl;



@end
