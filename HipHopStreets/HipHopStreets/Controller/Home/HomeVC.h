//
//  HomeVC.h
//  HipHopStreets
//
//  Created by IOS on 12/17/16.
//  Copyright © 2016 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
-(void)fetchFeaturesSongs;

@end
