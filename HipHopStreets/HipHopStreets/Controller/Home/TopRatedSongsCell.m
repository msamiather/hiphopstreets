//
//  CategorySongsCell.m
//  HipHopStreets
//
//  Created by IOS on 26/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "TopRatedSongsCell.h"

@implementation TopRatedSongsCell

-(void)awakeFromNib{
    [super awakeFromNib];
    
    CGSize _screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat width = ((_screenSize.width-28)/2);
    
    
    self.contentView.layer.cornerRadius = 2.0f;
    self.contentView.layer.borderWidth = 1.0f;
    self.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    self.contentView.layer.masksToBounds = YES;
    
    
    CALayer * layer = [self layer];
    [layer setShadowOffset:CGSizeMake(0, 1)];
    [layer setShadowRadius:1.0];
    [layer setShadowColor:[UIColor blackColor].CGColor] ;
    [layer setShadowOpacity:0.5];
    layer.cornerRadius = 3;
    layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, width, width+50) cornerRadius:layer.cornerRadius].CGPath;
    self.clipsToBounds=NO;
    
    
    
    
}
@end
