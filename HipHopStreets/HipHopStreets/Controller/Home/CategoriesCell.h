//
//  CategoriesCell.h
//  HipHopStreets
//
//  Created by IOS on 27/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesCell : UICollectionViewCell
@property (nonatomic ,strong) IBOutlet UIImageView *categoryImgView;
@property (nonatomic ,strong) IBOutlet UILabel *categoryNameLbl;

@end
