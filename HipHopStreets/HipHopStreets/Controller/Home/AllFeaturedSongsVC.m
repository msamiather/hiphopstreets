//
//  AllFeaturedSongsVC.m
//  HipHopStreets
//
//  Created by IOS on 14/04/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "AllFeaturedSongsVC.h"

#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"
#import "Song.h"
#import "TopRatedSongsCell.h"
#import "SongLibrary.h"




@interface AllFeaturedSongsVC ()
@property (nonatomic, strong) NSMutableArray *topRatedSongs;
@property (nonatomic, assign) CGSize screenSize;


@end

@implementation AllFeaturedSongsVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _screenSize = [[UIScreen mainScreen] bounds].size;

    [self fetchTopratedSong];
    
}

-(void)fetchTopratedSong{
    
    //http://xpandingus.us/hiphopstreets/api/featuredsonglist?format=json
    
    [self showActivity:nil];
    
    [[SBHTTPClient sharedClient] GET:SERVICE_FEATURE_SONGS parameters:@{@"format":@"json"} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"success"]) {
            self.topRatedSongs=[[NSMutableArray alloc]init];
            NSArray *arr=resp[@"featuredsonglist"];
            if (arr.count!=0) {
                for (NSDictionary *sDict in arr) {
                    Song *s=[[Song alloc]init];
                    s.songId=sDict[@"songid"];
                    s.songName=sDict[@"song_name"];
                    s.songImgUrl=sDict[@"song_image"];
                    s.songCategory=sDict[@"song_category"];
                    s.songFile=sDict[@"song_file"];
                    s.likeCount=sDict[@"likecount"];
                    NSDictionary *uDict=sDict[@"user"];
                    s.user=[[User alloc]init];
                    s.user.userId=uDict[@"id"];
                    s.user.name=uDict[@"name"];
                    s.user.emailId=uDict[@"email_id"];
                    s.user.mobileNum=uDict[@"mobile"];
                    s.user.imgUrl=uDict[@"pic"];
                    s.user.gender=uDict[@"sex"];
                    s.user.regDate=uDict[@"regddate"];
                    [self.topRatedSongs addObject:s];
                    
                }
            }
        }
        else{
            self.topRatedSongs=nil;
        }
        
        [self.collectionView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        [self showToast:error.localizedDescription];
    }];
    
    
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.topRatedSongs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Song *sn=self.topRatedSongs[indexPath.row];
    TopRatedSongsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TopRatedSongsCell class]) forIndexPath:indexPath];
    [cell.songImgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,sn.songImgUrl]] placeholderImage:[UIImage imageNamed:@"songPlacehodler"]];
    cell.songName.text=sn.songName;
    cell.artistName.text=sn.user.name;
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (_screenSize.width-28)/2;
    return CGSizeMake(width, width+50);
    
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10,10, 4,10);//top, left, bottom, right
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Song *sn=self.topRatedSongs[indexPath.row];
    
    [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    [[SongLibrary sharedLibrary] showPlayerAndPlaySong:[sn crateDictionary]];
    
}

#pragma ButtonActions

- (IBAction)backBtnAction:(UIBarButtonItem *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
