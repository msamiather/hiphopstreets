//
//  HomeVC.m
//  HipHopStreets
//
//  Created by IOS on 12/17/16.
//  Copyright © 2016 Goigi. All rights reserved.
//

#import "HomeVC.h"
#import "HHMusicCell.h"
#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"
#import "UIKit+AFNetworking.h"
#import "CategorySongsVC.h"
#import "TopRatedSongsCell.h"
#import "HHCollectionHeaderView.h"
#import "CategoriesCell.h"
#import "Song.h"

#import "SongLibrary.h"
#import "CirclerView.h"
#import "AudioPlayerVC.h"


@interface HomeVC ()
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSMutableArray *topRatedSongs;

@property (nonatomic, assign) CGSize screenSize;

@end

@implementation HomeVC

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    // Do any additional setup after loading the view.
    _screenSize = [[UIScreen mainScreen] bounds].size;
    self.categories = @[@"Club",@"Conscious",@"Freestyle",@"Gangsta",@"R&B",@"Trap",@"Tribute"];
    
    [self fetchFeaturesSongs];
    
    //self.collectionView.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"backGround"]];
    
}
-(void)fetchFeaturesSongs{
    //http://xpandingus.us/hiphopstreets/api/featuredsonglist?format=json
    
    [self showActivity:nil];
    
    [[SBHTTPClient sharedClient] GET:SERVICE_FEATURE_SONGS parameters:@{@"format":@"json"} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        NSLog(@"%@",resp);
        
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"success"]) {
            self.topRatedSongs=[[NSMutableArray alloc]init];
            NSArray *arr=resp[@"featuredsonglist"];
            if (arr.count!=0) {
                for (NSDictionary *sDict in arr) {
                    Song *s=[[Song alloc]init];
                    s.songId=sDict[@"songid"];
                    s.songName=sDict[@"song_name"];
                    s.songImgUrl=sDict[@"song_image"];
                    s.songCategory=sDict[@"song_category"];
                    s.songFile=sDict[@"song_file"];
                    s.likeCount=sDict[@"likecount"];
                    NSDictionary *uDict=sDict[@"user"];
                    s.user=[[User alloc]init];
                    s.user.userId=uDict[@"id"];
                    s.user.name=uDict[@"name"];
                    s.user.emailId=uDict[@"email_id"];
                    s.user.mobileNum=uDict[@"mobile"];
                    s.user.imgUrl=uDict[@"pic"];
                    s.user.gender=uDict[@"sex"];
                    s.user.regDate=uDict[@"regddate"];
                    [self.topRatedSongs addObject:s];
                    
                }
            }
        }
        else{
            self.topRatedSongs=nil;
        }
        
        [self.collectionView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        //[self showToast:error.localizedDescription];
    }];
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.topRatedSongs.count==0) {
        return 1;
    }
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.topRatedSongs.count==0) {
        return self.categories.count;
    }
    else{
        if (section==0) {
            return self.topRatedSongs.count;
        }
        else{
            return _categories.count;
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.topRatedSongs.count==0) {
        CategoriesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CategoriesCell class]) forIndexPath:indexPath];
       
        cell.categoryImgView.image=[UIImage imageNamed:self.categories[indexPath.row]];
        return cell;
    }
    
    else{
    
        if (indexPath.section==0) {
            Song *sn=self.topRatedSongs[indexPath.row];
            TopRatedSongsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TopRatedSongsCell class]) forIndexPath:indexPath];
            [cell.songImgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,sn.songImgUrl]] placeholderImage:[UIImage imageNamed:@"songPlacehodler"]];
            cell.songName.text=sn.songName;
            cell.artistName.text=sn.user.name;
            return cell;
        }
        else{
            CategoriesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CategoriesCell class]) forIndexPath:indexPath];
             cell.categoryImgView.image=[UIImage imageNamed:self.categories[indexPath.row]];
            return cell;
        }
    }
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    

    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        HHCollectionHeaderView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HHCollectionHeaderView" forIndexPath:indexPath];
        
        if (self.topRatedSongs.count==0) {
            view.headerTitle.text = @"Categories";
            [view.viewAllSongsBtn setHidden:YES];
  
        }
        else{
            switch (indexPath.section) {
                case 0:
                {
                    [view.viewAllSongsBtn setHidden:NO];
                    
                    view.headerTitle.text = @"Featured Songs";
                    [view.viewAllSongsBtn setHidden:NO];
                    break;
                }
                case 1:
                {
                    [view.viewAllSongsBtn setHidden:YES];
                    view.headerTitle.text = @"Categories";
                    [view.viewAllSongsBtn setHidden:YES];

                    break;
                }
                default:
                    break;
            }
        }
        
        return view;
    }
    else{
    
        return nil;
    }

}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.topRatedSongs.count==0) {
        CGFloat width = (_screenSize.width-28)/2;
        return CGSizeMake(width, width);
    }
    
    else{
        if (indexPath.section==0) {
            CGFloat width = (_screenSize.width-28)/2;
            return CGSizeMake(width, width+50);
        }
        else{
            CGFloat width = (_screenSize.width-28)/2;
            return CGSizeMake(width, width);
        }
    }
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10,10, 4,10);//top, left, bottom, right
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if (self.topRatedSongs.count==0) {
        if (indexPath.row==4) {
            [self fetchCategorySongs:@"RB"];
            
        }
        else{
            [self fetchCategorySongs:self.categories[indexPath.row]];
            
        }
    }
    else{
        switch (indexPath.section) {
            case 0:{
            
                [self downloadSong:indexPath];
            }
                break;
            case 1:{
                if (indexPath.row==4) {
                    [self fetchCategorySongs:@"R&B"];
  
                }
                else{
                    [self fetchCategorySongs:self.categories[indexPath.row]];

                }
//                CategorySongsVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CategorySongsVC class])];
//                vc.categoryName=self.categories[indexPath.row];
//                vc.title=self.categories[indexPath.row];
//                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
            default:
                break;
        }
    }

}

//Service category songs

-(void)fetchCategorySongs:(NSString*)categoryStr{
    
    [self showActivity:nil];
    
    NSString *url=[NSString stringWithFormat:@"%@/%@",SERVICE_CATEOGORY_SONGS,categoryStr];
    
    [[SBHTTPClient sharedClient] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"success"]) {
            
            NSMutableArray *categorySongs=[[NSMutableArray alloc]init];
            NSArray *arr=resp[@"songanduserlist"];

            for (NSDictionary *sDict in arr) {
                NSMutableDictionary *xa=[[NSMutableDictionary alloc]init];
                
                [xa setObject:sDict[@"song_name"] forKey:@"song_name"];
                [xa setObject:sDict[@"songid"] forKey:@"songid"];
                [xa setObject:sDict[@"song_image"] forKey:@"song_image"];
                [xa setObject:sDict[@"song_file"] forKey:@"song_file"];
                [xa setObject:sDict[@"likecount"] forKey:@"likecount"];
                
                NSDictionary *uDict=sDict[@"user"];
                [xa setObject:uDict[@"id"] forKey:@"id"];
                [xa setObject:uDict[@"name"] forKey:@"name"];
                [categorySongs addObject:xa];
            }
            
            [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
            
            if (categorySongs.count>0) {
                //[[SongLibrary sharedLibrary] showPlayerAndPlaySong:categorySongs repeatType:SongRepeatAll];
                [[SongLibrary sharedLibrary] showPlayerAndPlaySong:categorySongs repeatType:SongRepeatAll songIdex:0 type:NO];
            }
            else {
                [self showAlert:@"This category has no songs."];
            }
        }
        else{
            [self showAlert:@"No songs found for this category"];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        [self showToast:error.localizedDescription];
    }];
    
    
}

#pragma Buttons Actions

-(void)downloadSong:(NSIndexPath*)indexPath;
{
    Song *sn=self.topRatedSongs[indexPath.row];
    
    [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
        
   // [[SongLibrary sharedLibrary] showPlayerAndPlaySong:[sn crateDictionary] repeatType:SongRepeatAll];
    [[SongLibrary sharedLibrary] showPlayerAndPlaySong:[sn crateDictionary] repeatType:SongRepeatAll songIdex:0 type:NO];

}


@end
