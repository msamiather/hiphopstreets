//
//  CategorySongsVC.m
//  HipHopStreets
//
//  Created by IOS on 26/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "CategorySongsVC.h"
#import "CategorySongsCell.h"

#import "SBHTTPClient.h"
#import "SBExtensions.h"
#import "SBPreference.h"

#import "Song.h"
#import "SongLibrary.h"

@interface CategorySongsVC ()
@property (nonatomic ,strong) NSMutableArray *categorySongs;
@property (nonatomic ,assign) NSInteger startPageCount;
@property (nonatomic ,assign) NSInteger totalPageCount;
@property (nonatomic ,assign) NSInteger endPageCount;



@end

@implementation CategorySongsVC


- (void)viewDidLoad {
    [super viewDidLoad];

    self.startPageCount=0;
    self.totalPageCount=100;
    self.endPageCount=10;

    
    [self fetchCategorySongsWithStartWith:_startPageCount endWith:_endPageCount];
}

-(void)fetchCategorySongsWithStartWith:(NSInteger)startCount endWith:(NSInteger)endCount{
    
    if (self.startPageCount==0) {
        [self showActivity:nil];
    }

    NSString *url=[NSString stringWithFormat:@"%@/%@/%lu/%@",SERVICE_CATEOGORY_SONGS,self.categoryName,startCount,[NSString stringWithFormat:@"%lu",endCount]];
    
    [[SBHTTPClient sharedClient] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideActivity];
        [self hideMessage];
        NSDictionary *resp=[responseObject dictionaryRemovingNullValues];
        if ([resp isKindOfClass:[NSDictionary class]] && [resp[@"status"] isEqualToString:@"success"]) {
            
            self.totalPageCount=[resp[@"paginationtotalrows"] integerValue];
            
            if (self.startPageCount>=self.totalPageCount) {
                [self.activity stopAnimating];
                return;
            }
            
            if (self->_startPageCount==0) {
                self.categorySongs=[[NSMutableArray alloc]init];
                NSArray *arr=resp[@"songanduserlist"];
                if (arr.count!=0) {
                    for (NSDictionary *sDict in arr) {
                        Song *s=[[Song alloc]init];
                        s.songId=sDict[@"songid"];
                        s.songName=sDict[@"song_name"];
                        s.songImgUrl=sDict[@"song_image"];
                        s.songCategory=sDict[@"song_category"];
                        s.songFile=sDict[@"song_file"];
                        NSDictionary *uDict=sDict[@"user"];
                        s.user=[[User alloc]init];
                        s.user.userId=uDict[@"id"];
                        s.user.name=uDict[@"name"];
                        s.user.emailId=uDict[@"email_id"];
                        s.user.mobileNum=uDict[@"mobile"];
                        s.user.imgUrl=uDict[@"pic"];
                        s.user.gender=uDict[@"sex"];
                        s.user.regDate=uDict[@"regddate"];
                        [self.categorySongs addObject:s];
                    }
                    
                }
                [self.tableView reloadData];
            }
            else{
                NSInteger i=self.categorySongs.count;
                NSArray *arr=resp[@"songanduserlist"];
                NSMutableArray *indexPaths=[[NSMutableArray alloc]init];
                if (arr.count!=0) {
                    for (NSDictionary *sDict in arr) {
                        Song *s=[[Song alloc]init];
                        s.songId=sDict[@"songid"];
                        s.songName=sDict[@"song_name"];
                        s.songImgUrl=sDict[@"song_image"];
                        s.songCategory=sDict[@"song_category"];
                        s.songFile=sDict[@"song_file"];
                        NSDictionary *uDict=sDict[@"user"];
                        s.user=[[User alloc]init];
                        s.user.userId=uDict[@"id"];
                        s.user.name=uDict[@"name"];
                        s.user.emailId=uDict[@"email_id"];
                        s.user.mobileNum=uDict[@"mobile"];
                        s.user.imgUrl=uDict[@"pic"];
                        s.user.gender=uDict[@"sex"];
                        s.user.regDate=uDict[@"regddate"];
                        [self.categorySongs addObject:s];
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        i++;
                    }
                }
                [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
            }
        }
        else{
            self.categorySongs=nil;
            [self showMessage:@"No songs found"];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideActivity];
        [self hideMessage];
        [self showToast:error.localizedDescription];
    }];
    
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.categorySongs.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Song *sn=self.categorySongs[indexPath.row];
    CategorySongsCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CategorySongsCell class])];
    [cell.songImgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",BASE_URL,SERVICE_PATH_SONG_IMAGE,sn.songImgUrl]] placeholderImage:[UIImage imageNamed:@"songPlacehodler"]];
    cell.songNameLbl.text=sn.songName;
    cell.artistNameLbl.text=sn.user.name;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.startPageCount>=self.totalPageCount) {
        [self.activity stopAnimating];
    }
    else{
    
        if (indexPath.row==self.categorySongs.count-1) {
            [self.activity startAnimating];
            self.startPageCount=_startPageCount+10;
            self.endPageCount=_endPageCount+10;
            if (self.startPageCount>=self.totalPageCount) {
                [self.activity stopAnimating];
            }
            else{
                [self fetchCategorySongsWithStartWith:_startPageCount endWith:_endPageCount];

            }
        }
    }

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    Song *sn=self.categorySongs[indexPath.row];
    
    [[SongLibrary sharedLibrary] addSong:[sn crateDictionary]];
    
    [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    [[SongLibrary sharedLibrary] showPlayerAndPlaySong:[sn crateDictionary]];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 98;
}
#pragma buttonActions

-(IBAction)backBtnAction:(UIButton*)sender{
    
    [self.navigationController popViewControllerAnimated:YES];

}




@end
