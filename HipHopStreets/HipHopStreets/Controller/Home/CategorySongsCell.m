//
//  CategorySongsCell.m
//  HipHopStreets
//
//  Created by IOS on 14/02/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "CategorySongsCell.h"

@implementation CategorySongsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.bgView.layer.cornerRadius=8.0f;
    //self.bgView.clipsToBounds=YES;
    
    
    CGSize _screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat width = _screenSize.width-16;
    
    CALayer * layer = [self.bgView layer];
    [layer setShadowOffset:CGSizeMake(0, 1)];
    [layer setShadowRadius:2.0];
    [layer setShadowColor:[UIColor blackColor].CGColor] ;
    [layer setShadowOpacity:0.5];
    layer.cornerRadius = 2;
    layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, width, 90) cornerRadius:2].CGPath;
    self.clipsToBounds=NO;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    UIView * view = [[UIView alloc] init];
    view.backgroundColor=[UIColor clearColor];
    self.selectedBackgroundView=view;
}



@end
