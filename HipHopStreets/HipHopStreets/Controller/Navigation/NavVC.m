//
//  NavVC.m
//  SBLibrary
//
//  Created by IOS on 12/17/16.
//  Copyright © 2016 SMB. All rights reserved.
//

#import "NavVC.h"

@interface NavVC ()

@end

@implementation NavVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
