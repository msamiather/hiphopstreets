//
//  UITableView+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 5/24/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Additions)
- (NSIndexPath *)smb_indexPathForView:(UIView *)view;
- (void)smb_showMessage:(NSString *)msg;
- (void)smb_showMessage:(NSString *)msg insets:(UIEdgeInsets)insets;
- (void)smb_hideMessage;
@end
