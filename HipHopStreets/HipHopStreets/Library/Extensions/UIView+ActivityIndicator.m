//
//  UIView+ActivityIndicator.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 03/03/13.
//  Copyright (c) 2013 goigi. All rights reserved.
//

#import "UIView+ActivityIndicator.h"
#import <objc/runtime.h>

static char const * const vakey = "viewackey";

@implementation UIView (smb_Activity)

#pragma mark - Show Activity

- (UIView *)smb_showActivity {
    UIActivityIndicatorView *aiView = objc_getAssociatedObject(self, vakey);
    if (aiView==nil) {
        
        self.userInteractionEnabled = NO;
        
        aiView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [aiView setCenter:CGPointMake(CGRectGetMidX(self.bounds),CGRectGetMidY(self.bounds))];
        aiView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:aiView];
        [aiView startAnimating];
        objc_setAssociatedObject(self, vakey, aiView, OBJC_ASSOCIATION_ASSIGN);
    }
    return aiView;
}

- (UIView *)smb_showActivityAnimated:(BOOL)animated {
    UIView *view = [self smb_showActivity];
    if (view!=nil && animated) {
        view.alpha = 0.0f;
        [UIView animateWithDuration:ANIMATION_DELAY animations:^(){
            view.alpha = 1.0f;
        }];
    }
    return view;
}

- (UIView *)smb_showActivityAnimated:(BOOL)animated offset:(UIOffset)offset {
    UIActivityIndicatorView *aiView = objc_getAssociatedObject(self, vakey);
    if (aiView==nil) {
        
        self.userInteractionEnabled = NO;
        
        aiView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [aiView setCenter:CGPointMake(CGRectGetMidX(self.bounds)+offset.horizontal,CGRectGetMidY(self.bounds)+offset.vertical)];
        aiView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:aiView];
        [aiView startAnimating];
        objc_setAssociatedObject(self, vakey, aiView, OBJC_ASSOCIATION_ASSIGN);
        
        if (animated) {
            aiView.alpha = 0.0f;
            [UIView animateWithDuration:ANIMATION_DELAY animations:^(){
                aiView.alpha = 1.0f;
            }];
        }
    }
    return aiView;
}

- (UIView *)smb_showActivity:(UIActivityIndicatorViewStyle)style animated:(BOOL)animated offset:(UIOffset)offset {
    UIActivityIndicatorView *aiView = objc_getAssociatedObject(self, vakey);
    if (aiView==nil) {
        
        self.userInteractionEnabled = NO;
        
        aiView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
        [aiView setCenter:CGPointMake(CGRectGetMidX(self.bounds)+offset.horizontal,CGRectGetMidY(self.bounds)+offset.vertical)];
        aiView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:aiView];
        [aiView startAnimating];
        objc_setAssociatedObject(self, vakey, aiView, OBJC_ASSOCIATION_ASSIGN);
        
        if (animated) {
            aiView.alpha = 0.0f;
            [UIView animateWithDuration:ANIMATION_DELAY animations:^(){
                aiView.alpha = 1.0f;
            }];
        }
    }
    return aiView;
}

- (UIView *)smb_showActivity:(UIActivityIndicatorViewStyle)style message:(NSString *)msg animated:(BOOL)animated offset:(UIOffset)offset {
    UIView *view = objc_getAssociatedObject(self, vakey);
    if (view==nil) {
        
        self.userInteractionEnabled = NO;
        
        view = [[UIView alloc] init];
        
        UIActivityIndicatorView *aiView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
        aiView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        UILabel *msgLbl = [[UILabel alloc] init];
        msgLbl.text = msg;
        msgLbl.font = [UIFont systemFontOfSize:14.0];
        msgLbl.textColor = [UIColor darkGrayColor];
        [msgLbl sizeToFit];
        CGRect f = msgLbl.frame;
        f.origin.x = aiView.frame.size.width+5;
        f.size.height = aiView.frame.size.height;
        msgLbl.frame = f;
        
        view.frame = CGRectMake(0, 0, aiView.frame.size.width+5+msgLbl.frame.size.width, aiView.frame.size.height);
        [view setCenter:CGPointMake(CGRectGetMidX(self.bounds)+offset.horizontal,CGRectGetMidY(self.bounds)+offset.vertical)];
        
        [view addSubview:aiView];
        [view addSubview:msgLbl];
        [self addSubview:view];
        [aiView startAnimating];
        
        objc_setAssociatedObject(self, vakey, view, OBJC_ASSOCIATION_ASSIGN);
        
        if (animated) {
            view.alpha = 0.0f;
            [UIView animateWithDuration:ANIMATION_DELAY animations:^(){
                view.alpha = 1.0f;
            }];
        }
    }
    return view;
}

- (UIView *)smb_showMessageActivity:(NSString *)msg {
    return [self smb_showActivity:UIActivityIndicatorViewStyleGray message:msg animated:NO offset:UIOffsetZero];
}

#pragma mark - Hide Activity

- (void)smb_hideActivity {
    self.userInteractionEnabled = YES;
    UIView *v = objc_getAssociatedObject(self, vakey);
    if (v!=nil) {
        [v removeFromSuperview];
        objc_setAssociatedObject(self, vakey, nil, OBJC_ASSOCIATION_ASSIGN);
    }
}

- (void)smb_hideActivityAnimated:(BOOL)animated {
    self.userInteractionEnabled = YES;
    UIView *v = objc_getAssociatedObject(self, vakey);
    if (v!=nil) {
        if (animated) {
            [UIView animateWithDuration:ANIMATION_DELAY animations:^(){
                v.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [v removeFromSuperview];
                    objc_setAssociatedObject(self, vakey, nil, OBJC_ASSOCIATION_ASSIGN);
                }
            }];
        }
        else {
            [v removeFromSuperview];
            objc_setAssociatedObject(self, vakey, nil, OBJC_ASSOCIATION_ASSIGN);
        }
    }
}



- (void)showCustomActivity:(UIView *)activityView {
    if (objc_getAssociatedObject(self, vakey)==nil) {
        
        self.userInteractionEnabled = NO;
        
        UIColor *backgroundColor = ACTIVITY_BACKGROUND_COLOR;
        
        //Background View
        UIView *backView = [[UIView alloc] init];
        backView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        backView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        backView.backgroundColor = backgroundColor;
        
        UIView *middleView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 120)];
        middleView.center=CGPointMake(CGRectGetMidX(backView.bounds),CGRectGetMidY(backView.bounds));
        middleView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        middleView.backgroundColor=[UIColor whiteColor];
        middleView.layer.cornerRadius=5.0f;
        [backView addSubview:middleView];
        
        
        
        activityView.center = CGPointMake(CGRectGetMidX(middleView.bounds),CGRectGetMidY(middleView.bounds));
        activityView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        [middleView addSubview:activityView];
        
        [self addSubview:backView];
        objc_setAssociatedObject(self, vakey, backView, OBJC_ASSOCIATION_ASSIGN);
        
        
    }
}

- (UIView *)customActivityView {
    UIView *view = (UIView*)objc_getAssociatedObject(self, vakey);
    return [view.subviews lastObject];
}



#pragma mark - Hide Activity

- (void)hideActivity {
    self.userInteractionEnabled = YES;
    UIView *v = objc_getAssociatedObject(self, vakey);
    if (v!=nil) {
        [v removeFromSuperview];
        objc_setAssociatedObject(self, vakey, nil, OBJC_ASSOCIATION_ASSIGN);
    }
}

- (void)hideActivityAnimated:(BOOL)animated {
    self.userInteractionEnabled = YES;
    UIView *v = objc_getAssociatedObject(self, vakey);
    if (v!=nil) {
        if (animated) {
            [UIView animateWithDuration:ANIMATION_DELAY animations:^(){
                v.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [v removeFromSuperview];
                    objc_setAssociatedObject(self, vakey, nil, OBJC_ASSOCIATION_ASSIGN);
                }
            }];
        }
        else {
            [v removeFromSuperview];
            objc_setAssociatedObject(self, vakey, nil, OBJC_ASSOCIATION_ASSIGN);
        }
    }
}
#pragma mark - Activity Status

- (BOOL)smb_isActivityVisible {
    UIView *v = objc_getAssociatedObject(self, vakey);
    return v!=nil;
}

@end
