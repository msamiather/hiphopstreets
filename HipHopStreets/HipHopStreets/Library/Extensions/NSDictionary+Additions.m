//
//  NSDictionary+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 6/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)
+ (instancetype)dictionaryWithMKCoordinate:(CLLocationCoordinate2D)coordinate {
    return @{@"lat":[NSNumber numberWithDouble:coordinate.latitude],@"lng":[NSNumber numberWithDouble:coordinate.longitude]};
}
- (CLLocationCoordinate2D)MKCoordinateValue {
    return CLLocationCoordinate2DMake([self[@"lat"] doubleValue], [self[@"lng"] doubleValue]);
}

- (nullable id)nonNullObjectForKey:(nonnull NSString *)aKey {
    id obj = [self objectForKey:aKey];
    if ([obj isKindOfClass:[NSNull class]]) {
        return nil;
    }
    return obj;
}

- (nonnull NSDictionary *)dictionaryRemovingNullValues {
    NSMutableDictionary *mdic = [self mutableCopy];
    for (NSString *key in self.allKeys) {
        if ([[self objectForKey:key] isKindOfClass:[NSNull class]]) {
            [mdic setObject:@"" forKey:key];
        }
    }
    return mdic;
}


@end
