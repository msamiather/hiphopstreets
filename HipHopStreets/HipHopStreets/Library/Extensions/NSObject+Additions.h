//
//  NSObject+Additions.h
//  HipHopStreets
//
//  Created by IOS on 01/03/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Additions)
- (NSArray *)arrayFromCorruptedDictionary;
@end
