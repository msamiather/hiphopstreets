//
//  UIView+MBProgressCirclerView.m
//  MyMusic
//
//  Created by IOS on 01/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "UIView+MBProgressCirclerView.h"

@implementation UIView (MBProgressCirclerView)

-(UIView *)initilizingMbprogress
{
    MBCircularProgressBarView *circularView=[[MBCircularProgressBarView alloc]init];
    circularView.frame=CGRectMake(0, 0, 100, 100);
    circularView.progressColor=[UIColor  colorWithRed:140.0f/255.0f green:0.0f blue:47.0f/255.0f alpha:1.0f];
    circularView.backgroundColor=[UIColor clearColor];
    [circularView setFontColor:[UIColor colorWithRed:201.0f/255.0f green:146.0f/255.0f blue:173.0f/255.0f alpha:1.0f]];
    circularView.progressLineWidth=5.0f;
    [circularView setProgressAngle:100.0f];
    circularView.emptyLineWidth=5.0f;
    circularView.value=0.0f;
    return circularView;
}

-(void)mbprogressValue:(NSInteger*)value
{
    
    
    
}

@end
