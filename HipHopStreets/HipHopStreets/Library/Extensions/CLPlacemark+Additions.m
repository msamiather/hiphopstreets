//
//  CLPlacemark+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 6/6/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "CLPlacemark+Additions.h"
#import <Contacts/Contacts.h>

@implementation CLPlacemark (Additions)

- (NSString *)placeAddressString {
    CNMutablePostalAddress *address = [[CNMutablePostalAddress alloc] init];
    CLPlacemark *p = self;
    address.street = p.addressDictionary[@"Street"]? p.addressDictionary[@"Street"]:@"";
    address.state = p.addressDictionary[@"State"]? p.addressDictionary[@"State"]:@"";
    address.city = p.addressDictionary[@"City"]? p.addressDictionary[@"City"]:@"";
    address.country = p.addressDictionary[@"Country"]? p.addressDictionary[@"Country"]:@"";
    address.postalCode = p.addressDictionary[@"ZIP"]? p.addressDictionary[@"ZIP"]:@"";
    
    NSString *addressString = [CNPostalAddressFormatter stringFromPostalAddress:address style:CNPostalAddressFormatterStyleMailingAddress];
    if (addressString==nil) {
        addressString = p.locality;
    }
    return addressString;
}

- (NSString *)addressString {
    // put a space between "4" and "Melrose Place"
    NSString *firstSpace = (self.subThoroughfare != nil && self.thoroughfare != nil) ? @" " : @"";
    // put a comma between street and city/state
    NSString *comma = (self.subThoroughfare != nil || self.thoroughfare != nil) && (self.subAdministrativeArea != nil || self.administrativeArea != nil) ? @", " : @"";
    // put a space between "Washington" and "DC"
    NSString *secondSpace = (self.subAdministrativeArea != nil && self.administrativeArea != nil) ? @" " : @"";
    NSString *addressLine = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",
                             (self.subThoroughfare == nil ? @"" : self.subThoroughfare),
                             firstSpace,
                             (self.thoroughfare == nil ? @"" : self.thoroughfare),
                             comma,
                             // city
                             (self.locality == nil ? @"" : self.locality),
                             secondSpace,
                             // state
                             (self.administrativeArea == nil ? @"" : self.administrativeArea)
                             ];
    return addressLine;
}

@end
