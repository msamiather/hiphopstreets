//
//  Cryptography+Encoding.h
//  Utility
//
//  Created by Soumen Bhuin on 3/28/14.
//  
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software. Permission is granted to anyone to
//  use this software for any purpose, including commercial applications, and to
//  alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//  2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//  3. This notice may not be removed or altered from any source
//     distribution.
//

#import <Foundation/Foundation.h>

@interface NSData (Encryption)

- (NSData *)AES256EncryptedDataWithKey:(NSString *)key;

- (NSData *)AES256DecryptedDataWithKey:(NSString *)key;

@end

@interface NSData (Encoding)

- (NSString*)base64EncodedString;
- (NSString*)base64DecodedString;

@end

@interface NSString (Encoding)

- (NSString*)base64EncodedString; // Returns Base64 encoded string

- (NSString*)base64DecodedString; // Returns readable string

@end

@interface NSString (Encryption)

- (NSString *)AES256Encrypted_Base64EncodedStringWithKey:(NSString *)key; // Readable String -> AES256 Encrypted String -> base64 String
- (NSString *)AES256Decrypted_Base64DecodedStringWithKey:(NSString *)key; // base64 String -> AES256 Encrypted String -> Readable String

- (NSString *)AES256EncryptedStringWithKey:(NSString *)key;
- (NSString *)AES256DecryptedStringWithKey:(NSString *)key;

- (NSString *)MD5HashString;

@end

