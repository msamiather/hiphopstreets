//
//  MKMapView+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 5/12/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (Additions)
- (void)zoomMapRegionAroundCoordinate:(CLLocationCoordinate2D)coordinate;
- (void)zoomMapRegionAroundCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated;
@end
