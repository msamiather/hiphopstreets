//
//  UIView+MBProgressCirclerView.h
//  MyMusic
//
//  Created by IOS on 01/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCircularProgressBarView.h"

@interface UIView (MBProgressCirclerView)

@end
