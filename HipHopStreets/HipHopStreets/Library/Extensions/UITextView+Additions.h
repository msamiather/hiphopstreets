//
//  UITextView+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 08/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (Additions)
@property (strong, nonatomic, readonly) NSString *trimmedText;
@property (assign, nonatomic, getter=isResigningToolbarEnabled) BOOL resigningToolbarEnabled;
@end
