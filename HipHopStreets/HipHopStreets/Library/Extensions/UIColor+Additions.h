//
//  UIColor+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 04/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Additions)
+ (UIColor *)colorWithHex:(NSInteger)hex;
@end
