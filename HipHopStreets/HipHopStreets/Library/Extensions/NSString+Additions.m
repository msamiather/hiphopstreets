//
//  NSString+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 04/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (BOOL)matchesWithRegularExpression:(NSString *)regEx {
    NSRange r = [self rangeOfString:regEx options:NSRegularExpressionSearch];
    if (r.location == NSNotFound) {
        return NO;
    }
    else if ([self length]>r.length) {
        return NO;
    }
    return YES;
}

- (BOOL)isValidEmail {
    NSString *regEx = @"[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}";
    return [self matchesWithRegularExpression:regEx];
}

/*- (BOOL)isValidPhoneNumber {
    NSString *regEx = @"^\\+(?:[0-9] ?){6,14}[0-9]$";
    return [self matchesWithRegularExpression:regEx];
}*/

- (BOOL)isValidPhoneNumber {
    NSString *regEx1 = @"[0-9]{8}";
    NSString *regEx2 = @"[0-9]{10}";
    NSString *regEx3 = @"[0-9]{9}";
    return [self matchesWithRegularExpression:regEx1] || [self matchesWithRegularExpression:regEx2] || [self matchesWithRegularExpression:regEx3];
}

- (BOOL)isContainsOnlyDigit {
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([self rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
        return YES;
    }
    return NO;
}

- (BOOL)containsString:(NSString *)str {
    if ([self rangeOfString:str].location == NSNotFound) {
        return NO;
    } else {
        return YES;
    }
}

- (NSString *)trimmedString {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSUInteger)wordsCount {
    NSCharacterSet *separators = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSArray *words = [self componentsSeparatedByCharactersInSet:separators];
    NSIndexSet *separatorIndexes = [words indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [obj isEqualToString:@""];
    }];
    return [words count] - [separatorIndexes count];
}

@end
