//
//  UIViewController+Additions.m
//  Fleamonkey
//
//  Created by Soumen Bhuin on 04/02/16.
//  Copyright © 2016 igi. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UIViewController+Additions.h"
#import "SVProgressHUD.h"
#import "SBLabel.h"
#import "UIView+Toast.h"


@implementation UIViewController (Additions)

- (void)showToast:(NSString *)message {
    
    
    [self.view makeToast:message duration:1.5 position:CSToastPositionCenter style:nil];
}



- (void)showAlert:(NSString *)message {
    UIAlertController *con = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    [con addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:con animated:YES completion:nil];
}

+ (void)setupHUDDefaults {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.000 alpha:0.805]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setForegroundColor:[UIColor redColor]];
    [SVProgressHUD setMinimumSize:CGSizeMake(30, 30)];
    [SVProgressHUD setRingThickness:2];
    [SVProgressHUD setRingRadius:10];
    [SVProgressHUD setRingNoTextRadius:10];
    [SVProgressHUD setCornerRadius:8];
}





- (void)showActivity:(NSString *)text {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[self class] setupHUDDefaults];
    });
    
    if (text) {
        [SVProgressHUD showWithStatus:text];
    }
    else {
        [SVProgressHUD show];
    }
    
    
}

- (void)hideActivity {
    [SVProgressHUD dismiss];
}

- (void)hideActivityWithSuccess:(BOOL)success {
    if (success) {
        [SVProgressHUD showSuccessWithStatus:@"Success"];
    }
    else {
        [SVProgressHUD showErrorWithStatus:@"Falied"];
    }
}

- (void)attachChildViewController:(UIViewController *)childController {
    [self addChildViewController:childController];
    [self.view addSubview:childController.view];
    [childController didMoveToParentViewController:self];
}

- (void)detachChildViewController:(UIViewController *)childController {
    [childController willMoveToParentViewController:nil];
    [childController.view removeFromSuperview];
    [childController removeFromParentViewController];
}

- (void)setTapToDismissKeyboardEnabled:(BOOL)enabled {
    UITapGestureRecognizer *tg = nil;
    for (UITapGestureRecognizer *g in self.view.gestureRecognizers) {
        if ([g isKindOfClass:[UITapGestureRecognizer class]]) {
            tg = g;
            break;
        }
    }
    
    if (enabled) {
        if (tg==nil) {
            UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapToDismissKeyboard)];
            [self.view addGestureRecognizer:tg];
        }
        
    }
    else {
        if (tg!=nil) {
            [self.view removeGestureRecognizer:tg];
        }
    }
    
}

- (void)didTapToDismissKeyboard {
    [self.view endEditing:YES];
}

@end
