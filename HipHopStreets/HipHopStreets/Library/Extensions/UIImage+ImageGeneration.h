//
//  UIImage+ImageGeneration.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 3/16/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageGeneration)
+ (UIImage *)imageWithSize:(CGSize)size color:(UIColor *)color;
@end
