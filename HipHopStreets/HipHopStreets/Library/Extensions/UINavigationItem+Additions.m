//
//  UINavigationItem+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 6/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UINavigationItem+Additions.h"

@implementation UINavigationItem (Additions)

- (void)showActivityIndicator {
    [self showActivityIndicationWithMessage:@"Loading..."];
}

- (void)showActivityIndicationWithMessage:(NSString *)msg {
    UIActivityIndicatorView *aiView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    aiView.frame = CGRectMake(0, 0, aiView.bounds.size.width, aiView.bounds.size.height);
    aiView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.font = [UIFont systemFontOfSize:14.0];
    lbl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    lbl.text = msg;
    [lbl sizeToFit];
    lbl.frame = CGRectMake(5+CGRectGetMaxX(aiView.frame), 0, lbl.frame.size.width, aiView.frame.size.height);
    
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, aiView.frame.size.width+lbl.frame.size.width, aiView.frame.size.height)];
    [v addSubview:aiView];
    [v addSubview:lbl];
    
    self.titleView = v;
    
    [aiView startAnimating];
}

- (void)hideActivityIndicator {
    self.titleView = nil;
}

@end
