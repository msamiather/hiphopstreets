//
//  UICollectionViewController+Additions.m
//  SOS
//
//  Created by Soumen Bhuin on 3/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UICollectionViewController+Additions.h"
#import "SBLabel.h"

@implementation UICollectionViewController (Additions)

- (void)showMessage:(NSString *)msg {
    SBLabel *lbl = [[SBLabel alloc] initWithFrame:CGRectZero];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor lightGrayColor];
    lbl.numberOfLines = 0;
    lbl.text = msg;
    lbl.edgeInsets = UIEdgeInsetsMake(30, 30, 30, 30);
    self.collectionView.backgroundView = lbl;
}

- (void)hideMessage {
    self.collectionView.backgroundView = nil;
}

@end
