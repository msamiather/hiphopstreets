//
//  UITextView+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 08/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UITextView+Additions.h"

@implementation UITextView (Additions)

- (NSString *)trimmedText {
    return self.text?[self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
}

- (void)setResigningToolbarEnabled:(BOOL)resigningToolbarEnabled {
    if (resigningToolbarEnabled) {
        UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.window.bounds.size.width, 44)];
        toolBar.translucent = NO;
        toolBar.barTintColor = [UIColor whiteColor];
        toolBar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(inputAccessoryDoneButtonAction)]];
        self.inputAccessoryView = toolBar;
    }
    else {
        self.inputAccessoryView = nil;
    }
}

- (BOOL)isResigningToolbarEnabled {
    return self.inputAccessoryView!=nil;
}

- (void)inputAccessoryDoneButtonAction {
    [self resignFirstResponder];
}

@end
