//
//  UIViewController+SVProgressHUD.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/21/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UIViewController+SVProgressHUD.h"
#import "SVProgressHUD.h"

@implementation UIViewController (SVProgressHUD)

+ (void)setupHUDDefaults {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.000 alpha:0.805]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:0.141 green:0.620 blue:0.071 alpha:1.000]];
    [SVProgressHUD setMinimumSize:CGSizeMake(50, 50)];
    [SVProgressHUD setRingThickness:2];
    [SVProgressHUD setRingRadius:15];
    [SVProgressHUD setRingNoTextRadius:15];
    [SVProgressHUD setCornerRadius:5];
}

- (void)showActivity:(NSString *)text {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[self class] setupHUDDefaults];
    });
    
    if (text) {
        [SVProgressHUD showWithStatus:text];
    }
    else {
        [SVProgressHUD show];
    }
}

- (void)hideActivity {
    [SVProgressHUD dismiss];
}

- (void)hideActivityWithSuccess:(BOOL)success {
    if (success) {
        [SVProgressHUD showSuccessWithStatus:@"Success"];
    }
    else {
        [SVProgressHUD showErrorWithStatus:@"Falied"];
    }
}

@end
