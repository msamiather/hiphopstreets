//
//  NSFileManager+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 6/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "NSFileManager+Additions.h"

@implementation NSFileManager (Additions)

- (NSString *)documentDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return paths.firstObject;
}

@end
