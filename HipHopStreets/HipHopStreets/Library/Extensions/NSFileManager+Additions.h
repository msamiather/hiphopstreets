//
//  NSFileManager+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 6/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (Additions)

- (NSString *)documentDirectoryPath;

@end
