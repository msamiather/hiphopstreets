//
//  UIImage+smb_utils.h
//  Utility
//
//  Created by sbhuin on 15/05/12.
//  Copyright (C) 2012 Soumen Bhuin. All rights reserved.

//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software. Permission is granted to anyone to
//  use this software for any purpose, including commercial applications, and to
//  alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//  2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//  3. This notice may not be removed or altered from any source
//     distribution.
//

#import <UIKit/UIKit.h>

@interface UIImage (smb_utils)

typedef enum {
    MGImageResizeCrop,	// analogous to UIViewContentModeScaleAspectFill, i.e. "best fit" with no space around.
    MGImageResizeCropStart,
    MGImageResizeCropEnd,
    MGImageResizeScale	// analogous to UIViewContentModeScaleAspectFit, i.e. scale down to fit, leaving space around if necessary.
} MGImageResizingMethod;

//rotation
- (UIImage *) rotate:(UIImageOrientation)orient;
- (UIImage *) imageRotatedByRadians:(CGFloat)radians;
- (UIImage *) imageRotatedByDegrees:(CGFloat)degrees;

//ProportionalFill
- (UIImage *) imageToFitSize:(CGSize)size method:(MGImageResizingMethod)resizeMethod;
- (UIImage *) imageCroppedToFitSize:(CGSize)size; // uses MGImageResizeCrop
- (UIImage *) imageScaledToFitSize:(CGSize)size; // uses MGImageResizeScale
- (UIImage *) imageByScalingProportionallyToMinimumSize:(CGSize)targetSize;
- (UIImage *) imageByScalingProportionallyToSize:(CGSize)targetSize;
- (UIImage *) imageByScalingToSize:(CGSize)targetSize;

//Resizing and Scaling
+ (UIImage *) imageOfSize:(CGSize)size fromImage:(UIImage *)image;
- (UIImage *) imageScaledToSize:(CGSize)size;
- (UIImage *) imageScaledToWidth:(CGFloat)width;
- (UIImage *) imageScaledToHeight:(CGFloat)height;
- (UIImage *) imageScaledToSizeWithSameAspectRatio:(CGSize)targetSize;

//Masking
- (UIImage *) imageWithMask:(UIImage *)maskImage;
- (UIImage *) imageWithMiddleMask:(UIImage *)maskImage withMaskSize:(CGSize)size;

//Thumbnail
- (UIImage *) thumbnailImageOfSize:(CGSize)size;
- (UIImage *) imageAtRect:(CGRect)rect;

//data representation
- (NSData *) PNGData;
- (NSData *) JPEGData;



@end
