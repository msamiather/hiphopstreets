//
//  UIView+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 3/17/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UIView+Additions.h"

@implementation UIView (Additions)

- (void)shake {
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.duration = 0.6;
    animation.values = @[ @(-20), @(20), @(-20), @(20), @(-10), @(10), @(-5), @(5), @(0) ];
    [self.layer addAnimation:animation forKey:@"shake"];
}

- (void)shakeOnCompletion:(nullable void (^)(void))completion {
    [CATransaction begin];
    [CATransaction setCompletionBlock:completion];
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.duration = 0.6;
    animation.values = @[ @(-20), @(20), @(-20), @(20), @(-10), @(10), @(-5), @(5), @(0) ];
    [self.layer addAnimation:animation forKey:@"shake"];
    [CATransaction commit];
    
    
}



@end
