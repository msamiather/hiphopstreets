//
//  NSString+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 04/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (BOOL)matchesWithRegularExpression:(NSString *)regEx;
- (BOOL)isValidEmail;
- (BOOL)isValidPhoneNumber;
- (BOOL)isContainsOnlyDigit;
- (BOOL)containsString:(NSString *)str;
- (NSString *)trimmedString;
- (NSUInteger)wordsCount;
@end
