//
//  UITextField+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 04/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Additions)
@property (strong, nonatomic, readonly) NSString *trimmedText;
@property (strong, nonatomic, readonly) NSString *trimmedPlaceholder;
@end
