//
//  UIViewController+Additions.h
//  Fleamonkey
//
//  Created by Soumen Bhuin on 04/02/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Additions)

+ (void)setupHUDDefaults;

- (void)showToast:(NSString *)message;
- (void)showBottomToast:(NSString *)message;

- (void)showAlert:(NSString *)message;
- (void)showActivity:(NSString *)text;
- (void)hideActivity;
- (void)hideActivityWithSuccess:(BOOL)success;

- (void)attachChildViewController:(UIViewController *)childController;
- (void)detachChildViewController:(UIViewController *)childController;

- (void)setTapToDismissKeyboardEnabled:(BOOL)enabled;

@end
