//
//  UITableViewController+Additions.m
//  SOS
//
//  Created by Soumen Bhuin on 3/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UITableViewController+Additions.h"
#import "SBLabel.h"

@implementation UITableViewController (Additions)

- (NSIndexPath *)indexPathForView:(UIView *)view {
    UIView *cell = view;
    while (cell && ![cell isKindOfClass:[UITableViewCell class]])
        cell = cell.superview;
    return [self.tableView indexPathForCell:(UITableViewCell *)cell];
}
- (void)showMessage:(NSString *)msg {
    SBLabel *lbl = [[SBLabel alloc] initWithFrame:CGRectZero];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor lightGrayColor];
    lbl.numberOfLines = 0;
    lbl.text = msg;
    lbl.edgeInsets = UIEdgeInsetsMake(30, 30, 30, 30);
    self.tableView.backgroundView = lbl;
}

- (void)hideMessage {
    self.tableView.backgroundView = nil;
}

@end
