//
//  CLPlacemark+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 6/6/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLPlacemark (Additions)

- (NSString *)placeAddressString;
- (NSString *)addressString;

@end
