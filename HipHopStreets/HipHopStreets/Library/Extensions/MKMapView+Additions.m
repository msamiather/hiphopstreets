//
//  MKMapView+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 5/12/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "MKMapView+Additions.h"

@implementation MKMapView (Additions)

- (void)zoomMapRegionAroundCoordinate:(CLLocationCoordinate2D)coordinate {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = coordinate.latitude;
    location.longitude = coordinate.longitude;
    region.span = span;
    region.center = location;
    [self setRegion:region animated:NO];
}

- (void)zoomMapRegionAroundCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = coordinate.latitude;
    location.longitude = coordinate.longitude;
    region.span = span;
    region.center = location;
    [self setRegion:region animated:animated];
}

@end
