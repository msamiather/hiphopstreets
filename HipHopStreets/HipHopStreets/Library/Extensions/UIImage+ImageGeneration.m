//
//  UIImage+ImageGeneration.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 3/16/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UIImage+ImageGeneration.h"

@implementation UIImage (ImageGeneration)

+ (UIImage *)imageWithSize:(CGSize)size color:(UIColor *)color {
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
