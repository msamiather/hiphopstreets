//
//  UIView+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 3/17/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Additions)

- (void)shake;
- (void)shakeOnCompletion:(nullable void (^)(void))completion;

@end
