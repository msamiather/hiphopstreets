//
//  UITableView+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 5/24/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UITableView+Additions.h"
#import "SBLabel.h"

@implementation UITableView (Additions)

- (NSIndexPath *)smb_indexPathForView:(UIView *)view {
    UIView *cell = view;
    while (cell && ![cell isKindOfClass:[UITableViewCell class]])
        cell = cell.superview;
    return [self indexPathForCell:(UITableViewCell *)cell];
}

- (void)smb_showMessage:(NSString *)msg {
    SBLabel *lbl = [[SBLabel alloc] initWithFrame:CGRectZero];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor lightGrayColor];
    lbl.numberOfLines = 0;
    lbl.text = msg;
    lbl.edgeInsets = UIEdgeInsetsMake(30, 30, 30, 30);
    self.backgroundView = lbl;
}

- (void)smb_showMessage:(NSString *)msg insets:(UIEdgeInsets)insets {
    SBLabel *lbl = [[SBLabel alloc] initWithFrame:CGRectZero];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor lightGrayColor];
    lbl.numberOfLines = 0;
    lbl.text = msg;
    lbl.edgeInsets = insets;
    self.backgroundView = lbl;
}

- (void)smb_hideMessage {
    self.backgroundView = nil;
}

@end
