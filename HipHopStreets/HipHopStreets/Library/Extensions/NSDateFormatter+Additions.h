//
//  NSDateFormatter+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 08/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Additions)

+ (instancetype)sharedFormater;
+ (instancetype)dateTimeFormater;
+ (instancetype)serverDateTimeFormater;
+ (instancetype)serverDateFormater;
+ (instancetype)serverTimeFormater;
+ (instancetype)eventDateFormater;
+ (instancetype)eventDateDisplayFormater;

@end
