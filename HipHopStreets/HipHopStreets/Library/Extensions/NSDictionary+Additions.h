//
//  NSDictionary+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 6/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface NSDictionary (Additions)
+ (instancetype)dictionaryWithMKCoordinate:(CLLocationCoordinate2D)coordinate;
- (CLLocationCoordinate2D)MKCoordinateValue;

- (nullable id)nonNullObjectForKey:(nonnull NSString *)aKey;
- (nonnull NSDictionary *)dictionaryRemovingNullValues;
@end
