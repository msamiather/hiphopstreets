//
//  UINavigationItem+Additions.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 6/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (Additions)

- (void)showActivityIndicator;
- (void)showActivityIndicationWithMessage:(NSString *)msg;
- (void)hideActivityIndicator;

@end
