//
//  NSDateFormatter+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 08/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "NSDateFormatter+Additions.h"

@implementation NSDateFormatter (Additions)

+ (instancetype)sharedFormater {
    static NSDateFormatter *df = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        df = [[NSDateFormatter alloc] init];
        df.dateStyle = NSDateFormatterMediumStyle;
    });
    return df;
}

+ (instancetype)dateTimeFormater {
    static NSDateFormatter *df = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"MM-dd-YY hh:mm a";
    });
    return df;
}

+ (instancetype)serverDateTimeFormater {
    static NSDateFormatter *df = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"MM-dd-YY hh:mm a";
    });
    return df;
}

+ (instancetype)serverDateFormater {
    static NSDateFormatter *df = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"MM-dd-YY";
    });
    return df;
}

+ (instancetype)eventDateFormater {
    static NSDateFormatter *df = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"yyyy-MM-dd HH:mm";
        df.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    });
    return df;
}

+ (instancetype)eventDateDisplayFormater {
    static NSDateFormatter *df = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"dd MMM yyyy";
        df.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    });
    return df;
}

+ (instancetype)serverTimeFormater {
    static NSDateFormatter *df = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"hh:mm a";
    });
    return df;
}

@end
