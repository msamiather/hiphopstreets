//
//  UIViewController+SVProgressHUD.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/21/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (SVProgressHUD)

+ (void)setupHUDDefaults;

- (void)showActivity:(NSString *)text;
- (void)hideActivity;

- (void)hideActivityWithSuccess:(BOOL)success;

@end
