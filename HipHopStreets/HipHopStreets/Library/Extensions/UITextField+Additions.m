//
//  UITextField+Additions.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 04/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UITextField+Additions.h"

@implementation UITextField (Additions)

- (NSString *)trimmedText {
    return self.text?[self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
}

- (NSString *)trimmedPlaceholder {
    return self.placeholder?[self.placeholder stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
}

@end
