//
//  UIView+ActivityIndicator.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 03/03/13.
//  Copyright (c) 2013 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


#define ANIMATION_DELAY 0.4f
#define CENTER_BOX_DIMENTION 40
#define ACTIVITY_BACKGROUND_COLOR [UIColor clearColor]

@interface UIView (smb_Activity)

/*
 Shows and animates UIActivityIndicatorView
 */
- (UIView *)smb_showActivity;
- (UIView *)smb_showMessageActivity:(NSString *)msg;
- (UIView *)smb_showActivityAnimated:(BOOL)animated;
- (UIView *)smb_showActivityAnimated:(BOOL)animated offset:(UIOffset)offset;
- (UIView *)smb_showActivity:(UIActivityIndicatorViewStyle)style animated:(BOOL)animated offset:(UIOffset)offset;
- (UIView *)smb_showActivity:(UIActivityIndicatorViewStyle)style message:(NSString *)msg animated:(BOOL)animated offset:(UIOffset)offset;

/*
 Hides UIActivityIndicatorView
 */

- (void)smb_hideActivity;
- (void)smb_hideActivityAnimated:(BOOL)animated;

/*
 Activity status
 */

- (BOOL)smb_isActivityVisible;



- (void)showCustomActivity:(UIView *)activityView;
- (UIView *)customActivityView;
- (void)hideActivity;
@end
