//
//  UITableViewController+Additions.h
//  SOS
//
//  Created by Soumen Bhuin on 3/2/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewController (Additions)

- (NSIndexPath *)indexPathForView:(UIView *)view;
- (void)showMessage:(NSString *)msg;
- (void)hideMessage;

@end
