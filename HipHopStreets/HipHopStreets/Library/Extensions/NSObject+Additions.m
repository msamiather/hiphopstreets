//
//  NSObject+Additions.m
//  HipHopStreets
//
//  Created by IOS on 01/03/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "NSObject+Additions.h"

@implementation NSObject (Additions)

- (NSArray *)arrayFromCorruptedDictionary {
    if ([self isKindOfClass:[NSArray class]]) {
        return (NSArray *)self;
    }
    else if([self isKindOfClass:[NSDictionary class]] ){
        NSDictionary *dic = (NSDictionary *)self;
        int i = 0;
        id obj = dic[[@(i) stringValue]];
        NSMutableArray *arr = [NSMutableArray new];
        while (obj) {
            [arr addObject:obj];
            i++;
            obj = dic[[@(i) stringValue]];
        }
        
        return [arr copy];
    }
    else {
        return nil;
    }
}

@end
