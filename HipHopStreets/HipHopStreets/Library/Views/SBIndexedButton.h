//
//  SBIndexedButton.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 11/18/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBIndexedButton : UIButton
@property (nonatomic, strong) NSIndexPath *indexPath;
@end
