//
//  SBLabel.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 23/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
