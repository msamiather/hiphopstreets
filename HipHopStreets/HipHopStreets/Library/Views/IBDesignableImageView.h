//
//  IBDesignableImageView.h
//  designable
//
//  Created by minggo on 16/5/13.
//  Copyright © 2016年 minggo. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface IBDesignableImageView : UIImageView
@property(nonatomic,assign) IBInspectable CGFloat cornerRadius;
@property(nonatomic,assign) IBInspectable CGFloat borderwidth;
@property(nonatomic,assign) IBInspectable UIColor *borderColor;
@end
