//
//  SBRightImageButton.m
//  SBLibrary
//
//  Created by IOS on 12/29/16.
//  Copyright © 2016 SMB. All rights reserved.
//

#import "SBRightImageButton.h"

@implementation SBRightImageButton

- (instancetype)init {
    if (self = [super init]) {
        [self setupDefaults];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setupDefaults];
    }
    return self;
}

- (void)setupDefaults {
    self.imageView.contentMode = UIViewContentModeCenter;
}

- (CGRect)backgroundRectForBounds:(CGRect)bounds {
    return bounds;
}

- (CGRect)contentRectForBounds:(CGRect)bounds {
    return bounds;
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    CGRect r = contentRect;
    r.size.width -= 20;
    return r;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    CGRect r = contentRect;
    r.size.width = 20;
    r.origin.x = contentRect.size.width-20;
    return r;
}

@end
