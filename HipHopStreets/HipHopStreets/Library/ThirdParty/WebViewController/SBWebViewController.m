//
//  SBWebViewController.m
//
//  Created by Soumen Bhuin on 6/30/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBWebViewController.h"
#import "SBExtensions.h"

@interface SBWebViewController () <UIWebViewDelegate>
@property (nonatomic, strong) NSURLRequest *request;
@end

@implementation SBWebViewController


- (instancetype)initWithRequest:(NSURLRequest *)req {
    if (self = [super init]) {
        self.request = req;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = CGRectZero;
    if (self.navigationController) {
        frame.origin.y = 64;
        frame.size.width = self.view.bounds.size.width;
        frame.size.height = self.view.bounds.size.height;
    }
    else {
        frame.origin.y = 0;
        frame.size.width = self.view.bounds.size.width;
        frame.size.height = self.view.bounds.size.height;
    }
    
    self.webView = [[UIWebView alloc] initWithFrame:frame];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _webView.backgroundColor = [UIColor darkGrayColor];
    _webView.scalesPageToFit = YES;
    [self.view addSubview:_webView];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(btnCancelAction:)];
    
    //self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:239.0f/255.0f green:143.0f blue:42.0f/255.0f alpha:1.0f];
    //self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    //self.navigationController.navigationBar.translucent=NO;
    [self.webView setDelegate:self];
    [self.webView loadRequest:self.request];
}

- (void)btnCancelAction:(UIBarButtonItem *)item {
    [self.webView stopLoading];
    if ([self.Delegate respondsToSelector:@selector(didEndedTheWebviewController:)]) {
        [self.Delegate didEndedTheWebviewController:self];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)btnReloadAction:(UIBarButtonItem *)item {
    [self.webView reload];
}

#pragma mark - UIWebViewDelegate

// Waiting for user's authorization from webView
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self showActivity:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self hideActivity];
}


@end

