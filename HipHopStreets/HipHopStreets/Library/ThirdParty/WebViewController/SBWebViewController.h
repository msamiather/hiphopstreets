//
//  SBWebViewController.h
//
//  Created by Soumen Bhuin on 6/30/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SBWebViewController;
@protocol SBWebViewControllerDelegate <NSObject>
-(void)didEndedTheWebviewController:(SBWebViewController*)controller;
@end

// HOW TO USE:
// Example for TermsOfService
/*NSString *path = [[NSBundle mainBundle] pathForResource:@"TermsOfService" ofType:@"pdf"];
NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]];
SBWebViewController *vc = [[SBWebViewController alloc] initWithRequest:req];
vc.title = @"Terms of Service";
UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
 
[self presentViewController:nav animated:YES completion:nil];*/



@interface SBWebViewController : UIViewController

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, assign) id <SBWebViewControllerDelegate>Delegate;
- (instancetype)initWithRequest:(NSURLRequest *)req;

@end
