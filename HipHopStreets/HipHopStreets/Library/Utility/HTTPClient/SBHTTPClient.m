//
//  SBHTTPClient.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 03/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBHTTPClient.h"



//http://xpandingus.us/hiphopstreets/api/song/13?format=json

//old  http://xpandingus.us/hiphopstreets/

//New one http://app.hiphopstreets.com/

//NSString *const BASE_URL =@"http://www.hiphopstreets.com/";

NSString *const BASE_URL =@"http://app.hiphopstreets.com/";

NSString *const SERVICE_PATH_LOGIN = @"api/login";
NSString *const SERVICE_PATH_REGISTRATION = @"api/user";
NSString *const SERVICE_PATH_FORGOTPASSWORD = @"api/forgotpassword";
NSString *const SERVICE_PATH_CHANGE_PASSWORD = @"api/userpass";
NSString *const SERVICE_PATH_EDIT_PROFILE = @"api/userprofileupdate";

NSString *const SERVICE_UPLOADED_SONGSLIST = @"api/songlist";
NSString *const SERVICE_PATH_UPLOAD_SONG = @"api/song";
NSString *const SERVICE_PATH_DELETE_SONG = @"api/songfr/";
NSString *const SERVICE_PATH_MAKE_FEATURE_SONG = @"api/featuredsong";


NSString *const SERVICE_FAVOURITES = @"api/v1/favsonglist";
NSString *const SERVICE_PATH_LIKE_SONG = @"api/like";
NSString *const SERVICE_PATH_FOLLOW_ARTIST = @"api/follow";


//home page
NSString *const SERVICE_FEATURE_SONGS = @"api/featuredsonglist";

NSString *const SERVICE_TOP_RATED_SONGS = @"api/songlist";
NSString *const SERVICE_CATEOGORY_SONGS = @"http://app.hiphopstreets.com/api/songlists";

NSString *const SERVICE_SONG_SEARCH = @"api/songsearch";

//update profile image
NSString *const SERVICE_UPDATE_PROFILE_IMAGE = @"api/userpic";

//track count

NSString *const SERVICE_PATH_PAYMENT = @"api/payment";

NSString *const SERVICE_TRACK_COUNT = @"Api/user";

//attached paths
NSString *const SERVICE_PATH_IMAGES = @"uploads";
NSString *const SERVICE_PATH_SONGFILE = @"songfile";
NSString *const SERVICE_PATH_SONG_IMAGE = @"songimage";


//artist
NSString *const SERVICE_PATH_ARTIST_PROFILE = @"api/user";


@implementation SBHTTPClient

+ (instancetype)sharedClient {
    static SBHTTPClient *client = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[SBHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        client.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        [client.requestSerializer setValue:@"25d55ad283aa400af464c76d713c07ad" forHTTPHeaderField:@"x-api-key"];
        client.responseSerializer.acceptableContentTypes = nil;
        
    });
    return client;
}

- (NSURLSessionDownloadTask *)download:(NSString *)URLString
                            parameters:(id)parameters
                           destination:(NSURL *)destinationPath
                              progress:(void (^)(NSProgress *downloadProgress)) downloadProgress
                               success:(void (^)(NSURLSessionDownloadTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDownloadTask *task, NSError *error))failure {
    
    NSError *serializationError = nil;
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:&serializationError];
    if (serializationError) {
        if (failure) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
            dispatch_async(self.completionQueue ?: dispatch_get_main_queue(), ^{
                failure(nil, serializationError);
            });
#pragma clang diagnostic pop
        }
        
        return nil;
    }
    
    __block NSURLSessionDownloadTask *downloadTask = nil;
    
    downloadTask = [self downloadTaskWithRequest:request progress:downloadProgress destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return destinationPath;
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (error) {
            if (failure) {
                failure(downloadTask, error);
            }
        } else {
            if (success) {
                success(downloadTask, filePath);
            }
        }
    }];
    
    [downloadTask resume];
    
    return downloadTask;
}

- (id)responseObjectForError:(NSError *)error {
    NSData *data = [error userInfo][AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *dic =  nil;
    if (data) {
        dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    }
    return dic;
}

- (NSString *)responseStringForError:(NSError *)error {
    NSData *data = [error userInfo][AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return str;
}

@end
