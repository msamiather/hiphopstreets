//
//  SBHTTPClient.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 03/02/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"

extern NSString *const BASE_URL;

extern NSString *const SERVICE_PATH_LOGIN;
extern NSString *const SERVICE_PATH_REGISTRATION;
extern NSString *const SERVICE_PATH_FORGOTPASSWORD;
extern NSString *const SERVICE_PATH_CHANGE_PASSWORD;
extern NSString *const SERVICE_PATH_EDIT_PROFILE;


extern NSString *const SERVICE_UPLOADED_SONGSLIST;
extern NSString *const SERVICE_PATH_UPLOAD_SONG;
extern NSString *const SERVICE_PATH_DELETE_SONG;
extern NSString *const SERVICE_PATH_MAKE_FEATURE_SONG;


extern NSString *const SERVICE_FAVOURITES;
extern NSString *const SERVICE_PATH_LIKE_SONG;
extern NSString *const SERVICE_PATH_FOLLOW_ARTIST;


//update profile image
extern NSString *const SERVICE_UPDATE_PROFILE_IMAGE;


//home
extern NSString *const SERVICE_FEATURE_SONGS;
extern NSString *const SERVICE_TOP_RATED_SONGS;
extern NSString *const SERVICE_CATEOGORY_SONGS;
extern NSString *const SERVICE_SONG_SEARCH ;

//track count
extern NSString *const SERVICE_PATH_PAYMENT;
extern NSString *const SERVICE_TRACK_COUNT;

// attached paths
extern NSString *const SERVICE_PATH_IMAGES;
extern NSString *const SERVICE_PATH_SONGFILE;
extern NSString *const SERVICE_PATH_SONG_IMAGE;


//artist
extern NSString *const SERVICE_PATH_ARTIST_PROFILE;


@interface SBHTTPClient : AFHTTPSessionManager

+ (instancetype)sharedClient;


- (NSURLSessionDownloadTask *)download:(NSString *)URLString
                            parameters:(id)parameters
                           destination:(NSURL *)targetPath
                              progress:(void (^)(NSProgress *downloadProgress)) downloadProgress
                               success:(void (^)(NSURLSessionDownloadTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDownloadTask *task, NSError *error))failure;

- (id)responseObjectForError:(NSError *)error;
- (NSString *)responseStringForError:(NSError *)error;

@end
