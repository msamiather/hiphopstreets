//
//  IAPStore.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 19/02/14.
//  Copyright (c) 2014 SMB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

/**
 
 interface IAPProductsViewController () <IAPStoreDelegate>
 @property (nonatomic, strong) IAPStore *store;
 @property (nonatomic, strong) NSNumberFormatter *currencyFormatter;
 @property (nonatomic, strong) NSDictionary *subscriptionInfo;
 @property (nonatomic, strong) NSDictionary *serverProducts;
 @end
 
 self.store = [[IAPStore alloc] init];
 _serverProducts = @{@"com.sipscore.usermem1m":@{@"pid":@"13",@"amount":@"3.99"},@"com.sipscore.usermem1y":@{@"pid":@"11",@"amount":@"39.99"}};
 [_store setProductIdentifiers:@[@"com.sipscore.usermem1m", @"com.sipscore.usermem1y"]];
 [_store setDelegate:self];
 [_store startObservingTransaction];
 
 [_store fetchAndValidateProducts];
 
 [self.store buyProduct:p];
 
 - (void)dealloc {
    [_store stopObservingTransaction];
    [_store setDelegate:nil];
 }
 
 */

@protocol IAPStoreDelegate <NSObject>
@optional
- (void)didFinishFetchingProductsWithError:(NSError *)erroe;
- (void)didFinishPurchasingProductID:(NSString*)pid error:(NSError*)error;
- (void)didFinishRestoringProductsWithError:(NSError*)error;
- (void)didFinishDownloadingProductID:(NSString*)pid error:(NSError*)error;

@end

@interface IAPStore : NSObject

@property (nonatomic, strong) NSArray *productIdentifiers;
@property (nonatomic, strong) NSArray *products;
@property (nonatomic, assign) id<IAPStoreDelegate> delegate;

+ (instancetype)sharedStore;

- (void)stopObservingTransaction;
- (void)startObservingTransaction;

- (void)fetchAndValidateProducts;

- (void)buyProduct:(SKProduct *)product;

- (void)buyProductWithID:(NSString*)pid;

- (SKProduct*)productWithID:(NSString*)pid;

- (void)restoreCompletedTransactions;

- (BOOL)isRestoringTransactions;

- (void)validateReceiptWithAppStoreCompletion:(void (^)(BOOL valid))completion;

- (NSString *)base64EncodedTransactionReceipt;

@end
