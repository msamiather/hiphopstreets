//
//  IAPStore.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 19/02/14.
//  Copyright (c) 2014 SMB. All rights reserved.
//

#import "IAPStore.h"
#import "SBHTTPClient.h"

@interface IAPStore () <SKProductsRequestDelegate,SKPaymentTransactionObserver> {
    BOOL _isRestoringTransactions;
}
@property (nonatomic, strong) NSError *lastError;
@end

@implementation IAPStore

#pragma mark - Initialization

+ (instancetype)sharedStore {
    static IAPStore *store = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        store = [[IAPStore alloc] init];
    });
    return store;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark - Public

- (void)stopObservingTransaction {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)startObservingTransaction {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}


- (void)fetchAndValidateProducts {
    if (self.products==nil) {
        [self validateProductIdentifiers:_productIdentifiers];
    }
    else {
        [self.delegate didFinishFetchingProductsWithError:nil];
    }
}

- (void)buyProduct:(SKProduct *)product {
    if (product) {
        SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
        _isRestoringTransactions = NO;
    }
    else {
        [self.delegate didFinishPurchasingProductID:nil error:[NSError errorWithDomain:@"Product can't be nil" code:0 userInfo:nil]];
    }
}


- (void)buyProductWithID:(NSString*)pid {
    NSArray *pids = [self.products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier == %@",pid]];
    
    SKProduct *p = [pids lastObject];
    
    [self buyProduct:p];
}

- (SKProduct*)productWithID:(NSString*)pid {
    NSArray *pids = [self.products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier == %@",pid]];
    return [pids lastObject];
}

- (void)restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

#pragma mark - Private

- (void)validateProductIdentifiers:(NSArray *)productIdentifiers {
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
                                          initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
    productsRequest.delegate = self;
    [productsRequest start];
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    self.products = response.products;
    
    for (NSString *invalidIdentifier in response.invalidProductIdentifiers) {
        // Handle any invalid product identifiers.
        NSLog(@"Invalid Product-%@",invalidIdentifier);
    }
    
    if ([self.products count]==0) {
        [self.delegate didFinishFetchingProductsWithError:[NSError errorWithDomain:@"com.IAP.Store" code:0 userInfo:nil]];
    }
    else {
        [self.delegate didFinishFetchingProductsWithError:nil];
    }
}

#pragma mark - SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray*)transactions;
{
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
            case SKPaymentTransactionStateRestored:
            {
                if (transaction.downloads.count) {
                    [[SKPaymentQueue defaultQueue] startDownloads:transaction.downloads];
                } else {
                    // unlock features
                    
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                    [self.delegate didFinishPurchasingProductID:transaction.payment.productIdentifier error:nil];
                }
                break;
        	}
            case SKPaymentTransactionStateFailed:
            {
                NSLog(@"Purchase Failed: %@",transaction.error);
                [self.delegate didFinishPurchasingProductID:transaction.payment.productIdentifier error:transaction.error];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                
                break;
        	}
            default:
                break;
                
        }
    }	
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    [self.delegate didFinishRestoringProductsWithError:nil];
    _isRestoringTransactions = NO;
}

// Sent when all transactions from the user's purchase history have successfully been added back to the queue.
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    _isRestoringTransactions = YES;
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions {
    if ([[queue transactions] count]==0) {
        self.lastError = nil;
        _isRestoringTransactions = NO;
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads;
{
    
    for (SKDownload *download in downloads) {
        
        if (download.downloadState == SKDownloadStateFinished) {
            [self processDownload:download]; // not written yet
            // now we're done
            
            [self.delegate didFinishDownloadingProductID:download.transaction.payment.productIdentifier error:nil];
            
            NSLog(@"Product Purchased:%@",download.transaction.payment.productIdentifier);
            
            [queue finishTransaction:download.transaction];
            
        } else if (download.downloadState == SKDownloadStateActive) {
            
            //NSString *productID = download.contentIdentifier; // in app purchase identifier
            //NSTimeInterval remaining = download.timeRemaining; // secs
            //float progress = download.progress; // 0.0 -> 1.0
            
            // NOT SHOWN: use the productID to notify your model of download progress...
            
        }
        else if(download.downloadState == SKDownloadStateFailed || download.downloadState == SKDownloadStateCancelled) {    // failed, cancelled
            
            self.lastError = download.error;
            
            [self.delegate didFinishDownloadingProductID:download.transaction.payment.productIdentifier error:download.error];
            
            [[SKPaymentQueue defaultQueue] finishTransaction:download.transaction];
        }
        else {    // waiting, paused
            //NSLog(@"Warn: not handled: %ld", (long)download.downloadState);
        }
    }
}

- (void) processDownload:(SKDownload*)download;
{
    // convert url to string, suitable for NSFileManager
    NSString *path = [download.contentURL path];
    
    // files are in Contents directory
    path = [path stringByAppendingPathComponent:@"Contents"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *files = [fileManager contentsOfDirectoryAtPath:path error:&error];
    NSString *dir = [self downloadableContentPath]; // not written yet
    
    for (NSString *file in files) {
        NSString *fullPathSrc = [path stringByAppendingPathComponent:file];
        NSString *fullPathDst = [dir stringByAppendingPathComponent:file];
        
        // not allowed to overwrite files - remove destination file
        [fileManager removeItemAtPath:fullPathDst error:NULL];
        
        if ([fileManager moveItemAtPath:fullPathSrc toPath:fullPathDst error:&error] == NO) {
            NSLog(@"Error: unable to move item: %@", error);
        }
    }
    
}

- (NSString *) downloadableContentPath;
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    directory = [directory stringByAppendingPathComponent:@"Downloads"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:directory] == NO) {
        
        NSError *error;
        if ([fileManager createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:&error] == NO) {
            NSLog(@"Error: Unable to create directory: %@", error);
        }
        
        NSURL *url = [NSURL fileURLWithPath:directory];
        // exclude downloads from iCloud backup
        if ([url setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error] == NO) {
            NSLog(@"Error: Unable to exclude directory from backup: %@", error);
        }
    }
    
    return directory;
}


- (BOOL)isRestoringTransactions {
    return _isRestoringTransactions;
}

- (void)validateReceiptWithAppStoreCompletion:(void (^)(BOOL valid))completion {
    
    // Load the receipt from the app bundle.
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    if (!receipt) { /* No local receipt -- handle the error. */
    
    
    }
    
    /* ... Send the receipt data to your server ... */

    // Create the JSON object that describes the request
    NSError *error;
    NSDictionary *requestContents = @{
                                      @"receipt-data": [receipt base64EncodedStringWithOptions:0],
                                      @"password":@""
                                      };
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    NSString *sandboxURL =  @"https://sandbox.itunes.apple.com/verifyReceipt";
    //NSString *productionURL =   @"https://buy.itunes.apple.com/verifyReceipt";
    
    // Create a POST request with the receipt data.
    NSURL *storeURL = [NSURL URLWithString:sandboxURL];
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:storeRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        if ([dic[@"status"] integerValue]==0) {//valid
            completion(YES);
        }
        else {
            completion(NO);
        }
    }];
    
    [task resume];
}

- (NSString *)base64EncodedTransactionReceipt {
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    return [receipt base64EncodedStringWithOptions:0];
}

@end
