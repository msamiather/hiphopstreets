//
//  SongLibrary.h
//  MyMusic
//
//  Created by IOS on 02/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Song.h"

typedef enum : NSUInteger {
    SongRepeatNone,
    SongRepeatAll,
    SongRepeatOne,
} SongRepeatType;

@interface SongLibrary : NSObject

+ (instancetype)sharedLibrary;

- (BOOL)addSong:(NSDictionary*)songInfo;
- (void)removeSong:(NSDictionary*)songInfo;


- (NSArray*)allSongs;
- (NSInteger)getSongIndexById:(NSString*)songId;

@property (nonatomic, assign) NSUInteger currentIndex;
@property (nonatomic, assign, getter=isPlayingMusic) BOOL playingMusic;
@property (nonatomic, assign) SongRepeatType repeatType;

- (void)shuffle:(BOOL)shuffle;
- (NSDictionary *)songAtIndex:(NSUInteger)index;

- (void)saveTheCurrentSong:(NSDictionary*)song;
- (NSArray*)recentlyPlayedSongs;

@property (nonatomic, weak) UITabBarController *tabBarController;
-(void)showPlayerAndPlaySong:(id)song;
- (void)showPlayerAndPlaySong:(id)song repeatType:(SongRepeatType)rtype songIdex:(NSInteger)idex type:(BOOL)isTopratesSongs;
-(NSInteger)getcurrentIndexFromArry:(NSArray*)arr fromDict:(NSDictionary*)dict;
@end
