//
//  SongLibrary.m
//  MyMusic
//
//  Created by IOS on 02/11/16.
//  Copyright © 2016 igi. All rights reserved.
//

#import "SongLibrary.h"
#import "SBPreference.h"
#import "SBHTTPClient.h"
#import "AudioPlayerVC.h"
#import "Song.h"

@interface SongLibrary ()
@property (nonatomic ,strong) NSMutableArray *songs;
@property (nonatomic ,strong) NSMutableArray *shuffledSongs;
@end

@implementation SongLibrary

+ (instancetype)sharedLibrary {
    static SongLibrary *shared = nil; 
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[SongLibrary alloc] init];
    });
    return shared;
}

- (instancetype)init {
    if (self = [super init]) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"songs.json"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            self.songs = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path] options:NSJSONReadingMutableContainers error:nil];
            self.shuffledSongs = [_songs mutableCopy];
        }
        else {
            self.songs = [NSMutableArray new];
            self.shuffledSongs = [NSMutableArray new];
        }
        
    }
    return self;
}

- (BOOL)addSong:(NSDictionary*)songInfo {
    
    BOOL isExitSong=NO;
    for (NSDictionary * dict in _songs) {
        if ([dict[@"songid"] isEqualToString:songInfo[@"songid"]]) {
            isExitSong=YES;
            break;
        }
    }

    if (!isExitSong) {
        [_songs addObject:songInfo];
        
        [self saveSongList];
        
        [_shuffledSongs addObject:songInfo];
    }
    return isExitSong;
}

- (NSArray*)allSongs {
    return _shuffledSongs;
}

- (void)removeSong:(NSDictionary*)songInfo {

    NSInteger index = 0;
    for (NSDictionary *dict in _songs) {
        if ([dict[@"songid"] isEqualToString:songInfo[@"songid"]]) {
            break;
        }
        index++;
    }
    
    [self.songs removeObjectAtIndex:index];
    
    index = 0;
    for (NSDictionary *dict in _shuffledSongs) {
        if ([dict[@"songid"] isEqualToString:songInfo[@"songid"]]) {
            break;
        }
        index++;
    }
    
    [self.shuffledSongs removeObjectAtIndex:index];
    
    [self saveSongList];
}


- (void)saveSongList{
    NSError *error = nil;
    
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"songs.json"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:_songs
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    [jsonData writeToFile:path atomically:YES];
    
}


- (NSInteger)getSongIndexById:(NSString*)songId {
    NSInteger index = 0;
    for (NSDictionary *dict in _shuffledSongs) {
        if ([dict[@"songid"] isEqualToString:songId]) {
            break;
        }
        index++;
    }
    return index;
}

- (void)shuffle:(BOOL)shuffle {
    if (shuffle) {
        
        NSUInteger count = [self.shuffledSongs count];
        for (NSUInteger i = 0; i < count; ++i) {
            NSInteger remainingCount = count - i;
            NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
            [_shuffledSongs exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
        }
        
    }
    else {
        self.shuffledSongs = [_songs mutableCopy];
    }
}

- (NSDictionary *)songAtIndex:(NSUInteger)index {
    return [_shuffledSongs objectAtIndex:index];
}


-(void)saveTheCurrentSong:(NSDictionary*)song{

   NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSMutableArray *playedSongs=nil;
    NSArray *arr=[userDefaults arrayForKey:@"recentlyPlayedSongs"];
    if (arr.count ) {
        playedSongs=[arr mutableCopy];
    }
    else{
        
        playedSongs=[[NSMutableArray alloc]init];
    
    }
    
    NSInteger index=0;
    NSInteger resultindex = -1;
    for (NSDictionary *dict in playedSongs) {
        if ([dict[@"songid"] isEqualToString:song[@"songid"]]) {
            resultindex = index;
            break;
        }
        index++;
    }
    if (resultindex!=-1) {
        [playedSongs removeObjectAtIndex:index];
    }
    
    [playedSongs insertObject:song atIndex:0];
    
    if (playedSongs.count==11){
     [playedSongs removeLastObject];
    }
    [userDefaults setObject:playedSongs forKey:@"recentlyPlayedSongs"];
    [userDefaults synchronize];
    
}

-(NSInteger)getcurrentIndexFromArry:(NSArray*)arr fromDict:(NSDictionary*)dict{
    int i=0;
    for (NSDictionary *xb in arr) {
        if ([xb[@"songid"] isEqualToString:dict[@"songid"]]) {
            break;
        }
        i++;
    }
    return i;
}

-(NSArray*)recentlyPlayedSongs{
    return [[NSUserDefaults standardUserDefaults] arrayForKey:@"recentlyPlayedSongs"];
}

- (void)showPlayerAndPlaySong:(id)song {
    [self.tabBarController setSelectedIndex:2];
    UINavigationController *nav = [self.tabBarController viewControllers][2];
   AudioPlayerVC *vc = (AudioPlayerVC *) [[nav viewControllers] firstObject];
    //[vc playSong:song];
    [vc playSong:song currentIdex:0 type:NO];

}

- (void)showPlayerAndPlaySong:(id)song repeatType:(SongRepeatType)rtype songIdex:(NSInteger)idex type:(BOOL)isTopratesSongs{
    self.repeatType = rtype;
    [self.tabBarController setSelectedIndex:2];
    UINavigationController *nav = [self.tabBarController viewControllers][2];
    AudioPlayerVC *vc = (AudioPlayerVC *) [[nav viewControllers] firstObject];
   // [vc playSong:song];
    [vc playSong:song currentIdex:idex type:isTopratesSongs];
    
}

@end
