//
//  UITheme.m
//  SBLibrary
//
//  Created by Soumen Bhuin on 3/21/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import "UITheme.h"

@implementation UITheme


+ (NSAttributedString *)attributedPlaceholder:(NSString *)placeholder {
    return [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:17.0],NSForegroundColorAttributeName:[UIColor colorWithWhite:0.541 alpha:1.000]}];
}

+ (UIImage *)contactPlaceholderImage {
    return [UIImage imageNamed:@"userface"];
}

+ (UIImage *)whiteTextFieldBackground {
    return [[UIImage imageNamed:@"white3dbg"]resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20) resizingMode:UIImageResizingModeStretch];
}

+ (UIImage *)greenButtonBackground {
    return [[UIImage imageNamed:@"green3dbg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
}



@end
