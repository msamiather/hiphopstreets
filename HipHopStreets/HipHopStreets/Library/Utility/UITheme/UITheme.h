//
//  UITheme.h
//  SBLibrary
//
//  Created by Soumen Bhuin on 3/21/16.
//  Copyright © 2016 goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define THEME_GREENCOLOR [UIColor colorWithRed:0.141 green:0.620 blue:0.071 alpha:1.000]
#define THEME_REDCOLOR [UIColor colorWithRed:0.980 green:0.071 blue:0.102 alpha:1.000]

@interface UITheme : NSObject

+ (NSAttributedString *)attributedPlaceholder:(NSString *)placeholder;

+ (UIImage *)contactPlaceholderImage;

+ (UIImage *)whiteTextFieldBackground;

+ (UIImage *)greenButtonBackground;


@end
