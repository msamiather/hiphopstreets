//
//  SBPreference.m
//  SBLibrary
//
//  Created by Soumen Bhuin
//  Copyright © 2016 goigi. All rights reserved.
//

#import "SBPreference.h"

@interface SBPreference ()
@property (nonatomic, strong) NSUserDefaults *ud;
@end

@implementation SBPreference

+ (instancetype)sharedPreference {
    static SBPreference *sharedPref = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPref = [[SBPreference alloc] init];
    });
    return sharedPref;
}

- (void)setupDefaults {
    _loggedInUserInfo = [_ud dictionaryForKey:@"liuserinfo"];
}

- (instancetype)init {
    if (self = [super init]) {
        self.ud = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

- (void)setLoggedInUserInfo:(NSDictionary *)loggedInUserInfo {
    _loggedInUserInfo = [loggedInUserInfo mutableCopy];
    if (loggedInUserInfo) {
        [_ud setObject:loggedInUserInfo forKey:@"liuserinfo"];
    }
    else {
        [_ud removeObjectForKey:@"liuserinfo"];
    }
    [_ud synchronize];
}

- (void)updateLoggedInUserInfoValue:(NSString *)value forKey:(NSString *)key {
    NSMutableDictionary *dic = [_loggedInUserInfo mutableCopy];
    if (value) {
        [dic setObject:value forKey:key];
    }
    else {
        [dic removeObjectForKey:key];
    }
    _loggedInUserInfo = dic;
    [_ud setObject:dic forKey:@"liuserinfo"];
    [_ud synchronize];
}

- (void)setUserLoggedIn:(BOOL)userLoggedIn {
    [_ud setBool:userLoggedIn forKey:@"userlogged"];
    [_ud synchronize];
}

- (BOOL)isUserLoggedIn {
    return [_ud boolForKey:@"userlogged"];
}

- (NSString *)mobileNumber {
    return _loggedInUserInfo[MOBILE_NO];
}

- (NSString *)callingCode {
    id x = _loggedInUserInfo[CALLING_CODE];
    if ([x isKindOfClass:[NSNumber class]]) {
        return [(NSNumber *)x stringValue];
    }
    return (NSString *)x;
}

- (NSString *)userName {
    NSMutableString *mStr = [NSMutableString new];
    if (_loggedInUserInfo[FIRSTNAME]) {
        [mStr appendString:_loggedInUserInfo[FIRSTNAME ]];
        [mStr appendString:@" "];
    }
    if (_loggedInUserInfo[LASTNAME]) {
        [mStr appendString:_loggedInUserInfo[LASTNAME ]];
    }
    return [mStr copy];
}

- (NSString *)userInitial {
    NSMutableString *mStr = [NSMutableString new];
    NSString *fName = _loggedInUserInfo[FIRSTNAME];
    if (fName.length>0) {
        [mStr appendString:[fName substringToIndex:1]];
    }
    NSString *lName = _loggedInUserInfo[LASTNAME];
    if (lName.length>0) {
        [mStr appendString:[lName substringToIndex:1]];
    }
    return [mStr copy];
}

- (NSString *)appVersion {
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * versionBuildString = [NSString stringWithFormat:@"v. %@(#%@)", appVersionString, appBuildString];
    return versionBuildString;
}

@end
