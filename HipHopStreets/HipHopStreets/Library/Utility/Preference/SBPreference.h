//
//  SBPreference.h
//  SBLibrary
//
//  Created by Soumen Bhuin
//  Copyright © 2016 goigi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define USER_ID @"id"
#define MOBILE_NO @"mobile"
#define FIRSTNAME @"fname"
#define LASTNAME @"lname"
#define PHOTO @"pic"
#define PASSWORD @"password";
#define NAME @"name"
#define EMAIL @"email_id"
#define USERTYPE @"user_cat"
#define BIOGRAPHY @"biography"



#define GENDER @"sex"
#define DOB @"Dob"
#define CALLING_CODE @"CCode"
#define DESCRIPTION @"Descriptions"
#define XMPPP @"RandomPassword"

@interface SBPreference : NSObject

+ (instancetype)sharedPreference;
- (void)setupDefaults;

@property (strong, nonatomic) NSDictionary *loggedInUserInfo;
@property (assign, nonatomic, getter=isUserLoggedIn) BOOL userLoggedIn;

- (void)updateLoggedInUserInfoValue:(NSString *)value forKey:(NSString *)key;

- (NSString *)mobileNumber;
- (NSString *)callingCode;
- (NSString *)userName;
- (NSString *)userInitial;
- (NSString *)appVersion;

@end
