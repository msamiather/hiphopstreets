//
//  UploadedSongListCell.h
//  HipHopStreets
//
//  Created by IOS on 13/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadedSongListCell : UITableViewCell
@property(nonatomic,strong) IBOutlet UIView *bgView;
@property(nonatomic,strong) IBOutlet UILabel *lblSongName;
@property(nonatomic,strong) IBOutlet UILabel *lblCategoryName;
@property(nonatomic,strong) IBOutlet UIImageView *songImage;

@end
