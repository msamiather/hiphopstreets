//
//  UploadedSongListCell.m
//  HipHopStreets
//
//  Created by IOS on 13/01/17.
//  Copyright © 2017 Goigi. All rights reserved.
//

#import "UploadedSongListCell.h"

@implementation UploadedSongListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.bgView.layer.cornerRadius=10.0f;
    
    CGSize _screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat width = (_screenSize.width-10);
    
    CALayer * layer = [self.bgView layer];
    [layer setShadowOffset:CGSizeMake(0, 1)];
    [layer setShadowRadius:1.0];
    [layer setShadowColor:[UIColor blackColor].CGColor] ;
    [layer setShadowOpacity:0.5];
    layer.cornerRadius = 2;
    layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, width, 78) cornerRadius:2].CGPath;
    self.clipsToBounds=NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
